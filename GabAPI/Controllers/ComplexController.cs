﻿using COMMON;
using GabAPI.Business;
using GabAPI.Models.Inputs.Complex;
using System.Web.Http;

namespace GabAPI.Controllers
{
    [RoutePrefix("Complex")]
    public class ComplexController : ApiController
    {
        readonly ComplexBusiness _ComplexManager = new ComplexBusiness();

        [HttpPost]
        [Route("GetAllComplexList")]
        public IHttpActionResult GetAllComplexList()
        {
            var result = _ComplexManager.GetAllComplexList();
            return Ok(result);
        }

        [HttpPost]
        [Route("AddComplex")]
        public IHttpActionResult AddComplex(Complex value)
        {
             _ComplexManager.AddComplex(value);
            return Ok("بازار با موفقیت اضافه شد");
        }

        [HttpPost]
        [Route("GetallComplex")]
        public IHttpActionResult GetallComplex()
        {
            var result = _ComplexManager.GetallComplex();
            return Ok(result);
        }

        [HttpPost]
        [Route("RemoveComplex")]
        public IHttpActionResult RemoveComplex(InputEditComplex value)
        {
            _ComplexManager.RemoveComplex(value);
            return Ok("بازار با موفقیت حذف شد");
        }
    }
}
