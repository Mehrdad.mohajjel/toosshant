﻿using COMMON;
using GabzApi.Business;
using System;
using System.Web.Http;
using GabzApi.Filters;
using GabAPI.Models.Inputs.Customer;
using GabAPI.Models.Inputs.KarKard;

namespace GabzApi.Controllers
{
    [RoutePrefix("Customer")]
    public class CustomerController : ApiController
    {
        readonly CustomerBusiness _CustomerBusinessManager = new CustomerBusiness();

        [HttpPost]
        [JwtValidation]
        [Route("AddCustomer")]
        public IHttpActionResult AddCustomer(Customer values)
        {
            values.IsActive = true;
            _CustomerBusinessManager.AddCustomer(values);
            return Ok("مشترک با موفقیت اضافه شده");
        }
        [HttpPost]
        [JwtValidation]
        [Route("AddNewExpence")]
        public IHttpActionResult AddNewExpence(InputaddExpence values)
        {
            _CustomerBusinessManager.AddNewExpence(values);
            return Ok("کارکرد با موفقیت اضافه شده");
        }
        [HttpPost]
        [JwtValidation]
        [Route("GetExpenceDetail")]
        public IHttpActionResult GetExpenceDetail(InputGetKarkardDetail values)
        {
            var result =_CustomerBusinessManager.GetExpenceDetail(values);
            return Ok(result);
        }


        [HttpPost]
        [Route("GetLastCode")]
        public IHttpActionResult GetLastCode()
        {
           var LastCode = _CustomerBusinessManager.GetLastCode();
            LastCode = (Convert.ToInt64(LastCode) + 1).ToString("0000");
            return Ok(LastCode);
        }
        
        [HttpPost]
        [Route("GetAllCustomerList")]
        public IHttpActionResult GetAllCustomerList()
        {
            var Result = _CustomerBusinessManager.GetAllCustomerList();
            return Ok(Result);
        }
        [HttpPost]
        [Route("GetCustomerList")]
        public IHttpActionResult GetCustomerList()
        {
            var Result = _CustomerBusinessManager.GetCustomerList();
            return Ok(Result);
        }

        [HttpPost]
        [Route("GetCustomerDetailByID")]
        public IHttpActionResult GetCustomerDetailByID(InputGetCustomerByID values)
        {
            var Result = _CustomerBusinessManager.GetCustomerDetailByID(values);
            return Ok(Result);
        }
        [HttpPost]
        [Route("GetActivePriceList")]
        public IHttpActionResult GetActivePriceList()
        {
            var Result = _CustomerBusinessManager.GetActivePriceList();
            return Ok(Result);
        }
        [HttpPost]
        [Route("GetAllCustomeTypeList")]
        public IHttpActionResult GetAllCustomeTypeList()
        {
            var Result = _CustomerBusinessManager.GetAllCustomeTypeList();
            return Ok(Result);
        }
        
        [HttpPost]
        [JwtValidation]
        [Route("RemoveCustomer")]
        public IHttpActionResult RemoveCustomer(Customer values)
        {
            values.IsActive = true;
            _CustomerBusinessManager.RemoveCustomer(values);
            return Ok("مشترک با موفقیت حذف شد");
        }

    }
}
