﻿using GabAPI.Business;
using GabAPI.Models.Inputs.CustomerShop.AssignShop;
using System.Web.Http;

namespace GabAPI.Controllers
{
    [RoutePrefix("Shop")]
    public class ShopController : ApiController
    {
        //
        readonly ShopBusiness _ShopBusinessManager = new ShopBusiness();

        [HttpPost]
        [Route("GetShopListByComplexID")]
        public IHttpActionResult GetShopListByComplexID(InputGetShopListByComplexID values)
        {
            var result = _ShopBusinessManager.GetShopListByComplexID(values);
            return Ok(result);
        }

    }
}
