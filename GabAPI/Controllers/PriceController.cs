﻿using COMMON;
using GabAPI.Business;
using GabAPI.Models.Inputs.Price;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GabAPI.Controllers
{
    [RoutePrefix("Price")]
    public class PriceController : ApiController
    {
        readonly PriceBusiness _PriceBusinessManager = new PriceBusiness();

        [HttpPost]
        [Route("AddPrice")]
        public IHttpActionResult AddPrice(Price values)
        {

            _PriceBusinessManager.AddPrice(values);
            return Ok("مبلغ با موفقیت اضافه شده");
        }


        [HttpPost]
        [Route("GetallPrice")]
        public IHttpActionResult GetallUser()
        {
            var result = _PriceBusinessManager.GetAllPriceList();
            return Ok(result);
        }



        [HttpPost]
        [Route("ChangeStatus")]
        public IHttpActionResult ChangeStatus(InputEditPrice values)
        {

            _PriceBusinessManager.ChageStatus(values);
            return Ok(" تغییر وضعیت داده شد");
        }
       
    }
}
