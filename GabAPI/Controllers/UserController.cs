﻿using COMMON;
using GabAPI.Business;
using GabAPI.Models.Inputs.User;
using GabzApi.Filters;
using System.Linq;
using System.Web.Http;

namespace GabAPI.Controllers
{
    [RoutePrefix("User")]
    public class UserController : ApiController
    {
        readonly UserBusiness _UserBusinessManager = new UserBusiness();

        [HttpPost]
        [JwtValidation]
        [Route("AddUser")]
        public IHttpActionResult AddUser(User values)
        {
     
        _UserBusinessManager.AddUser(values);
            return Ok("کاربر با موفقت اضافه شده");
        }


        [HttpPost]
        [JwtValidation]
        [Route("GetallUser")]
        public IHttpActionResult GetallUser(InputGetAllUserList values)
        {
           var result =  _UserBusinessManager.GetAllUserList().Where( a => a.NickName.Contains(values.NickName) || string.IsNullOrEmpty(values.NickName.ToString()) && a.UserName.Contains(values.Username) || string.IsNullOrEmpty(values.Username.ToString()));
            return Ok(result);
        }


        [HttpPost]
        [JwtValidation]
        [Route("ResetPassword")]
        public IHttpActionResult ResetPassword(InputResetPassword values)
        {

            _UserBusinessManager.ResetPassword(values);
            return Ok("رمز عبورکاربر با موفقیت ریست شد");
        }


        [HttpPost]
        [JwtValidation]
        [Route("ChangeStatus")]
        public IHttpActionResult ChangeStatus(InputChangeStatus values)
        {

            _UserBusinessManager.ChangeStatus(values);
            return Ok("کاربر تغییر وضعیت داده شد");
        }
        [HttpPost]
        [JwtValidation]
        [Route("RemoveUser")]
        public IHttpActionResult RemoveUser(InputRemoveUser values)
        {

            _UserBusinessManager.RemoveUser(values);
            return Ok("کاربر به حالت حذف تغییر وضعیت داده شد");
        }
        
    }
}
