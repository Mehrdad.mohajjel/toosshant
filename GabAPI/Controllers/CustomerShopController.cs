﻿using GabAPI.Business;
using GabAPI.Models.Inputs.AssignShop;
using GabAPI.Models.Inputs.CustomerShop;
using GabAPI.Models.Inputs.CustomerShop.AssignPeopleToLine;
using GabzApi.Filters;
using System.Web.Http;

namespace GabAPI.Controllers
{
    [RoutePrefix("CustomerShop")]
    public class CustomerShopController : ApiController
    {
        readonly CustomerShopBusiness _CustomerShopBusinessManager = new CustomerShopBusiness();
        [HttpPost]
        [Route("GetCustomerShopHistory")]
        public IHttpActionResult GetCustomerShopByCustomerID(InputGetCustomerShopListByCustomertID values)
        {
            var result = _CustomerShopBusinessManager.GetCustomerShopByCustomerID(values);
            return Ok(result);
        }
        [HttpPost]
        [Route("AddCustomerShop")]
        [RequestModelNullValidation]
        [RequestModelValidation]
        public IHttpActionResult AddCustomerShop(InputAddCustomerToShop values)
        {
            CustomerShopBusiness.AddCustomerShop(values);
            return Ok("مغازه به مشتری  تخصیص یافت.");
        }
        [HttpPost]
        [Route("GetCustomerShopByComplexID")]
        [RequestModelNullValidation]
        [RequestModelValidation]
        public IHttpActionResult GetCustomerShopByComplexID(InputCustomerShopByComplexID values)
        {
            var result = _CustomerShopBusinessManager.GetCustomerShopByComplexID(values);
            return Ok(result);
        }

        [HttpPost]
        [Route("RemoveShopCustomer")]
        [RequestModelNullValidation]
        [RequestModelValidation]
        public IHttpActionResult RemoveShopCustomer(InputRemovePeople values)
        {
            CustomerShopBusiness.RemoveCustomersShop(values);
            return Ok("مغازه از مشتری  حذف شد.");
        }
        
    }
}
