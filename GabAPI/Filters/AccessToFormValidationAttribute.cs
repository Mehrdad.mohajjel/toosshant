﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace GabzApi.Filters
{
    public class AccessToFormValidationAttribute : ActionFilterAttribute
    {
        public string FormCode { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
           // CheckAccessToFrom(actionContext);
            base.OnActionExecuting(actionContext);
        }
        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            //CheckAccessToFrom(actionContext);
            return base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
        //private void CheckAccessToFrom(HttpActionContext actionContext)
        //{
        //    try
        //    {
        //        var userId = 0;
        //        HttpCookie reqCookies = HttpContext.Current.Request.Cookies["userInfo"];
        //        if (reqCookies != null)
        //        {
        //            userId = Convert.ToInt32(reqCookies["UserId"]);

        //            var result = Utilities.UserTools.HasAccessToForm(userId, FormCode, "022");
        //            if (!result)
        //            {
        //                actionContext.Response = actionContext.Request
        //                   .CreateErrorResponse(HttpStatusCode.Unauthorized, "شما مجوز دسترسی به این صفحه را ندارید");

        //            }
        //        }
        //        else
        //        {
        //            actionContext.Response = actionContext.Request
        //            .CreateErrorResponse(HttpStatusCode.Unauthorized, "شما مجوز دسترسی به این بخش را ندارید");

        //        }



        //    }
        //    catch
        //    {
        //        actionContext.Response = actionContext.Request
        //               .CreateErrorResponse(HttpStatusCode.Unauthorized, "شما مجوز دسترسی به این بخش را ندارید");
        //    }
        //}

    }
}