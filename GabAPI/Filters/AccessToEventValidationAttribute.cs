﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace GabzApi.Filters
{
    public class AccessToEventValidationAttribute: ActionFilterAttribute
    {
        public string FormCode { get; set; }
        public string EventCode { get; set; }
        public string SystemCode { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            CheckAccessToEvent(actionContext);
            base.OnActionExecuting(actionContext);
        }
        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            CheckAccessToEvent(actionContext);
            return base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
        private void CheckAccessToEvent(HttpActionContext actionContext)
        {
            try
            {


                var token = actionContext.Request.Headers.Authorization.Parameter;
                var userId = 0;
                if(string.IsNullOrEmpty(token))
                {
                    actionContext.Response = actionContext.Request
                       .CreateErrorResponse(HttpStatusCode.Unauthorized, "شما مجوز دسترسی به این بخش را ندارید");
                }
                else
                {

                    //userId = Utilities.UserTools.GetUserIdByJsonWebToken(token);
                    //var result = Utilities.UserTools.HasAccessToEvent(userId, FormCode,EventCode ,SystemCode);
                    //if(!result)
                    //{
                    //    actionContext.Response = actionContext.Request
                    //       .CreateErrorResponse(HttpStatusCode.Unauthorized, "شما مجوز دسترسی به این عملیات را ندارید");

                    //}
                }
            }
            catch
            {
                actionContext.Response = actionContext.Request
                       .CreateErrorResponse(HttpStatusCode.Unauthorized, "شما مجوز دسترسی به این بخش را ندارید");
            }
        }
    }
}