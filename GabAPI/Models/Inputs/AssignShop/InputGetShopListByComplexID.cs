﻿using System.ComponentModel.DataAnnotations;

namespace GabAPI.Models.Inputs.CustomerShop.AssignShop
{
    public class InputGetShopListByComplexID
    {
        [Required]
        public long ComplexID { get; set; }
    }
}