﻿using System.ComponentModel.DataAnnotations;

namespace GabAPI.Models.Inputs.CustomerShop.AssignPeopleToLine
{
    public class InputGetEmployeeListOfDepartment
    {
        [Required]
        public int UnitCode { get; set; }
    }
}