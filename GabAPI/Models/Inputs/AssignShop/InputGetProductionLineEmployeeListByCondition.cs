﻿using System.ComponentModel.DataAnnotations;

namespace GabAPI.Models.Inputs.CustomerShop.AssignPeopleToLine
{
    public class InputGetProductionLineEmployeeListByCondition
    {
        [Required]
        public int DepartmentID { get; set; }

    }
}