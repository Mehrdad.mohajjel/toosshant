﻿using System.ComponentModel.DataAnnotations;

namespace GabAPI.Models.Inputs.CustomerShop.AssignPeopleToLine
{
    public class InputTransferEmployeeToProductionLine
    {
        [Required]
        public long ProductionLineEmployeeID { get; set; }
        [Required]
        public int DestinationDepartmentID { get; set; }
        [Required]
        public int UserID { get; set; }
    }
}