﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GabAPI.Models.Inputs.AssignShop
{
    public class InputRemovePeople
    {
        public long? CustomerShopID { get; set; }
        public long UserID { get; set; }
    }
}