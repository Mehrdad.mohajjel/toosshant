﻿using System.ComponentModel.DataAnnotations;

namespace GabAPI.Models.Inputs.CustomerShop.AssignPeopleToLine
{
    public class InputAddCustomerToShop
    {
        [Required]
        public long ShopID { get; set; }
        [Required]
        public long CustomerID { get; set; }
        public string TabloName { get; set; }
        public long UserID { get; set; }
    }
}