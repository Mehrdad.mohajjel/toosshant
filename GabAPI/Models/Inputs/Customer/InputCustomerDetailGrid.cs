﻿namespace GabAPI.Models.Inputs.Customer
{
    public class InputCustomerDetailGrid
    {
        public long CustomerId { get; set; }
    }
}