﻿using System;

namespace GabAPI.Models.Inputs.Customer
{
    public class InputGetCustomerList
    {
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
    }
}