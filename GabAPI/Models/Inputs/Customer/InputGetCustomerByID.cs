﻿namespace GabAPI.Models.Inputs.Customer
{
    public class InputGetCustomerByID
    {
        public long CustomerID { get; set; }
    }
}