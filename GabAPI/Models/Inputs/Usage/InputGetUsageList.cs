﻿using System;

namespace GabAPI.Models.Inputs.Usage
{
    public class InputGetUsageList
    {
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }

    }
}