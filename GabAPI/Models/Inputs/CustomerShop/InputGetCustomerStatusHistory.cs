﻿using System.ComponentModel.DataAnnotations;

namespace GabAPI.Models.Inputs.CustomerShop
{
    public class InputGetCustomerStatusHistory
    {
        [Required]
        public long CustomerID { get; set; }
    }
}