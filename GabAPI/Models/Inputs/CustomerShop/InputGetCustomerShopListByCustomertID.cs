﻿namespace GabAPI.Models.Inputs.CustomerShop
{
    public class InputGetCustomerShopListByCustomertID
    {
        public long CustomerID { get; set; }
    }
}