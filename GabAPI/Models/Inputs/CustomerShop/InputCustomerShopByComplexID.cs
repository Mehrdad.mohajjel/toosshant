﻿namespace GabAPI.Models.Inputs.CustomerShop
{
    public class InputCustomerShopByComplexID
    {
        public long ComplexID { get; set; }
    }
}