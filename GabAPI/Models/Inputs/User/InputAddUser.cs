﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GabAPI.Models.Inputs.User
{
    public class InputAddUser
    {

        public string UserName { get; set; }
        public string Password { get; set; }
        [Phone]
        public string Mobile { get; set; }
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
        public string NickName { get; set; }
        public Nullable<int> Status { get; set; }
    }
}