﻿namespace GabAPI.Models.Inputs.User
{
    public class InputGetAllUserList
    {
        public string Username { get; set; }
        public string NickName { get; set; }
    }
}