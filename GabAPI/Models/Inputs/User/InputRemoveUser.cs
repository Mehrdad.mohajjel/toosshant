﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GabAPI.Models.Inputs.User
{
    public class InputRemoveUser
    {
        public long UserID { get; set; }
    }
}