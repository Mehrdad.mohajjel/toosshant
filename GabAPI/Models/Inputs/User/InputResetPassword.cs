﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GabAPI.Models.Inputs.User
{
    public class InputResetPassword
    {
        public long UserID { get; set; }
        public long OperationUserId { get; set; }
    }
}