﻿using System;

namespace GabAPI.Models.Inputs.KarKard
{
    public class InputaddExpence
    {
        public long CustomerShopID { get; set; }
        public long PriceId { get; set; }
        public decimal StartInput { get; set; }
        public decimal EndInput { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public long UserID { get; set; }
        public DateTime CreationDate { get; set; }
    }
}