﻿using System;

namespace GabAPI.Models.Inputs.KarKard
{
    public class InputGetKarkardDetail
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long ComplexID { get; set; }
    }
}