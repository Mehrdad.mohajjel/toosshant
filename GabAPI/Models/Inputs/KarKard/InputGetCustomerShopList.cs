﻿namespace GabAPI.Models.Inputs.KarKard
{
    public class InputGetCustomerShopList
    {
        public long CustomerID { get; set; }
    }
}