﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GabAPI.Models.Inputs.Price
{
    public class InputAddPrice
    {
        public decimal Price { get; set; }
    }
}