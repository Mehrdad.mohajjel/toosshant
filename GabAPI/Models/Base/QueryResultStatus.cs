﻿
using GabzApi.Repositories.Implements.ADONET;

namespace GabzApi.Models.Base
{
    public static class QueryResultStatus
    {
        public static int StatusSuccess
        {
            get
            {
                return 1;
            }
        }

        public static int ReturnSuccess
        {
            get
            {
                return 1;
            }
        }

        public static bool IsQueryresultValid(QueryResult queryResult)
        {
            var isResultValid = queryResult.ReturnCode == ReturnSuccess &&
                                queryResult.SPCode == StatusSuccess;
            return isResultValid;
        }
    }
}