﻿using System;

namespace GabzApi.Models.DomainModels
{
    public class Tbl_MachineStop
    {
        public long MachineStopID { get; set; }
        public int DepartmentID { get; set; }
        public long StopCodeType { get; set; }
        public long StopCodeID { get; set; }
        public decimal StopCost { get; set; }
        public long DisposalID { get; set; }
        public long WorkOrderSerial { get; set; }
        public string Description { get; set; }
        public DateTime CreationDateTime { get; set; }
        public bool IsDeleted { get; set; }

    }
}