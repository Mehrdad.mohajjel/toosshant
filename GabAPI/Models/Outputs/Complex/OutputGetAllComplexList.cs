﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GabAPI.Models.Outputs.Complex
{
    public class OutputGetAllComplexList
    {
        public long ComplexId { get; set; }
        public string ComplexTitle { get; set; }
        public int ShopCount { get; set; }
    }
}