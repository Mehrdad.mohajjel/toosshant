﻿using System;

namespace GabAPI.Models.Outputs.User
{
    public class OutputGetAllUserList
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Mobile { get; set; }
        public String EmailAddress { get; set; }
        public string NickName { get; set; }
        public string StatusTitle { get; set; }
        public int Status { get; set; }
    }
}