﻿using System;

namespace GabAPI.Models.Outputs.CustomerShop
{
    public class OutputGetCustomerShopListByCustomertID
    {
        public long CustomerShopID { get; set; }
        public long CustomerID { get; set; }
        public long ShopID { get; set; }
        public string ComplexTitle { get; set; }
        public string TabloName { get; set; }
        public string Pelak { get; set; }
        public string CustomerName { get; set; }
        public string CreationDate { get; set; }
        public string StatusTitle { get; set; }
    }
}