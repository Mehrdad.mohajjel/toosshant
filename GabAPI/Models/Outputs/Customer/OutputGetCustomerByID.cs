﻿namespace GabAPI.Models.Outputs.Customer
{
    public class OutputGetCustomerByID
    {
        public long Id { get; set; }
        public long CustomerID { get; set; }
        public string TabloName { get; set; }
        public string Pelak { get; set; }
        public bool IsActive { get; set; }
    }
}