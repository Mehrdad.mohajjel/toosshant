﻿namespace GabzApi.Models.Outputs.Customer
{
    public class OutputGetLastCode
    {
        public string LastCode { get; set; }
    }
}