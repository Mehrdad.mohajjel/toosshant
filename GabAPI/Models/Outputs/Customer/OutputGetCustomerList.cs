﻿using System;

namespace GabAPI.Models.Outputs.Customer
{
    public class OutputGetCustomerList
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string lastName { get; set; }
        public string Mobile { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string IsActiveTitle { get; set; }
        public System.DateTime CreationDate { get; set; }
        public string CreationFaDate { get; set; }
        public string CustomerType { get; set; }

    }
}