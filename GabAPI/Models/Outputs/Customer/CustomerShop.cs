﻿namespace GabAPI.Models.Outputs.Customer
{
    public class CustomerShop
    {
        public long Id { get; set; }
        public long CustomerID { get; set; }
        public string TabloName { get; set; }
        public string Pelak { get; set; }
    }
}