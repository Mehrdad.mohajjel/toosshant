﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GabAPI.Models.Outputs.Price
{
    public class OutputGetAllPriceList
    {
        public long PriceId { get; set; }
        public decimal BasePrice { get; set; }
        public string CreationFaDate { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
    }
}