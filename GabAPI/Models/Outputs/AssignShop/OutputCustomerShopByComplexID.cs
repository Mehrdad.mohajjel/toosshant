﻿namespace GabAPI.Models.Outputs.AssignShop
{
    public class OutputCustomerShopByComplexID
    {
        public long ShopID { get; set; }
        public long CustomerShopID { get; set; }
        public long ComplexId { get; set; }
        public string Pelak { get; set; }
        public string ComplexTitle { get; set; }
        public string Tablo { get; set; }
        public string CustomerName { get; set; }
        public string CreationDate { get; set; }
    }
}