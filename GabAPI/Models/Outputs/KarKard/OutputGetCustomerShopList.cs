﻿namespace GabAPI.Models.Outputs.KarKard
{
    public class OutputGetCustomerShopList
    {
        public long CustomerShopID { get; set; }
        public long CustomerID { get; set; }
        public string TabloName { get; set; }
        public string Pelak { get; set; }
    }
}