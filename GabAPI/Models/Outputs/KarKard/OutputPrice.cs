﻿namespace GabAPI.Models.Outputs.KarKard
{
    public class OutputPrice
    {
        public long PriceID { get; set; }
        public decimal BasePrice { get; set; }
        public string CreationDateTime { get; set; }
    }
}