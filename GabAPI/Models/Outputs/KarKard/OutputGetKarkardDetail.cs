﻿namespace GabAPI.Models.Outputs.KarKard
{
    public class OutputGetKarkardDetail
    {
        public long Id { get; set; }
        public long CustomerShopID { get; set; }
        public string CustomerName { get; set; }
        public string Tablo { get; set; }
        public string Pelak { get; set; }
        public string ComplexTitle { get; set; }
        public long  PriceId { get; set; }
        public decimal BasePrice { get; set; }
        public string  StartDate { get; set; }
        public decimal StartCoutValue { get; set; }
        public string EndDate { get; set; }
        public decimal EndCoutValue { get; set; }
        public decimal TotalCount { get; set; }
        public decimal TotalPrice { get; set; }
        public string UserName { get; set; }
    }
}