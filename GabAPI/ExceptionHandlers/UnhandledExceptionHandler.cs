﻿using MotorsazanMachineryApiApp.HttpActionResults;
using System.Web.Http.ExceptionHandling;
using GabzApi.Utilities;

namespace MotorsazanMachineryApiApp.ExceptionHandlers
{
    public class UnhandledExceptionHandler: ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            var exceptionMessage = context.Exception.GetBaseException().Message.ToString();
#if DEBUG
            var debugContent = Newtonsoft.Json.JsonConvert.SerializeObject(context.Exception);
#else
            var debugContent = @"{ ""Message"" : exceptionMessage }";
#endif
            Log.Fatal(context.Exception.ToString(), 0);


            context.Result = new ErrorContentResult(debugContent, "application/json", context.Request);
        }
    }
}