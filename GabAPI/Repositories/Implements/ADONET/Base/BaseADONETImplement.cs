﻿using GabzApi.Repositories.Interface;
using GabzApi.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace GabzApi.Repositories.Implements.ADONET
{
    public class BaseADONETImplement<T>: DataBase, IBaseRepository<T>
    {
        public BaseADONETImplement(string KeyFieldName)
        {
            this.KeyFieldName = KeyFieldName;
        }

        public string KeyFieldName { get; }

        public bool Add(T item)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var Result = false;
                var DataTable = item.ToDataTable();
                var Columns = string.Join(",", DataTable.Columns.Cast<DataColumn>().Where(c => c.ColumnName != KeyFieldName).Select(c => c.ColumnName));
                var Values = string.Join(",", DataTable.Columns.Cast<DataColumn>().Where(c => c.ColumnName != KeyFieldName).Select(c => string.Format("@{0}", c.ColumnName)));
                var TableName = Tools.GetGenericName<T>(); // نام جدول از روی نام جنریک اخذ می شود
                var sqlCommandInsert = string.Format("INSERT INTO {0}({1}) VALUES ({2})", TableName, Columns, Values);

                using(var connection = new SqlConnection(DataBaseConfig.ConnectionString))
                using(var command = new SqlCommand(sqlCommandInsert, connection))
                {
                    connection.Open();
                    foreach(DataRow DR in DataTable.Rows)
                    {
                        command.Parameters.Clear();
                        foreach(DataColumn col in DataTable.Columns)
                        {
                            _ = command.Parameters.AddWithValue("@" + col.ColumnName, DR[col]);
                        }
                        var inserted = command.ExecuteNonQuery();
                        if(inserted > 0)
                        {
                            Result = true;
                        }
                    }
                }

                Log.Trace("ثبت با موفقیت انجام شد", sw.Elapsed.TotalMilliseconds);
                return Result;
            }
            catch(Exception ex)
            {
                Log.Fatal(ex.ToString(), sw.Elapsed.TotalMilliseconds);
                return false;
            }
        }

        public bool Add(IEnumerable<T> itemList)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                using(var connection = new SqlConnection(DataBaseConfig.ConnectionString))
                {
                    connection.Open();
                    var transaction = connection.BeginTransaction();

                    using(var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        bulkCopy.BatchSize = 100;
                        bulkCopy.DestinationTableName = Tools.GetGenericName<T>(); // نام جدول از روی نام جنریک اخذ می شود
                        try
                        {
                            bulkCopy.WriteToServer(itemList.ToDataTable());
                        }
                        catch(Exception)
                        {
                            transaction.Rollback();
                            connection.Close();
                            return false;
                        }
                    }

                    transaction.Commit();
                    Log.Trace("ثبت با موفقیت انجام شد", sw.Elapsed.TotalMilliseconds);
                    return true;
                }

            }
            catch(Exception ex)
            {
                Log.Fatal(ex.ToString(), sw.Elapsed.TotalMilliseconds);
                return false;
            }
        }

        public bool Delete(long itemId)
        {
            var sw = Stopwatch.StartNew();
            using(var connection = new SqlConnection(DataBaseConfig.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    var Result = false;
                    var commandText = string.Format("DELETE FROM {0} WHERE {1} = {2}", Tools.GetGenericName<T>(), KeyFieldName, itemId);

                    using(var command = new SqlCommand(commandText, connection) { CommandTimeout = DataBaseConfig.CommandTimeout, CommandType = CommandType.Text, Transaction = transaction })
                    {
                        var RowsAffectedCount = command.ExecuteNonQuery();
                        if(RowsAffectedCount > 0)
                        {
                            Result = true;
                        }
                    }

                    transaction.Commit();
                    Log.Trace("حذف با موفقیت انجام شد", sw.Elapsed.TotalMilliseconds);
                    return Result;
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    Log.Fatal(ex.ToString(), sw.Elapsed.TotalMilliseconds);
                    return false;
                }
            }
        }

        public bool Delete(IEnumerable<long> itemIdList)
        {
            var sw = Stopwatch.StartNew();
            var Result = false;
            var List = string.Join(",", itemIdList);
            var commandText = string.Format("DELETE FROM {0} WHERE {1} IN ({2})", Tools.GetGenericName<T>(), KeyFieldName, List);
            using(var connection = new SqlConnection(DataBaseConfig.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    using(var command = new SqlCommand(commandText, connection) { CommandTimeout = DataBaseConfig.CommandTimeout, CommandType = CommandType.Text, Transaction = transaction })
                    {
                        var RowsAffectedCount = command.ExecuteNonQuery();
                        if(RowsAffectedCount > 0)
                        {
                            Result = true;
                        }
                    }

                    transaction.Commit();
                    Log.Trace("حذف با موفقیت انجام شد", sw.Elapsed.TotalMilliseconds);
                    return Result;
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    Log.Fatal(ex.ToString(), sw.Elapsed.TotalMilliseconds);
                    return false;
                }
            }
        }

        public bool Delete(IEnumerable<T> itemList)
        {
            var List = new List<long>();
            foreach(var item in itemList)
            {
                var ItemKeyValue = Tools.GetGenericObjectSpecialFieldValue<T>(item, KeyFieldName);
                if(ItemKeyValue != null)
                {
                    List.Add(Convert.ToInt64(ItemKeyValue));
                }
            }

            return List.Count > 0 ? Delete(List) : false;
        }

        public bool Delete(T item)
        {
            var ItemKeyValue = Tools.GetGenericObjectSpecialFieldValue<T>(item, KeyFieldName);
            return ItemKeyValue != null ? Delete(Convert.ToInt64(ItemKeyValue)) : false;
        }

        public bool Edit(T item)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var Result = false;
                var DataTable = item.ToDataTable();
                var Columns = string.Join(",", DataTable.Columns.Cast<DataColumn>().Where(c => c.ColumnName != KeyFieldName).Select(c => string.Format("{0} = @{0}", c.ColumnName)));
                var TableName = Tools.GetGenericName<T>(); // نام جدول از روی نام جنریک اخذ می شود
                var ItemKeyValue = Tools.GetGenericObjectSpecialFieldValue<T>(item, KeyFieldName);
                if(ItemKeyValue != null)
                {
                    var sqlCommandInsert = string.Format("UPDATE {0} SET {1} WHERE {2} = {3}", TableName, Columns, KeyFieldName, ItemKeyValue);

                    using(var connection = new SqlConnection(DataBaseConfig.ConnectionString))
                    using(var command = new SqlCommand(sqlCommandInsert, connection))
                    {
                        connection.Open();
                        foreach(DataRow DR in DataTable.Rows)
                        {
                            command.Parameters.Clear();
                            foreach(DataColumn col in DataTable.Columns)
                            {
                                _ = command.Parameters.AddWithValue("@" + col.ColumnName, DR[col]);
                            }
                            var updated = command.ExecuteNonQuery();
                            if(updated > 0)
                            {
                                Result = true;
                            }
                        }
                    }
                }

                Log.Trace("ویرایش با موفقیت انجام شد", sw.Elapsed.TotalMilliseconds);
                return Result;
            }
            catch(Exception ex)
            {
                Log.Fatal(ex.ToString(), sw.Elapsed.TotalMilliseconds);
                return false;
            }
        }

        public IEnumerable<T> GetAll()
        {
            var items = new List<T>();
            var sw = Stopwatch.StartNew();
            try
            {
                var commandText = "SELECT * FROM " + Tools.GetGenericName<T>();
                using(var connection = new SqlConnection(DataBaseConfig.ConnectionString))
                {
                    using(var command = new SqlCommand(commandText, connection) { CommandTimeout = DataBaseConfig.CommandTimeout, CommandType = CommandType.Text })
                    {
                        if(connection.State != ConnectionState.Open)
                        {
                            connection.Open();
                        }

                        using(var record = command.ExecuteReader())
                        {
                            while(record.Read())
                            {
                                items.Add(Tools.Map<T>(record));
                            }
                        }
                    }
                }

                Log.Trace("دریافت لیست با موفقیت انجام شد", sw.Elapsed.TotalMilliseconds);
                return items;
            }
            catch(Exception ex)
            {
                Log.Fatal(ex.ToString(), sw.Elapsed.TotalMilliseconds);
                return items;
            }
        }

        public T GetSpecial(long itemId)
        {
            var sw = Stopwatch.StartNew();
            var ResultItem = (T)Activator.CreateInstance(typeof(T), new object[] { });
            try
            {
                var commandText = string.Format("SELECT TOP(1) * FROM {0} WHERE {1} = {2}", Tools.GetGenericName<T>(), KeyFieldName, itemId);
                using(var connection = new SqlConnection(DataBaseConfig.ConnectionString))
                {
                    using(var command = new SqlCommand(commandText, connection) { CommandTimeout = DataBaseConfig.CommandTimeout, CommandType = CommandType.Text })
                    {
                        if(connection.State != ConnectionState.Open)
                        {
                            connection.Open();
                        }

                        using(var record = command.ExecuteReader())
                        {
                            while(record.Read())
                            {
                                ResultItem = Tools.Map<T>(record);
                            }
                        }
                    }
                }

                Log.Trace("دریافت اطلاعات با موفقیت انجام شد", sw.Elapsed.TotalMilliseconds);
                return ResultItem;
            }
            catch(Exception ex)
            {
                Log.Fatal(ex.ToString(), sw.Elapsed.TotalMilliseconds);
                return ResultItem;
            }
        }

    }
}