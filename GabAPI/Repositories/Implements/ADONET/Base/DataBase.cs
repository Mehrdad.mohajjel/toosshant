﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace GabzApi.Repositories.Implements.ADONET
{
    public class DataBase
    {
        public static QueryResult ExecuteStoredProcedure(string storedProcedureName, List<SqlParameter> sqlParameters)
        {
            sqlParameters.Add(new SqlParameter { ParameterName = "@output_status", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
            sqlParameters.Add(new SqlParameter { ParameterName = "@output_message", SqlDbType = SqlDbType.NVarChar, Size = 4000, Direction = ParameterDirection.Output });
            sqlParameters.Add(new SqlParameter { ParameterName = "@returnvalue", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.ReturnValue });

            return Execute(storedProcedureName, CommandType.StoredProcedure, sqlParameters.ToArray());
        }

        public static QueryResult Execute(string commandText, CommandType commandType, SqlParameter[] parameters)
        {
            QueryResult result;
            var dS = new DataSet();
            var returnCodeID = parameters.Length - 1;
            var resultTextID = parameters.Length - 2;
            var spResultCodeID = parameters.Length - 3;

            try
            {
                using(var connection = new SqlConnection(DataBaseConfig.ConnectionString))
                using(var command = new SqlCommand(commandText, connection) { CommandTimeout = DataBaseConfig.CommandTimeout, CommandType = commandType })
                using(var dataAdaptor = new SqlDataAdapter(command))
                {
                    command.Parameters.AddRange(parameters);

                    connection.Open();
                    _ = dataAdaptor.Fill(dS);
                }

                if((int)parameters[returnCodeID].Value != 1)
                {
                    result = new QueryResult { ReturnCode = (int)parameters[returnCodeID].Value, Text = (string)parameters[resultTextID].Value };
                    return result;
                }
                if((int)parameters[spResultCodeID].Value != 1)
                {
                    result = new QueryResult { SPCode = (int)parameters[spResultCodeID].Value, ReturnCode = (int)parameters[returnCodeID].Value, Text = (string)parameters[resultTextID].Value };
                    return result;
                }
                result = new QueryResult { DataSet = dS, SPCode = (int)parameters[spResultCodeID].Value, ReturnCode = (int)parameters[returnCodeID].Value, Text = (string)parameters[resultTextID].Value };
                return result;
            }
            catch(Exception ex)
            {
                result = new QueryResult { Text = ex.Message.ToString(), ReturnCode = ex.HResult };
                return result;
            }
        }
    }
}