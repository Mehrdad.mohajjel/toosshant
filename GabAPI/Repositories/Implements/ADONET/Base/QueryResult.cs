﻿using System.Data;

namespace GabzApi.Repositories.Implements.ADONET
{
    public class QueryResult
    {
        public int SPCode { get; set; }

        public string Text { get; set; }

        public int ReturnCode { get; set; }

        public DataSet DataSet { get; set; }
    }
}