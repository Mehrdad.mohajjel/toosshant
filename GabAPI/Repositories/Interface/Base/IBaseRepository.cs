﻿using System.Collections.Generic;

namespace GabzApi.Repositories.Interface
{
    public interface IBaseRepository<T>
    {
        bool Add(T item);

        bool Add(IEnumerable<T> itemList);

        bool Edit(T item);

        bool Delete(T item);

        bool Delete(IEnumerable<long> itemIdList);

        bool Delete(IEnumerable<T> itemList);

        bool Delete(long itemId);

        T GetSpecial(long itemId);

        IEnumerable<T> GetAll();
    }
}
