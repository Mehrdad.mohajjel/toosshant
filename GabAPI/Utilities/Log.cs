﻿using NLog;
using System.Linq;
using System.Runtime.CompilerServices;

namespace GabzApi.Utilities
{
    public static class Log
    {
        public static Logger Logger { get; private set; }

        [System.Obsolete]
        static Log()
        {
            var logClasType = typeof(Log);
            var domain = logClasType.Namespace.ToString().Split('.')[0];

            var mainDirectory = @"C:\Motorsazan" + @"\" + domain + @"\";
            var logConfigFilePath = mainDirectory + @"Configure\Log.config";

            LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(logConfigFilePath, true);
            Logger = LogManager.GetLogger("*");

        }

        private static string Body(string message)
        {
            return "[" + message + "]";
        }

        private static string ExecutionTime(double time)
        {
            return "[" + time + "]";
        }

        private static string Consumer(string userName = "")
        {
            _ = userName.ToString();
            //if (string.IsNullOrEmpty(userName) == true)
            //    return "[]";

            //var wsun = "(Web Service User Name: " + userName + ")";
            //var iP = "(IP Address: " + Converter.GetIpAddress() + ")";
            //var hostName = "(Host Name: " + Converter.GetHostName() + ")";
            //var userAgent = "(User Agent: " + Converter.GetUserAgent() + ")";
            //var calledUrl = "(Called URL: " + Converter.GetCalledUrl() + ")";

            //return "[" + wsun + iP + hostName + userAgent + calledUrl + "]";
            return "";
        }

        private static string CurrentMethod(string sourceFilePath, string memberName, int sourceLineNumber)
        {
            sourceFilePath = sourceFilePath.Contains('\\') ? sourceFilePath.Substring(sourceFilePath.LastIndexOf('\\') + 1) : sourceFilePath;
            sourceFilePath = sourceFilePath.Contains('.') ? sourceFilePath.Substring(0, sourceFilePath.IndexOf('.')) : sourceFilePath;

            return "[" + sourceFilePath + "." + memberName + " line:" + sourceLineNumber + "]";
        }

        private static string Fromat(string message, double time, string userName, string sourceFilePath, string memberName, int sourceLineNumber)
        {
            var currentMethod = CurrentMethod(sourceFilePath, memberName, sourceLineNumber);
            var executiontime = ExecutionTime(time);
            var consumer = Consumer(userName);
            var body = Body(message);

            return executiontime + ";" + currentMethod + ";" + consumer + ";" + body;
        }


        public static void Trace(string message, double time, string userName = "", [CallerFilePath] string sourceFilePath = "", [CallerMemberName] string memberName = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Trace(Fromat(message, time, userName, sourceFilePath, memberName, sourceLineNumber));
        }

        public static void Debug(string message, double time = 0, string userName = "", [CallerFilePath] string sourceFilePath = "", [CallerMemberName] string memberName = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Debug(Fromat(message, time, userName, sourceFilePath, memberName, sourceLineNumber));
        }

        public static void Info(string message, double time, string userName = "", [CallerFilePath] string sourceFilePath = "", [CallerMemberName] string memberName = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Info(Fromat(message, time, userName, sourceFilePath, memberName, sourceLineNumber));
        }

        public static void Warn(string message, double time, string userName = "", [CallerFilePath] string sourceFilePath = "", [CallerMemberName] string memberName = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Warn(Fromat(message, time, userName, sourceFilePath, memberName, sourceLineNumber));
        }

        public static void Error(string message, double time, string userName = "", [CallerFilePath] string sourceFilePath = "", [CallerMemberName] string memberName = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Error(Fromat(message, time, userName, sourceFilePath, memberName, sourceLineNumber));
        }

        public static void Fatal(string message, double time, string userName = "", [CallerFilePath] string sourceFilePath = "", [CallerMemberName] string memberName = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Fatal(Fromat(message, time, userName, sourceFilePath, memberName, sourceLineNumber));
        }
    }
}