﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace GabzApi.Utilities
{
    public class WebApiConsumer<T>
    {
        readonly HttpClient client;
        private string BaseURL { get; set; }
        public WebApiConsumer(string _baseURL)
        {
            BaseURL = _baseURL;
            client = new HttpClient
            {
                BaseAddress = new Uri(_baseURL)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<T> Get(string Params)
        {
            var responseMessage = await client.GetAsync(BaseURL + "?" + Params).ConfigureAwait(false);
            var result = (T)Activator.CreateInstance(typeof(T), new object[] { });

            if(!responseMessage.IsSuccessStatusCode)
            {
                return result;
            }

            var responseData = responseMessage.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<T>(responseData);
        }

        [HttpPost]
        public async Task<bool> Add(T item)
        {
            var responseMessage = await client.PostAsJsonAsync(BaseURL, item);
            return responseMessage.IsSuccessStatusCode;
        }

        [HttpPut]
        public async Task<bool> Edit(int id, T item)
        {
            var responseMessage = await client.PutAsJsonAsync(BaseURL + "/" + id, item);
            return responseMessage.IsSuccessStatusCode;
        }

        [HttpDelete]
        public async Task<bool> Delete(int id)
        {
            var responseMessage = await client.DeleteAsync(BaseURL + "/" + id);
            return responseMessage.IsSuccessStatusCode;
        }
    }
}