﻿using System;
using System.Web;

namespace GabzApi.Utilities
{
    public static class UserTools
    {
        public static int? GetOnlineUser()
        {
            var sessionKey = "OnlineUserId";
            var currentSessionValue = HttpContext.Current.Session[sessionKey];
            return currentSessionValue != null ? Convert.ToInt32(currentSessionValue) : 0;
        }

        public static bool HasAccessToForm(int? userId, string formCode)
        {
            try
            {

                //if (isResponseValid)
                //{
                //    return false;
                //}

                //var result = response.Result.Params.HasAccessToPage;
                //return result != null ? result.HassAccess : false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}