﻿using BLL;
using COMMON;
using GabAPI.Models.Inputs.Customer;
using GabAPI.Models.Inputs.KarKard;
using GabAPI.Models.Outputs.Customer;
using GabAPI.Models.Outputs.KarKard;
using GabzApi.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

namespace GabzApi.Business
{
    public class CustomerBusiness
    {
        public void AddCustomer(Customer values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                values.CreationDate = DateTime.Now;
                values.IsActive = true;
               var serviceResult = CustomerService.Add(values);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }
        public void AddNewExpence(InputaddExpence values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = new ShopCounter()
                {
                    CreationDate = DateTime.Now,
                    CustomerShopID = values.CustomerShopID,
                    EndCoutValue = values.EndInput,
                    EndDate = Tools.ConvertToLatinDate(values.EndDate),
                    StartCoutValue = values.StartInput,
                    StartDate = Tools.ConvertToLatinDate(values.StartDate),
                    PriceId = values.PriceId,
                    UserId = values.UserID


                };
                var serviceResult = ShopCounterService.Add(item);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public OutputGetCustomerList[] GetAllCustomerList()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = CustomerService.GetALL().OrderByDescending(a => a.Id).Where(a => a.IsActive == true).ToList();
                var finalResult = new OutputGetCustomerList[item.Count];

                for (var i = 0; i < item.Count; i++)
                {
                    var tempDataRow = item[i];
                    finalResult[i] = new OutputGetCustomerList
                    {
                        Id = Convert.ToInt64(tempDataRow.Id),
                        Code = tempDataRow.Code.ToString(),
                        CreationDate = tempDataRow.CreationDate,
                        CreationFaDate = Tools.ToShamsiDateString(tempDataRow.CreationDate),
                        FirstName = tempDataRow.FirstName.ToString(),
                        IsActive = Convert.ToBoolean(tempDataRow.IsActive),
                        IsActiveTitle = Convert.ToBoolean(tempDataRow.IsActive) ? "فعال" : "غیر فعال",
                        lastName = tempDataRow.lastName,
                        Mobile = tempDataRow.Mobile,
                        CustomerType = DBNull.Value.Equals(tempDataRow.CustomerTypeId) ? "" : tempDataRow.CustomerType.Title.ToString()
                    };
                }
                return finalResult;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

        public OutputGetCustomerList[] GetCustomerList()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = CustomerService.GetALL().OrderByDescending(a => a.Id).ToList();
                var finalResult = new OutputGetCustomerList[item.Count];

                for (var i = 0; i < item.Count; i++)
                {
                    var tempDataRow = item[i];
                    finalResult[i] = new OutputGetCustomerList
                    {
                        Id = Convert.ToInt64(tempDataRow.Id),
                        Code = tempDataRow.Code.ToString(),
                        CreationDate = tempDataRow.CreationDate,
                        CreationFaDate = Tools.ToShamsiDateString(tempDataRow.CreationDate),
                        FirstName = tempDataRow.FirstName.ToString(),
                        IsActive = Convert.ToBoolean(tempDataRow.IsActive),
                        IsActiveTitle = Convert.ToBoolean(tempDataRow.IsActive) ? "فعال" : "غیر فعال",
                        lastName = tempDataRow.lastName,
                        Mobile = tempDataRow.Mobile,
                        CustomerType = DBNull.Value.Equals(tempDataRow.CustomerTypeId) ? "" : tempDataRow.CustomerType.Title.ToString()
                    };
                }
                return finalResult;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

        public string GetLastCode()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var LstCode = CustomerService.GetALL().OrderByDescending(a => a.Id).FirstOrDefault().Code.ToString();
                return LstCode;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

        public OutputGetCustomerByID[] GetCustomerDetailByID(InputGetCustomerByID values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                List<COMMON.CustomerShop> result = CustomerShopSevice.GetALL().Where(a => a.CustomerID == values.CustomerID).ToList();

                var canParseResult = result.Count() > 0 ? true : false;
                if (!canParseResult)
                {
                    return new OutputGetCustomerByID[0];
                }
                else
                {
                    OutputGetCustomerByID[] finalResult = new OutputGetCustomerByID[result.Count()];
                    for (int i = 0; i < result.Count(); i++)
                    {
                        finalResult[i] = new OutputGetCustomerByID();
                        finalResult[i].Id = result[i].Id;
                        finalResult[i].IsActive = Convert.ToBoolean(result[i].Customer.IsActive);
                    }
                    return finalResult;
                }


            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public CustomerType[] GetAllCustomeTypeList()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                List<CustomerType> result = CustomerTypeService.GetALL().Where(a => a.IsActive == true).ToList();

                var canParseResult = result.Count() > 0 ? true : false;
                if (!canParseResult)
                {
                    return new CustomerType[0];
                }
                else
                {
                    CustomerType[] finalResult = new CustomerType[result.Count()];
                    for (int i = 0; i < result.Count(); i++)
                    {
                        CustomerType item = new CustomerType()
                        {
                            ID = result[i].ID,
                            Title = result[i].Title.ToString()

                        };
                        finalResult[i] = new CustomerType();
                        finalResult[i].ID = item.ID;
                        finalResult[i].Title = item.Title.ToString();
                    }
                    return finalResult;
                }


            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public OutputPrice[] GetActivePriceList()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                List<Price> result = PriceService.GetALL().Where(a => a.IsActive == true).ToList();

                var canParseResult = result.Count() > 0 ? true : false;
                if (!canParseResult)
                {
                    return new OutputPrice[0];
                }
                else
                {
                    OutputPrice[] finalResult = new OutputPrice[result.Count()];
                    for (int i = 0; i < result.Count(); i++)
                    {
                        OutputPrice item = new OutputPrice()
                        {
                            PriceID = result[i].Id,
                            BasePrice = Convert.ToDecimal(result[i].BasePrice),
                            CreationDateTime = Tools.ToShamsiDateString(result[i].CreationDate),

                        };
                        finalResult[i] = new OutputPrice();
                        finalResult[i].PriceID = item.PriceID;
                        finalResult[i].BasePrice = Convert.ToDecimal(item.BasePrice);
                        finalResult[i].CreationDateTime = item.CreationDateTime;
                    }
                    return finalResult;
                }


            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }
        public void RemoveCustomer(Customer values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = new Customer();
                item.Id = values.Id;
                item = CustomerService.GetSpecial(item);
                item.IsActive = false;
                var serviceResult = CustomerService.Edit(item);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public OutputGetKarkardDetail[] GetExpenceDetail(InputGetKarkardDetail values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                DataTable result = StoreProcedureService.GetExpenceDetail(values.StartDate, values.EndDate,values.ComplexID);
                var finalResult = new OutputGetKarkardDetail[result.Rows.Count];

                for (var i = 0; i < result.Rows.Count; i++)
                {
                    var tempDataRow = result.Rows[i];
                    finalResult[i] = new OutputGetKarkardDetail
                    {
                        Id = Convert.ToInt64(tempDataRow.ItemArray[0]),
                        CustomerShopID = Convert.ToInt64(tempDataRow.ItemArray[1]),
                        CustomerName = tempDataRow.ItemArray[3].ToString(),
                        Tablo = tempDataRow.ItemArray[4].ToString(),
                        Pelak = tempDataRow.ItemArray[5].ToString(),
                        ComplexTitle = tempDataRow.ItemArray[6].ToString(),
                        BasePrice = Convert.ToDecimal(Tools. Rial(tempDataRow.ItemArray[8].ToString())),
                        StartDate = tempDataRow.ItemArray[9].ToString(),
                        StartCoutValue = Convert.ToDecimal(tempDataRow.ItemArray[10]),
                        EndDate = tempDataRow.ItemArray[11].ToString(),
                        EndCoutValue = Convert.ToDecimal(tempDataRow.ItemArray[12]),
                        TotalCount = Convert.ToDecimal(tempDataRow.ItemArray[13]),
                        TotalPrice = Convert.ToDecimal(Tools.Rial(tempDataRow.ItemArray[14].ToString())),
                        UserName = tempDataRow.ItemArray[15].ToString()
                    };
                }
                return finalResult;
            }
            catch (Exception ex)
            {
                //Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

    }
}