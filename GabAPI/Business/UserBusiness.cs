﻿using BLL;
using COMMON;
using GabAPI.Models.Inputs.User;
using GabAPI.Models.Outputs.User;
using GabzApi.Utilities;
using System;
using System.Diagnostics;
using System.Linq;

namespace GabAPI.Business
{
    public class UserBusiness
    {
        public void AddUser(User values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var serviceResult = UserService.Add(values);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public OutputGetAllUserList[] GetAllUserList()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var serviceResult = UserService.GetIQueryableAllData().OrderByDescending(a => a.Id).ToList();
                var serviceResultRows = serviceResult;
                var canParseResult = serviceResultRows != null &&
                                     serviceResultRows.Count != 0;
                if (!canParseResult)
                {
                    return new OutputGetAllUserList[0];
                }

                var rowsCount = serviceResultRows.Count;
                var finalResult = new OutputGetAllUserList[rowsCount];
                for (var i = 0; i < rowsCount; i++)
                {
                    var tempDataRow = serviceResultRows[i];
                    finalResult[i] = new OutputGetAllUserList();
                    finalResult[i].Id = Convert.ToInt64(tempDataRow.Id);
                    finalResult[i].EmailAddress = string.IsNullOrEmpty(tempDataRow.EmailAddress) ? "-" : tempDataRow.EmailAddress.ToString();
                    finalResult[i].Mobile = string.IsNullOrEmpty(tempDataRow.Mobile) ? "-" : tempDataRow.Mobile.ToString();
                    finalResult[i].NickName = string.IsNullOrEmpty(tempDataRow.NickName) ? "-" : tempDataRow.NickName.ToString();
                    finalResult[i].Password = string.IsNullOrEmpty(tempDataRow.Password) ? "" : tempDataRow.Password.ToString();
                    finalResult[i].StatusTitle = tempDataRow.Status1.ShowName.ToString();
                    finalResult[i].UserName = tempDataRow.UserName.ToString();
                    finalResult[i].Status = tempDataRow.Status.Value;

                }

                return finalResult;

            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

        public void ResetPassword(InputResetPassword values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = new User();
                item.Id = values.UserID;
                item = UserService.GetSpecial(item);
                item.Password = Tools.HashString("123");
                var serviceResult = UserService.Edit(item);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public void ChangeStatus(InputChangeStatus values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = new User();
                item.Id = values.UserID;
                item = UserService.GetSpecial(item);
                if(item.Status ==  (byte)SystemEnumsList.UserStatus.Disabled )
                {
                    item.Status = (byte)SystemEnumsList.UserStatus.Active;
                }
                else if (item.Status == (byte)SystemEnumsList.UserStatus.Active)
                {
                    item.Status = (byte)SystemEnumsList.UserStatus.Disabled;

                }
                else if((item.Status == (byte)SystemEnumsList.UserStatus.Removed))
                {
                    item.Status = (byte)COMMON.SystemEnumsList.UserStatus.Active;

                }
                var serviceResult = UserService.Edit(item);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public void RemoveUser(InputRemoveUser values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = new User();
                item.Id = values.UserID;
                item = UserService.GetSpecial(item);
                 item.Status = (byte)SystemEnumsList.UserStatus.Removed;
                var serviceResult = UserService.Edit(item);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        
    }
}