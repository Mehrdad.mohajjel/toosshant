﻿using BLL;
using COMMON;
using GabAPI.Models.Inputs.Price;
using GabAPI.Models.Outputs.Price;
using GabzApi.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace GabAPI.Business
{
    public class PriceBusiness
    {
        public void AddPrice(Price values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var serviceResult = PriceService.Add(values);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public OutputGetAllPriceList[] GetAllPriceList()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var serviceResult = PriceService.GetIQueryableAllData().OrderByDescending(a => a.Id).ToList();
                var serviceResultRows = serviceResult;
                var canParseResult = serviceResultRows != null &&
                                     serviceResultRows.Count != 0;
                if (!canParseResult)
                {
                    return new OutputGetAllPriceList[0];
                }

                var rowsCount = serviceResultRows.Count;
                var finalResult = new OutputGetAllPriceList[rowsCount];
                for (var i = 0; i < rowsCount; i++)
                {
                    var tempDataRow = serviceResultRows[i];
                    finalResult[i] = new OutputGetAllPriceList();
                    finalResult[i].PriceId = Convert.ToInt64(tempDataRow.Id);
                    finalResult[i].BasePrice =  Convert.ToDecimal(tempDataRow.BasePrice);
                    finalResult[i].Status = Convert.ToBoolean(tempDataRow.IsActive) ? "فعال" : "غیرفعال";
                    finalResult[i].CreationFaDate = Tools.ToShamsiDateString(tempDataRow.CreationDate);
                    finalResult[i].IsActive = Convert.ToBoolean(tempDataRow.IsActive);
                }

                return finalResult;

            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

        public void ChageStatus(InputEditPrice values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = new Price();
                item.Id = values.PriceID;
                item = PriceService.GetSpecial(item);
                if(item.IsActive == true)
                {
                    item.IsActive = false;
                }
                else
                {
                    item.IsActive = true;
                }
                var serviceResult = PriceService.Edit(item);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }
    }
}