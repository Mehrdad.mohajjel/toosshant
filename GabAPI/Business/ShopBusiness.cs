﻿using BLL;
using COMMON;
using GabAPI.Models.Inputs.CustomerShop.AssignShop;
using System;
using System.Diagnostics;
using System.Linq;

namespace GabAPI.Business
{
    public class ShopBusiness
    {
        public Shop[] GetShopListByComplexID(InputGetShopListByComplexID values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var serviceResult = StoreProcedureService.GetShopListByComplexID(values.ComplexID);
                var serviceResultRows = serviceResult.Rows;
                var canParseResult = serviceResultRows != null &&
                                     serviceResultRows.Count != 0;
                if (!canParseResult)
                {
                    return new Shop[0];
                }

                var rowsCount = serviceResultRows.Count;
                var finalResult = new Shop[rowsCount];
                for (var i = 0; i < rowsCount; i++)
                {
                    var tempDataRow = serviceResultRows[i];
                    finalResult[i] = new Shop
                    {
                        Id = Convert.ToInt64(tempDataRow.ItemArray[0]),
                        ComplexId = Convert.ToInt64(tempDataRow.ItemArray[1]),
                        Pelak = tempDataRow.ItemArray[2].ToString(),
                    };
                }

                return finalResult;

            }
            catch (Exception ex)
            {
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

    }
}