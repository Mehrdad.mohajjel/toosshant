﻿using BLL;
using GabAPI.Models.Inputs.AssignShop;
using GabAPI.Models.Inputs.CustomerShop;
using GabAPI.Models.Inputs.CustomerShop.AssignPeopleToLine;
using GabAPI.Models.Outputs.AssignShop;
using GabAPI.Models.Outputs.CustomerShop;
using GabzApi.Utilities;
using System;
using System.Diagnostics;
using System.Linq;

namespace GabAPI.Business
{
    public class CustomerShopBusiness
    {
        public OutputGetCustomerShopListByCustomertID[] GetCustomerShopByCustomerID(InputGetCustomerShopListByCustomertID values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var serviceResult = StoreProcedureService.GetCustomerShopByCustomerID(values.CustomerID);
                var serviceResultRows = serviceResult.Rows;
                var canParseResult = serviceResultRows != null &&
                                     serviceResultRows.Count != 0;
                if (!canParseResult)
                {
                    return new OutputGetCustomerShopListByCustomertID[0];
                }

                var rowsCount = serviceResultRows.Count;
                var finalResult = new OutputGetCustomerShopListByCustomertID[rowsCount];
                for (var i = 0; i < rowsCount; i++)
                {
                    var tempDataRow = serviceResultRows[i];
                    finalResult[i] = new OutputGetCustomerShopListByCustomertID
                    {
                        CustomerShopID = Convert.ToInt64(tempDataRow.ItemArray[0]),
                        CustomerID = Convert.ToInt64(tempDataRow.ItemArray[1]),
                        ShopID = Convert.ToInt64(tempDataRow.ItemArray[2]),
                        ComplexTitle = tempDataRow.ItemArray[3].ToString(),
                        TabloName = tempDataRow.ItemArray[4].ToString(),
                        Pelak = tempDataRow.ItemArray[5].ToString(),
                        CustomerName = tempDataRow.ItemArray[6].ToString(),
                        CreationDate = Tools.ToShamsiDateString(Convert.ToDateTime(tempDataRow.ItemArray[7])),
                        StatusTitle = tempDataRow.ItemArray[8].ToString(),
                    };
                }
                return finalResult;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

        public OutputCustomerShopByComplexID[] GetCustomerShopByComplexID(InputCustomerShopByComplexID values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var serviceResult = StoreProcedureService.GetCustomerShopByComplexID(values.ComplexID);
                var serviceResultRows = serviceResult.Rows;
                var canParseResult = serviceResultRows != null &&
                                     serviceResultRows.Count != 0;
                if (!canParseResult)
                {
                    return new OutputCustomerShopByComplexID[0];
                }

                var rowsCount = serviceResultRows.Count;
                var finalResult = new OutputCustomerShopByComplexID[rowsCount];
                for (var i = 0; i < rowsCount; i++)
                {
                    var tempDataRow = serviceResultRows[i];
                    finalResult[i] = new OutputCustomerShopByComplexID
                    {
                        ShopID = Convert.ToInt64(tempDataRow.ItemArray[0]),
                        CustomerShopID = Convert.ToInt64(tempDataRow.ItemArray[1]),
                        ComplexId = Convert.ToInt64(tempDataRow.ItemArray[2]),
                        Pelak = tempDataRow.ItemArray[3].ToString(),
                        ComplexTitle = tempDataRow.ItemArray[4].ToString(),
                        Tablo = tempDataRow.ItemArray[5].ToString(),
                        CustomerName = tempDataRow.ItemArray[6].ToString(),
                        CreationDate = Tools.ToShamsiDateString(Convert.ToDateTime(tempDataRow.ItemArray[7])),
                    };
                }

                return finalResult;
              
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }

        
        public static void AddCustomerShop(InputAddCustomerToShop values)
        {
            var sw = Stopwatch.StartNew();
            try
            {

                StoreProcedureService.AssingShop(values.CustomerID, values.ShopID, values.TabloName, values.UserID);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        public static void RemoveCustomersShop(InputRemovePeople values)
        {
            var sw = Stopwatch.StartNew();
            try
            {

                  StoreProcedureService.RemoveCustomerShop(values.CustomerShopID, values.UserID);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex.GetBaseException().ToString(), sw.Elapsed.TotalMilliseconds);
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }

        
    }
}