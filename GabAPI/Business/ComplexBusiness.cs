﻿using BLL;
using COMMON;
using GabAPI.Models.Inputs.Complex;
using GabAPI.Models.Outputs.Complex;
using System;
using System.Diagnostics;
using System.Linq;

namespace GabAPI.Business
{
    public class ComplexBusiness
    {
        public void AddComplex(Complex values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var serviceResult = ComplexService.Add(values);
            }
            catch (Exception ex)
            {
                throw new Exception((ex.GetBaseException().ToString()));
            }
        }
        public Complex[] GetAllComplexList()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = ComplexService.GetALL().OrderByDescending(a => a.Id).ToList();
                var finalResult = new Complex[item.Count];

                for (var i = 0; i < item.Count; i++)
                {
                    var tempDataRow = item[i];
                    finalResult[i] = new Complex
                    {
                        Id = Convert.ToInt64(tempDataRow.Id),
                        Title = tempDataRow.Title.ToString(),
                    };
                }
                return finalResult;
            }
            catch (Exception ex)
            {
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }


        public OutputGetAllComplexList[] GetallComplex()
        {
            var sw = Stopwatch.StartNew();
            try
            {
                var item = ComplexService.GetALL().OrderByDescending(a => a.Id).ToList();
                var finalResult = new OutputGetAllComplexList[item.Count];

                for (var i = 0; i < item.Count; i++)
                {
                    var tempDataRow = item[i];
                    finalResult[i] = new OutputGetAllComplexList();


                    finalResult[i].ComplexId = Convert.ToInt64(tempDataRow.Id);
                    finalResult[i].ComplexTitle = tempDataRow.Title.ToString();
                    finalResult[i].ShopCount = tempDataRow.Shops != null ? tempDataRow.Shops.Count() : 0;
                   
                }
                return finalResult;
            }
            catch (Exception ex)
            {
                throw new Exception((ex.GetBaseException().ToString()));
            }

        }
        public void RemoveComplex(InputEditComplex values)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                long complexid = values.ComplexID;
                var item = new Complex
                {
                    Id = values.ComplexID
                };
                var shopcount = ShopService.GetALL().Where(a => a.ComplexId == complexid).Count();
                if (shopcount > 0)
                {
                    throw new Exception("برای این بازار تعداد : " + shopcount +" عدد مغازه ثبت شده و قابلیت حذف ندارد لطفا اببتدا مغازه ها را حذف کنید.");
                }
                else
                {
                    item = ComplexService.GetSpecial(item);
                    ComplexService.Remove(item);
                }

            }
            catch (Exception ex)
            {
                throw new Exception((ex.Message.ToString()));
            }
        }

    }
}