﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace Tools
{
    public static class Tools
    {
        public static String GetObjectsPath(String folder, Page page)
        {
            try
            {
                if (!System.IO.Directory.Exists(page.Server.MapPath(System.IO.Path.Combine("~/FileBank/", folder))))
                    System.IO.Directory.CreateDirectory(page.Server.MapPath(System.IO.Path.Combine("~/FileBank/", folder)));

                return String.Format("~/FileBank/{0}/", folder);
            }
            catch
            {
                return GetObjectsPath(folder, page);
            }
        }

        public static void SetSmalPic(String SrcAddress, String DesAddress, String FileName, Page page, int Width)
        {
            try
            {
                Bitmap OldPic = new Bitmap(SrcAddress);
                Bitmap NewPic;
                Size size = new Size();
                size.Height = 200;
                size.Width = Width;
                NewPic = new Bitmap(OldPic, size);
                NewPic.Save(String.Format("{0}Small_{1}", page.Server.MapPath(DesAddress), FileName));
                NewPic.Dispose();
                OldPic.Dispose();
            }
            catch
            {
                SetSmalPic(SrcAddress, DesAddress, FileName, page, Width);
            }

        }
        public static String getjQueryCode(string jsCodetoRun)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            sb.AppendLine(jsCodetoRun);
            sb.AppendLine(" });");

            return sb.ToString();
        }
        public static String GetFarsiDate(DateTime Date)
        {
            PersianCalendar PC = new PersianCalendar();
            string Day = "";
            switch (PC.GetDayOfWeek(Date).ToString())
            {
                case "Monday":
                    Day = "دوشنبه";
                    break;
                case "Tuesday":
                    Day = "سه شنبه";
                    break;
                case "Wednesday":
                    Day = "چهارشنبه";
                    break;
                case "Thursday":
                    Day = "پنجشنبه";
                    break;
                case "Friday":
                    Day = "جمعه";
                    break;
                case "Saturday":
                    Day = "شنبه";
                    break;
                case "Sunday":
                    Day = "یکشنبه";
                    break;
                default:
                    Day = "";
                    break;
            }

            string Mounth = "";
            switch (PC.GetMonth(Date))
            {
                case 1:
                    Mounth = "فروردین";
                    break;
                case 2:
                    Mounth = "اردیبهشت";
                    break;
                case 3:
                    Mounth = "خرداد";
                    break;
                case 4:
                    Mounth = "تیر";
                    break;
                case 5:
                    Mounth = "مرداد";
                    break;
                case 6:
                    Mounth = "شهریور";
                    break;
                case 7:
                    Mounth = "مهر";
                    break;
                case 8:
                    Mounth = "آبان";
                    break;
                case 9:
                    Mounth = "آذر";
                    break;
                case 10:
                    Mounth = "دی";
                    break;
                case 11:
                    Mounth = "بهمن";
                    break;
                case 12:
                    Mounth = "اسفند";
                    break;
                default:
                    Mounth = "";
                    break;
            }
            return Day + " " + PC.GetDayOfMonth(Date) + " " + Mounth + " " + PC.GetYear(Date).ToString();
        }
        public static String GetFarsiShortDate(DateTime Date, string Spliter = "/")
        {
            PersianCalendar PC = new PersianCalendar();
            return PC.GetYear(Date).ToString() + Spliter + PC.GetMonth(Date).ToString("00") + Spliter + PC.GetDayOfMonth(Date).ToString("00");
        }
        public static String Rial(string Cost)
        {
            string Pcs, Pcs1 = "";
            int i, the = 0;
            ArrayList Price = new ArrayList();

            for (i = Cost.Length; i >= 1; i = i - 3)
            {
                Pcs = Right(Cost.Substring(0, i), 3);
                Price.Add(Pcs);
            }

            for (the = Price.Count - 1; the >= 1; the = the - 1)
                Pcs1 += Price[the].ToString() + ",";

            Pcs1 = Pcs1 + Price[0];
            return Pcs1;
        }
        private static string Right(this string value, int length)
        {
            if (value.Length > length)
                return value.Substring(value.Length - length);
            else
                return value;
        }
        public static List<string> GetFilesFromDirectory(string DirPath, Page page)
        {
            List<string> Result = new List<string>();
            if (System.IO.Directory.Exists(DirPath))
            {
                DirectoryInfo Dir = new DirectoryInfo(DirPath);
                FileInfo[] FileList = Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly);
                foreach (FileInfo FI in FileList)
                    Result.Add(FI.Name);
            }
            return Result;
        }
        public static Exception GetMainError(Exception ex)
        {
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }
            return ex;
        }
        public static string CreateSalt()
        {
            return Guid.NewGuid().ToString().Replace("-", "").Substring(0, 4);
        }
        public static string CreateImage(string path, int height, int width)
        {
            Random r = new Random();
            string salt = CreateSalt();
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(bmp);
            g.TextRenderingHint = TextRenderingHint.AntiAlias;
            g.Clear(Color.Silver);
            System.Drawing.Drawing2D.Matrix mymat = new System.Drawing.Drawing2D.Matrix();
            int i;
            for (i = 0; (i <= (salt.Length - 1)); i++)
            {
                mymat.Reset();
                mymat.RotateAt(r.Next(-30, 0), new PointF((width * (0.12f * i)), (height * 0.5f)));
                g.Transform = mymat;
                g.DrawString(salt[i].ToString(), new Font("Times New Roman", 16, FontStyle.Italic), Brushes.Black, (width * (0.12f * i)), (height * 0.5f));
                g.ResetTransform();
            }
            bmp.Save(path, ImageFormat.Gif);
            g.Dispose();
            bmp.Dispose();
            g.Dispose();
            bmp.Dispose();
            return salt;
        }
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            if (items != null)
            {
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }
                //put a breakpoint here and check datatable
            }
            return dataTable;
        }
        public static string ToTXTFile<T>(T item, Page page, string UserID)
        {
            string FileName = UserID.ToString() + "-" + DateTime.Now.Date.Year.ToString() + DateTime.Now.Date.Month.ToString() + DateTime.Now.Date.Day.ToString() + DateTime.Now.TimeOfDay.Hours.ToString() + DateTime.Now.TimeOfDay.Minutes.ToString() + DateTime.Now.TimeOfDay.Seconds.ToString();
            string path = page.Server.MapPath("~/FileBank/Temp/" + FileName + ".txt");
            FileStream fs = File.Create(path);
            StreamWriter str = new StreamWriter(fs);
            str.BaseStream.Seek(0, SeekOrigin.End);
            str.WriteLine("Date: " + DateTime.Now.ToLongDateString());
            str.WriteLine("Time: " + DateTime.Now.ToLongTimeString());
            str.WriteLine(".:||-----------------------------------------------------||:.");
            str.WriteLine("\t\tUser Identity Code: " + UserID);
            str.WriteLine(".:||-----------------------------------------------------||:.");
            str.WriteLine(" ");

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                str.WriteLine(prop.Name + " ===> " + prop.GetValue(item, null));
                str.WriteLine(" ");
            }
            str.Flush();
            str.Close();
            fs.Close();
            // Close the Stream then Individually you can access the file.

            return path;
        }
        public static List<int> FetchString(string str, char spliter)
        {
            List<int> _result = new List<int>();
            if (str != null)
            {
                foreach (string _item in str.Split(new char[] { spliter }))
                {
                    if (!string.IsNullOrEmpty(_item))
                        _result.Add(Convert.ToInt32(_item));
                }
            }
            return _result;
        }
        public static string MakeString(List<int> inputList, char spliter)
        {
            string _result = "";
            foreach (int _item in inputList)
            {
                _result += _item.ToString().Trim() + spliter;
            }
            if (_result.Length > 0)
                return _result.Substring(0, _result.Length - 1);
            else
                return _result;
        }
        public static string EncodeQueryString(string Data)
        {
            return Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(Data));
        }
        public static string DecodeQueryString(string Data)
        {
            Byte[] DecodeData = Convert.FromBase64String(Data);
            return System.Text.Encoding.ASCII.GetString(DecodeData);
        }
        public static string HashString(string input)
        {
            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        public static string FormatDecimal(string input)
        {
            string[] result = input.Split('.');
            if (result.Length > 1)
            {
                int secondPart = Convert.ToInt32(result[1]);
                if (secondPart != 0)
                    return "N2";
                else
                    return "N0";
            }
            else
                return "N0";
        }
        public static DateTime GetLatinDate(string ShamsiDate, string Format = "yyyy-MM-dd", char Spliter = '/')
        {
            PersianCalendar PC = new PersianCalendar();
            string[] ShamsiList = ShamsiDate.Split(Spliter);
            DateTime Result = new DateTime(Convert.ToInt32(ShamsiList[0]), Convert.ToInt32(ShamsiList[1]), Convert.ToInt32(ShamsiList[2]), PC);
            return Result;

        }
        public static bool IsNumberFormat(string Input)
        {
            try
            {
                int Test = Convert.ToInt32(Input);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static DateTime shamsi2miladi(string _date)
        {
            int year = int.Parse(_date.Substring(0, 4));
            int month = int.Parse(_date.Substring(5, 2));
            int day = int.Parse(_date.Substring(8, 2));

            PersianCalendar p = new PersianCalendar();
            DateTime date = p.ToDateTime(year, month, day, 1, 0, 0, 0);

            return date;
        }
        public static bool IsFarsiError(string input)
        {
            if (Regex.IsMatch(input.Trim().Substring(0, 1), @"^[آ-ی]$") || Regex.IsMatch(input.Trim().Substring(1, 1), @"^[آ-ی]$") || Regex.IsMatch(input.Trim().Substring(2, 1), @"^[آ-ی]$") || Regex.IsMatch(input.Trim().Substring(3, 1), @"^[آ-ی]$"))
                return true;
            else
                return false;
        }
        public static List<dynamic> GetEnumList<T>()
        {
            return System.Enum.GetValues(typeof(T))
                                .Cast<Enum>()
                                .Select(value => new
                                {
                                    (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()), typeof(DescriptionAttribute)) as DescriptionAttribute).Description,
                                    value
                                })
                                .OrderBy(item => item.value)
                                .ToList<dynamic>();
        }
        public static double GetLatinDateInOADATEAsInteger(string ShamsiDate)
        {
            PersianCalendar PC = new PersianCalendar();

            string[] ShamsiList = ShamsiDate.Split('/');
            DateTime Result = new DateTime(Convert.ToInt32(ShamsiList[0]), Convert.ToInt32(ShamsiList[1]), Convert.ToInt32(ShamsiList[2]), PC);
            return (double)(Result.ToOADate());

        }
        public static void jdn_civil(int jdn, ref int iYear, ref int iMonth, ref int iDay)
        {
            int l;
            int k;
            int n;
            int i;
            int j;

            if (jdn > 2299160)
            {
                l = jdn + 68569;
                n = ((4 * l) / 146097);
                l = l - ((146097 * n + 3) / 4);
                i = ((4000 * (l + 1)) / 1461001);
                l = l - ((1461 * i) / 4) + 31;
                j = ((80 * l) / 2447);
                iDay = l - ((2447 * j) / 80);
                l = (j / 11);
                iMonth = j + 2 - 12 * l;
                iYear = 100 * (n - 49) + i + l;
            }
            else
            {
                throw new Exception("out of range");
            }

        }
        public static int persian_jdn(int iYear, int iMonth, int iDay)
        {
            int PERSIAN_EPOCH = 1948321; // The JDN of 1 Farvardin 1
            int epbase;
            int epyear;
            int mdays;
            if (iYear >= 0)
                epbase = iYear - 474;
            else
                epbase = iYear - 473;
            epyear = 474 + (epbase % 2820);
            if (iMonth <= 7)
                mdays = ((iMonth) - 1) * 31;
            else
                mdays = (Convert.ToInt32(iMonth) - 1) * 30 + 6;
            return Convert.ToInt32(iDay)
                    + mdays
                    + Convert.ToInt32(((epyear * 682) - 110) / 2816)
                    + (epyear - 1) * 365
                    + Convert.ToInt32(epbase / 2820) * 1029983
                    + (PERSIAN_EPOCH - 1);
        }
        public static string GetIPAddress()
        {
            string result = string.Empty;
            string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(ip))
            {
                string[] ipRange = ip.Split(',');
                int le = ipRange.Length - 1;
                result = ipRange[0];
            }
            else
            {
                result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return result;
        }


        public static double ConvertMinutesToHours(double minutes) // pass the minute variable you want to convert to hour
        {
            return TimeSpan.FromMinutes(minutes).TotalHours;
        }
        public static string ToEnglishString(string number)
        {
            var sw = Stopwatch.StartNew();
            var englishString = string.Empty;
            var Arabic = new string[10] { "٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩" };
            var persian = new string[10] { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };

            try
            {
                for (int j = 0; j < persian.Length; j++) number = number.Replace(persian[j], j.ToString());
                for (int j = 0; j < Arabic.Length; j++) number = number.Replace(Arabic[j], j.ToString());
                englishString = number;

                return englishString;
            }
            catch (Exception ex)
            {
                return englishString;
            }
        }
        public static string GetGenericName<T>()
        {
            var GenericType = typeof(T);
            string name;
            if (GenericType.IsGenericType)
            {
                var GenericName = GenericType.GetGenericTypeDefinition();
                name = GenericName.Name.Substring(0, GenericType.Name.IndexOf('`'));
            }
            else
            {
                name = GenericType.Name;
            }

            return name;
        }
        public static object GetGenericObjectSpecialFieldValue<T>(T GenericItem, string FieldName)
        {
            if (GenericItem == null)
            {
                return null;
            }

            var type = typeof(T);
            var properties = type.GetProperties();

            foreach (var propertyInfo in properties)
            {
                var namesEqual = propertyInfo.Name.ToLower().Trim() == FieldName.ToLower().Trim();
                if (!namesEqual)
                {
                    continue;
                }

                return propertyInfo.GetValue(GenericItem);
            }

            return null;
        }

        public static T Map<T>(IDataRecord record)
        {
            var DataTransferObject = Activator.CreateInstance<T>();
            foreach (var property in typeof(T).GetProperties())
            {
                var hasColumn = record.HasColumn(property.Name);
                var value = record.GetOrdinal(property.Name);
                var hasValue = !record.IsDBNull(value);

                if (hasColumn && hasValue)
                {
                    property.SetValue(DataTransferObject, record[property.Name]);
                }
            }

            return DataTransferObject;
        }
        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            var ignoreCase = StringComparison.InvariantCultureIgnoreCase;
            for (var i = 0; i < dr.FieldCount; i++)
            {
                var isColumnNameEqual = dr.GetName(i).Equals(columnName, ignoreCase);
                if (isColumnNameEqual)
                {
                    return true;
                }
            }

            return false;
        }
        public static string ConvertDataTableToJSON(DataTable table)
        {
            return JsonConvert.SerializeObject(table);
        }

        public static string ConvertToEnglishNumber(string number)
        {
            var arabic = new string[10] { "٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩" };
            var persian = new string[10] { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };

            var loopLength = 10;
            for (var i = 0; i < loopLength; i++)
            {
                var iAsNum = i.ToString();
                number = number.Replace(persian[i], iAsNum)
                               .Replace(arabic[i], iAsNum);
            }

            return number;
        }

        public static DateTime ConvertToLatinDate(string shamsiDate, string format = "yyyy-MM-dd", char spliter = '/')
        {
            if (format is null)
            {
                throw new ArgumentNullException(nameof(format));
            }
            if(string.IsNullOrEmpty(shamsiDate))
            {
                return DateTime.Now;
            }
            var pc = new PersianCalendar();
            var shamsiList = shamsiDate.Split(spliter);
            var year = Convert.ToInt32(shamsiList[0]);
            var month = Convert.ToInt32(shamsiList[1]);
            var day = Convert.ToInt32(shamsiList[2]);

            return new DateTime(year, month, day, pc);
        }

        public static DateTime ConvertToLatinDateTime(string shamsiDate, string time)
        {
            var date = ConvertToLatinDate(shamsiDate);
            var splitedTime = time.Split(':');
            var hour = Convert.ToInt32(splitedTime[0]);
            var miniute = Convert.ToInt32(splitedTime[1]);
            date += new TimeSpan(hour, miniute, 0);
            return date;
        }

    }
}
