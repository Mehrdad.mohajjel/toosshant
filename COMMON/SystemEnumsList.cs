﻿namespace COMMON
{
    public static class SystemEnumsList
    {
        public enum AttachmentStatus
        {
            Confirmed = 1,
            Removed = 2
        };

        public enum CommentStatus
        {
            Confirmed = 1,
            Waiting = 2,
            Rejected = 3,
            Removed = 4
        };

        public enum CmsType
        {
            Category = 1,
            User = 2,
            AttachmentImage = 3,
            AttachmentVideo = 5,
            AttachmentPdf = 6,
            AttachmentAudio = 7,
            PostBlogItem = 8,
            Page = 9,
            UserLinks = 10,
            Slider = 11,
        };

        public enum PostStatus
        {
            Confirmed = 1,
            Waiting = 2,
            Rejected = 3,
            Removed = 4
        };

        public enum UserRoles
        {
            Admin = 1,
            SuperAdmin = 2,
            Editor = 3,
            Author = 4,
            Subscriber = 5,
            Contributor = 6
        };

        public enum UserStatus
        {
            Active = 1,
            Disabled = 2,
            Removed = 3,
        };

    }

}
