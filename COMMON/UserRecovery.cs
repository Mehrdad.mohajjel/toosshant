//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace COMMON
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserRecovery
    {
        public long Id { get; set; }
        public string EmailAddress { get; set; }
        public string Code { get; set; }
        public Nullable<System.DateTime> ExpirationDateTime { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
    }
}
