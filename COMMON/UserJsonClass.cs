﻿namespace COMMON
{
    public class UserJsonClass
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string NickName { get; set; }

        public double Exp { get; set; }
    }
}
