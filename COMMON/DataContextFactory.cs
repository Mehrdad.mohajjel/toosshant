﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Web;

namespace COMMON
{
    public class DataContextFactory
    {

        /// <summary>
        /// Returns either Web Request scoped DataContext or a Thread scoped
        /// request object if not running a Web request (ie. HttpContext.Current)
        /// is not available.
        /// </summary>
        /// <typeparam name="ModuleDbContext"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        /// 

        #region "ModuleDbContext Managment"
        //---------------------------------------------------------------------------------------------------//
        public static ModuleDbContext GetScopedDataContext()
        {
            if (HttpContext.Current != null)
                return GetWebRequestScopedDataContextInternal();

            return GetThreadScopedDataContextInternal();
        }
        //---------------------------------------------------------------------------------------------------//

        //---------------------------------------------------------------------------------------------------//
        static ModuleDbContext GetWebRequestScopedDataContextInternal()
        {
            // *** Create a unique Key for the Web Request/Context 
            string key = string.Format("__Context_{0}{1}", HttpContext.Current.GetHashCode().ToString("x"), Thread.CurrentContext.ContextID);
            ModuleDbContext context;
            try
            {

                context = (ModuleDbContext)HttpContext.Current.Items[key];
                if (context == null)
                {
                    context = new ModuleDbContext();

                    if (context != null)
                        HttpContext.Current.Items[key] = context;
                }
                return context;
            }
            catch (Exception ex)
            {
                if (ex.GetBaseException().Message.Contains("Unable to cast object of type"))
                {
                    HttpContext.Current.Items[key] = null;

                    context = new ModuleDbContext();
                    HttpContext.Current.Items[key] = context;
                    return context;
                }

                return null;
            }
        }
        //---------------------------------------------------------------------------------------------------//

        //---------------------------------------------------------------------------------------------------//
        static ModuleDbContext GetThreadScopedDataContextInternal()
        {
            string key = "__Context_" + Thread.CurrentContext.ContextID;

            LocalDataStoreSlot threadData = Thread.GetNamedDataSlot(key);

            ModuleDbContext context = null;
            if (threadData != null)
                context = (ModuleDbContext)Thread.GetData(threadData);

            if (context == null)
            {
                context = new ModuleDbContext();

                if (context != null)
                {
                    if (threadData == null)
                        threadData = Thread.AllocateNamedDataSlot(key);

                    Thread.SetData(threadData, context);
                }
            }

            return context;
        }
        //---------------------------------------------------------------------------------------------------//

        //---------------------------------------------------------------------------------------------------//
        public static void Dispose()
        {
            //----------------- Context -----------------//
            ModuleDbContext context = null;
            if (HttpContext.Current != null)
            {
                // *** Create a unique Key for the Web Request/Context 
                string key = string.Format("__Context_{0}{1}", HttpContext.Current.GetHashCode().ToString("x"), Thread.CurrentContext.ContextID);

                context = (ModuleDbContext)HttpContext.Current.Items[key];
                if (context != null)
                    HttpContext.Current.Items[key] = null;
            }
            else
            {
                string key = "__Context_" + Thread.CurrentContext.ContextID;

                LocalDataStoreSlot threadData = Thread.GetNamedDataSlot(key);

                if (threadData != null)
                    context = (ModuleDbContext)Thread.GetData(threadData);

                if (context != null)
                    Thread.SetData(threadData, null);
            }
            context.Dispose();
            //-----------------------------------------//

            //-------------- Transaction --------------//
            if (HttpContext.Current != null)
            {

                // *** Create a unique Key for the Web Request/Context 
                string key = string.Format("__Transaction_{0}{1}", HttpContext.Current.GetHashCode().ToString("x"), Thread.CurrentContext.ContextID);
                HttpContext.Current.Items[key] = null; // حذف تراکنش قبلی
            }
            else
            {
                string key = "__Transaction_" + Thread.CurrentContext.ContextID;

                LocalDataStoreSlot threadData = Thread.GetNamedDataSlot(key);
                if (threadData == null)
                    threadData = Thread.AllocateNamedDataSlot(key);
                Thread.SetData(threadData, null); // حذف تراکنش قبلی
            }
            //-----------------------------------------//
        }
        //---------------------------------------------------------------------------------------------------//
        #endregion

        #region "Begin Transaction Managment"
        //---------------------------------------------------------------------------------------------------//
        public static bool BeginCurrentTransaction(bool IsShouldToBeginTransaction)
        {
            if (IsShouldToBeginTransaction)
            {
                if (HttpContext.Current != null)
                    return BeginWebRequestScopedTransactionInternal();

                return BeginThreadScopedTransactionInternal();
            }
            else
                return false;
        }
        //---------------------------------------------------------------------------------------------------//

        //---------------------------------------------------------------------------------------------------//
        static bool BeginWebRequestScopedTransactionInternal()
        {
            DbContextTransaction Trans;

            // *** Create a unique Key for the Web Request/Context 
            string key = string.Format("__Transaction_{0}{1}", HttpContext.Current.GetHashCode().ToString("x"), Thread.CurrentContext.ContextID);
            if ((DbContextTransaction)HttpContext.Current.Items[key] == null)
            {
                Trans = GetScopedDataContext().Database.BeginTransaction();

                if (Trans != null)
                    HttpContext.Current.Items[key] = Trans;

                return true;
            }
            else
                return false; //قبلا تراکنش باز شده و نیازی به باز کردن مجدد آن وجود ندارد
        }
        //---------------------------------------------------------------------------------------------------//

        //---------------------------------------------------------------------------------------------------//
        static bool BeginThreadScopedTransactionInternal()
        {
            string key = "__Transaction_" + Thread.CurrentContext.ContextID;

            LocalDataStoreSlot threadData = Thread.GetNamedDataSlot(key);

            DbContextTransaction Trans = null;
            if (threadData != null)
                Trans = (DbContextTransaction)Thread.GetData(threadData);

            if (Trans == null)
            {
                Trans = GetScopedDataContext().Database.BeginTransaction();
                if (threadData == null)
                    threadData = Thread.AllocateNamedDataSlot(key);

                Thread.SetData(threadData, Trans);

                return true;
            }
            else
                return false; //قبلا تراکنش باز شده و نیازی به باز کردن مجدد آن وجود ندارد
        }
        //---------------------------------------------------------------------------------------------------//
        #endregion

        #region "Rollback Transaction Managment"
        //---------------------------------------------------------------------------------------------------//
        public static void RollbackCurrentTransaction()
        {
            if (HttpContext.Current != null)
                RollbackWebRequestScopedTransactionInternal();
            else
                RollbackThreadScopedTransactionInternal();
        }
        //---------------------------------------------------------------------------------------------------//

        //---------------------------------------------------------------------------------------------------//
        static void RollbackWebRequestScopedTransactionInternal()
        {
            DbContextTransaction Trans;

            // *** Create a unique Key for the Web Request/Context 
            string key = string.Format("__Transaction_{0}{1}", HttpContext.Current.GetHashCode().ToString("x"), Thread.CurrentContext.ContextID);
            if ((DbContextTransaction)HttpContext.Current.Items[key] != null)
            {
                GetScopedDataContext().Database.CurrentTransaction.Rollback();
                HttpContext.Current.Items[key] = null; // حذف تراکنش قبلی
            }
            else
                return; //قبلا تراکنش باز نشده و نیازی به رول بک کردن آن وجود ندارد
        }
        //---------------------------------------------------------------------------------------------------//

        //---------------------------------------------------------------------------------------------------//
        static void RollbackThreadScopedTransactionInternal()
        {
            string key = "__Transaction_" + Thread.CurrentContext.ContextID;

            LocalDataStoreSlot threadData = Thread.GetNamedDataSlot(key);

            DbContextTransaction Trans = null;
            if (threadData != null)
                Trans = (DbContextTransaction)Thread.GetData(threadData);

            if (Trans != null)
            {
                GetScopedDataContext().Database.CurrentTransaction.Rollback();
                if (threadData == null)
                    threadData = Thread.AllocateNamedDataSlot(key);
                Thread.SetData(threadData, null); // حذف تراکنش قبلی
            }
            else
                return; //قبلا تراکنش باز نشده و نیازی به رول بک کردن آن وجود ندارد
        }
        //---------------------------------------------------------------------------------------------------//
        #endregion
    }
}
