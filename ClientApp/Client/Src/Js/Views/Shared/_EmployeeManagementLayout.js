﻿(function () {

    function init() {
        setActiveTab();
    }

    function setActiveTab() {
        var currentPageUrl = location.pathname.toLowerCase();
        var links = document.querySelectorAll(".employee-management-layout__link");

        for (var i = 0; i < links.length; i++) {
            var tempLink = links[i];
            var linkUrl = tempLink.getAttribute("href").toLowerCase();
            var isUrlMatch = linkUrl.indexOf(currentPageUrl) > -1;
            if (!isUrlMatch) continue;

            $(tempLink).addClass("employee-management-layout__link--active");
        }
    }

    $(document).ready(init);

})();