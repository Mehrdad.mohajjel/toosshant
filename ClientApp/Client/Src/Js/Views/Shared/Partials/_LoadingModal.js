﻿var motorsazanClient = motorsazanClient || {};
motorsazanClient.loadingModal = (function () {

    var modalId = "#motorsazanLoadingModal";
    var activeLoadingCount = 0;

    function hideModal() {
        activeLoadingCount--;
        if (activeLoadingCount < 0) activeLoadingCount = 0;
        if (activeLoadingCount === 0) {
            $(modalId).removeClass("loading--active");
        }
    }

    function showModal() {
        activeLoadingCount++;
        $("#motorsazanLoadingModal").addClass("loading--active");
    }

    return {
        hide: hideModal,
        show: showModal
    };

})();