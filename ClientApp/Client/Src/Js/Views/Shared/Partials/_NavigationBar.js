﻿function initNavigationBar() {
    var dom = {
        nav: $("#nav"),
        showMenuBtn: $("#navShowMenuBtn"),
        drawer: $("#navDrawer"),
        overlay: $("#navOverlay")
    };

    function handleMenuItemsToggle(event) {
        var target = $(event.target);

        var isClickedItemTypeIsMenuRootItem = target.hasClass("nav__item--has-submenu");
        if (isClickedItemTypeIsMenuRootItem) target.toggleClass("nav__item--expanded");
    }

    function hideDrawer() {
        dom.drawer.removeClass("nav__drawer--active");
        dom.overlay.removeClass("nav__overlay--active");
        location.hash = "";
    }

    function setEvents() {
        dom.showMenuBtn.click(showDrawer);
        dom.overlay.click(hideDrawer);
        window.addEventListener("hashchange", hideDrawer);
        dom.drawer.click(handleMenuItemsToggle);
    }

    function showDrawer() {
        dom.drawer.addClass("nav__drawer--active");
        dom.overlay.addClass("nav__overlay--active");
        if (history.pushState) history.pushState(null, null, "#navigation");
        else location.hash = '#navigation';
    }


    setEvents();
}

$(document).ready(initNavigationBar);
