﻿var motorsazanClient = motorsazanClient || {};
motorsazanClient.contentModal = (function() {

    var dom = null;

    function ajaxShow(title, url, parameters, callback, showInMedium) {
        showInMedium = showInMedium || false;
        callback = callback || null;

        motorsazanClient.connector.post(url, parameters)
            .then(function(response) {
                showModal(title, response, showInMedium);
                if (callback) callback();
            })
            .catch(function(err) {
                console.log(err);
            });
    }

    function clientShow(title, content, showInMedium) {
        howInMedium = showInMedium || false;
        showModal(title, content, showInMedium);
    }

    function handleEscKeyPressByUser(event) {
        var escKeyCode = 27;
        var isEscPressed = event.keyCode === escKeyCode;
        if (isEscPressed) hideModal();
    }

    function hideModal() {
        dom.modal.removeClass("content-modal--active");
    }

    function init() {
        setDom();
        setEvents();
    }

    function setDom() {
        dom = {
            modal: $("#contentModal"),
            title: $("#contentModalTitle"),
            closeBtn: $("#contentModalCloseBtn"),
            maximizeBtn: $("#contentModalMaximizeBtn"),
            body: $("#contentModalBody"),
            form: $("#contentModalForm")
        };
    }

    function setEvents() {
        dom.closeBtn.click(hideModal);
        $("body").keydown(handleEscKeyPressByUser);
        dom.maximizeBtn.click(toggleMaximize);
    }

    function toggleMaximize() {
        dom.modal.toggleClass("content-modal--maximize");
    }

    $(document).ready(init);

    function showModal(title, contentHtml, showInMedium) {
        dom.modal.removeClass("content-modal--maximize");
        dom.title.html(title);
        dom.body.html(contentHtml);
        if (showInMedium) {
            dom.form.addClass("content-modal__form--medium")
        }
        dom.modal.addClass("content-modal--active");
    }

    return {
        ajaxShow: ajaxShow,
        clientShow: clientShow,
        hideModal: hideModal,
        hide: hideModal
    }

})();