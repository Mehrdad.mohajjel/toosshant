﻿using BLL;
using COMMON;
using GabAPI.Models.Inputs.Customer;
using GabAPI.Models.Inputs.CustomerShop;
using GabAPI.Models.Inputs.KarKard;
using GabAPI.Models.Inputs.Usage;
using GabAPI.Models.Inputs.User;
using GabAPI.Models.Outputs.Customer;
using GabAPI.Models.Outputs.CustomerShop;
using GabAPI.Models.Outputs.KarKard;
using GabAPI.Models.Outputs.Usage;
using GabAPI.Models.Outputs.User;
using GabAPI.Models.Inputs.CustomerShop.AssignPeopleToLine;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;
using GabAPI.Models.Inputs.CustomerShop.AssignShop;
using GabAPI.Models.Outputs.AssignShop;
using GabAPI.Models.Inputs.AssignShop;
using GabAPI.Models.Outputs.Price;
using GabAPI.Models.Inputs.Price;
using GabAPI.Models.Inputs.Complex;
using GabAPI.Models.Outputs.Complex;

namespace ClientApp.Api
{
    public static class ApiList
    {
        //readonly static string _baseUrl = "http://localhost:2020/";
        readonly static string _baseUrl = "http://localhost:9999/";
        //readonly static string _baseLoginUrl = "http://localhost:7997/";

        #region login
        public static dynamic Login(User values)
        {
            var user = UserService.GetUserByUserName(values.UserName);
            if(user == null)
            {
               // return (HttpStatusCode.NotFound, "کاربری با نام کاربری وارد شده یافت نشد");
                throw new System.Exception("کاربری با نام کاربری وارد شده یافت نشد");
            }

            var isPasswordMatched = UserService.IsUserPasswordMachByDatabase(values.Password, user.Password);
            if(!isPasswordMatched)
            { 
                throw new System.Exception("رمز عبور اشتباه است");
               // return (HttpStatusCode.NotFound, ""); 
            }

            if(user.Status == (byte)SystemEnumsList.UserStatus.Disabled)
            { 
                throw new System.Exception("حساب کاربری شما توسط مدیر سیستم غیرفعال شده است");
               // return (HttpStatusCode.NotFound, "حساب کاربری شما توسط مدیر سیستم غیرفعال شده است "); 
            }
            if(user.Status == (byte)SystemEnumsList.UserStatus.Removed)
            { 
                throw new System.Exception("حساب کاربری شما توسط مدیر سیستم حذف شده است");
                //return (HttpStatusCode.NotFound, "حساب کاربری شما توسط مدیر سیستم حذف شده است "); 
            }

            var jsonitem = new User
            {
                EmailAddress = user.EmailAddress,
                Id = user.Id,
                Mobile = user.Mobile,
                NickName = user.NickName,
                Status = user.Status,
                UserName = user.UserName
            };

          //  var token = UserService.GenerateUserToken(jsonitem);
            return jsonitem;

        }

        public static UserJsonClass GetUerInfoByJwt(string JsonToken)
        {
            var reqCookies = System.Web.HttpContext.Current.Request.Cookies["userInfo"];
            var jsonsession = System.Web.HttpContext.Current.Session["MohajjelJsonWebToken"].ToString();
            var stirngarray = JsonToken.Split('.');
            if(stirngarray.Length != 3)
            {
                JsonToken = jsonsession;
            }
            else if(reqCookies != null)
            {
                JsonToken = reqCookies["MohajjelJsonWebToken"].ToString();
            }
            var parsedToken = UserService.ParseToken(JsonToken);
            var jsonTokenValues = JsonConvert.DeserializeObject(parsedToken).ToString();
            jsonTokenValues = jsonTokenValues.Replace("{{", "{");
            jsonTokenValues = jsonTokenValues.Replace("}}", "}");
            var result = JsonConvert.DeserializeObject<UserJsonClass>(jsonTokenValues);
            return result;
        }

        #endregion login

        #region AssignPeopleToLine

        public static string AddCustomerToShop(InputAddCustomerToShop values, COMMON.User token)
        {
            var methodName = "CustomerShop/AddCustomerShop";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }


        //public static OutputGetProductionLineEmployeeListByCondition[] GetProductionLineEmployeeListByCondition(InputGetProductionLineEmployeeListByCondition values, COMMON.User token)
        //{
        //    var methodName = "AssignPeopleToLine/GetProductionLineEmployeeListByCondition";
        //    var task = Task.Run<OutputGetProductionLineEmployeeListByCondition[]>(async () => {
        //        return await ApiConnector<OutputGetProductionLineEmployeeListByCondition[]>.Post(_baseUrl, methodName, token, values);
        //    });
        //    return task.Result;
        //}

        //public static OutputGetEmployeeCurrentLineByProductionLineEmployeeID GetEmployeeCurrentLineByProductionLineEmployeeID(InputGetEmployeeCurrentLineByProductionLineEmployeeID values, COMMON.User token)
        //{
        //    var methodName = "AssignPeopleToLine/GetEmployeeCurrentLineByProductionLineEmployeeID";
        //    var task = Task.Run<OutputGetEmployeeCurrentLineByProductionLineEmployeeID>(async () => {
        //        return await ApiConnector<OutputGetEmployeeCurrentLineByProductionLineEmployeeID>.Post(_baseUrl, methodName, token, values);
        //    });
        //    return task.Result;
        //}

        public static string RemoveShopCustomer(InputRemovePeople values, COMMON.User token)
        {
            var methodName = "CustomerShop/RemoveShopCustomer";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }

        public static string ChangeStatus(InputChangeStatus values, COMMON.User token)
        {
            var methodName = "User/ChangeStatus";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static string ResetPassword(InputResetPassword values, COMMON.User token)
        {
            var methodName = "User/ResetPassword";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static string RemoveUser(InputRemoveUser values, COMMON.User token)
        {
            var methodName = "User/RemoveUser";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        
        public static string TransferEmployeeToProductionLine(InputTransferEmployeeToProductionLine values, COMMON.User token)
        {
            var methodName = "AssignPeopleToLine/TransferEmployeeToProductionLine";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }

        #endregion


        #region Customer

        public static string AddCustomer(Customer values, COMMON.User token)
        {
            var methodName = "Customer/AddCustomer";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }

        public static string GetLastCode()
        {
            var methodName = "Customer/GetLastCode";
            var task = Task.Run(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, null, null);
            });
            return task.Result.ToString();
        }
        public static OutputGetCustomerList[] GetAllCustomersList()
        {
            var methodName = "Customer/GetAllCustomerList";
            var task = Task.Run(async () =>
            {
                return await ApiConnector<OutputGetCustomerList[]>.Post(_baseUrl, methodName, null, null);
            });
            return task.Result;
        }
        public static COMMON.CustomerType[] GetCustomerTypeList()
        {
            var methodName = "Customer/GetAllCustomeTypeList";
            var task = Task.Run(async () =>
            {
                return await ApiConnector<CustomerType[]>.Post(_baseUrl, methodName, null, null);
            });
            return task.Result;
        }
        
        public static OutputCustomerShopByComplexID[] GetCustomerShopByComplexID(InputCustomerShopByComplexID values)
        {
            var methodName = "CustomerShop/GetCustomerShopByComplexID";
            var task = Task.Run(async () =>
            {
                return await ApiConnector<OutputCustomerShopByComplexID[]>.Post(_baseUrl, methodName, null, values);
            });
            return task.Result;
        }

        public static Complex[] GetComplexList()
        {
            var methodName = "Complex/GetAllComplexList";
            var task = Task.Run<Complex[]>(async () =>
            {
                return await ApiConnector<Complex[]>.Post(_baseUrl, methodName, null, null);
            });
            return task.Result;
        }

        public static OutputGetCustomerList[] GetCustomerList(InputGetCustomerList values, COMMON.User token)
        {
            var methodName = "Customer/GetCustomerList";
            var task = Task.Run<OutputGetCustomerList[]>(async () =>
            {
                return await ApiConnector<OutputGetCustomerList[]>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }

        public static OutputGetCustomerByID GetCustomerDetailByID(InputGetCustomerByID values, COMMON.User token)
        {
            var methodName = "Customer/GetCustomerDetailByID";
            var task = Task.Run<OutputGetCustomerByID>(async () =>
            {
                return await ApiConnector<OutputGetCustomerByID>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static string RemoveCustomer(Customer values, COMMON.User token)
        {

            var methodName = "Customer/RemoveCustomer";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        #endregion

        #region CustomerShop
        public static OutputGetCustomerList[] GetAllCustomerList(InputGetCustomerList values, COMMON.User token)
        {
            var methodName = "Customer/GetAllCustomerList";
            var task = Task.Run<OutputGetCustomerList[]>(async () =>
            {
                return await ApiConnector<OutputGetCustomerList[]>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static OutputGetCustomerShopListByCustomertID[] GetCustomerShopByCustomerID(InputGetCustomerShopListByCustomertID values, COMMON.User token)
        {
            var methodName = "CustomerShop/GetCustomerShopHistory";
            var task = Task.Run<OutputGetCustomerShopListByCustomertID[]>(async () =>
            {
                return await ApiConnector<OutputGetCustomerShopListByCustomertID[]>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static OutputGetCustomerShopListByCustomertID[] GetCustomerShopHistory(InputGetCustomerStatusHistory values, COMMON.User token)
        {
            var methodName = "CustomerShop/GetCustomerShopHistory";
            var task = Task.Run<OutputGetCustomerShopListByCustomertID[]>(async () =>
            {
                return await ApiConnector<OutputGetCustomerShopListByCustomertID[]>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }

        public static Shop[] GetShopListByComplexID(InputGetShopListByComplexID values, COMMON.User token)
        {
            var methodName = "Shop/GetShopListByComplexID";
            var task = Task.Run<Shop[]>(async () =>
            {
                return await ApiConnector<Shop[]>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }

        #endregion CustomerShop
        #region Karkard 
        public static string AddNewExpence(InputaddExpence values, COMMON.User token)
        {
            var methodName = "Customer/AddNewExpence";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static OutputPrice[] GetActivePriceList(COMMON.User token)
        {
            var methodName = "Customer/GetActivePriceList";
            var task = Task.Run(async () =>
            {
                return await ApiConnector<OutputPrice[]>.Post(_baseUrl, methodName, token);
            });
            return task.Result;
        }
        public static OutputGetKarkardDetail[] GetKarkardDetail(InputGetKarkardDetail values)
        {
             var methodName = "Customer/GetExpenceDetail";
            var task = Task.Run(async () =>
            {
                return await ApiConnector<OutputGetKarkardDetail[]>.Post(_baseUrl, methodName,  values);
            });
            return task.Result;
        }
        #endregion Karkard

        #region Usage
        public static OutputGetUsageByDate[] GetUsageByDate(InputGetUsageList values, COMMON.User token)
        {
            var methodName = "Usage/GetUsageByDate";
            var task = Task.Run<OutputGetUsageByDate[]>(async () =>
            {
                return await ApiConnector<OutputGetUsageByDate[]>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        #endregion Usage

        #region User
        public static string AddUser(User values, COMMON.User token)
        {
            var methodName = "User/AddUser";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static OutputGetAllUserList[] GetAllUserList(InputGetAllUserList values, COMMON.User token)
        {
            var methodName = "User/GetallUser";
            var task = Task.Run<OutputGetAllUserList[]>(async () =>
            {
                return await ApiConnector<OutputGetAllUserList[]>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }


        #endregion User
        #region Price
        public static string AddPrice(Price values, COMMON.User token)
        {
            var methodName = "Price/AddPrice";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static OutputGetAllPriceList[] GetAllPrice(COMMON.User token)
        {
            var methodName = "Price/GetallPrice";
            var task = Task.Run<OutputGetAllPriceList[]>(async () =>
            {
                return await ApiConnector<OutputGetAllPriceList[]>.Post(_baseUrl, methodName, token);
            });
            return task.Result;
        }
        public static string ChangePriceStatus(InputEditPrice values, COMMON.User token)
        {
            var methodName = "Price/ChangeStatus";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        #endregion Price
        #region Complex
        public static string AddComplex(Complex values, COMMON.User token)
        {
            var methodName = "Complex/AddComplex";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        public static OutputGetAllComplexList[] GetAllComplex(COMMON.User token)
        {
            var methodName = "Complex/GetallComplex";
            var task = Task.Run<OutputGetAllComplexList[]>(async () =>
            {
                return await ApiConnector<OutputGetAllComplexList[]>.Post(_baseUrl, methodName, token);
            });
            return task.Result;
        }
        public static string RemoveComplex(InputEditComplex values, COMMON.User token)
        {
            var methodName = "Complex/RemoveComplex";
            var task = Task.Run<string>(async () =>
            {
                return await ApiConnector<string>.Post(_baseUrl, methodName, token, values);
            });
            return task.Result;
        }
        #endregion Complex
    }
}