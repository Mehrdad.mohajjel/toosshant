﻿using Newtonsoft.Json;
using ClientApp.Models.Base;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ClientApp.Api
{
    public class ApiConnector<T>
    {
        public static async Task<T> Post(string baseUrl, string mthode, COMMON.User token = null, object parameters = null)
        {
            using(var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var hasToken = token;
               

                var response = await client.PostAsJsonAsync(mthode, parameters);
                if(response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<T>(jsonResult);
                }
                else if(response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new LoginException();
                }
                else
                {
                    var httpErrorObject = response.Content.ReadAsStringAsync().Result;
                    var anonymousErrorObject = new { message = "", ModelState = new Dictionary<string, string[]>() };
                    var deserializedErrorObject =
                        JsonConvert.DeserializeAnonymousType(httpErrorObject, anonymousErrorObject);
                
                    throw new AlertException(deserializedErrorObject.message);
                }
            }
        }

        public static async Task<T> Post(string baseUrl, string mthode,object parameters = null)
        {
            using(var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                var response = await client.PostAsJsonAsync(mthode, parameters);
                if(response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<T>(jsonResult);
                }
                else if(response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    throw new LoginException();
                }
                else
                {
                    var httpErrorObject = response.Content.ReadAsStringAsync().Result;
                    if(httpErrorObject is string)
                    {
                        throw new AlertException(httpErrorObject);

                    }
                    else
                    {
                        var anonymousErrorObject = new { message = "", ModelState = new Dictionary<string, string[]>() };
                        var deserializedErrorObject =
                            JsonConvert.DeserializeAnonymousType(httpErrorObject, anonymousErrorObject);

                        throw new AlertException(deserializedErrorObject.message);

                    }
                }
            }
        }

    }
}