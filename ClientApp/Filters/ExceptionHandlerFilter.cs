﻿using ClientApp.Models.Base;
using System.Web.Mvc;

namespace ClientApp.Filters
{
    public class ExceptionHandlerFilter: FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            var baseException = filterContext.Exception.GetBaseException();
            string error;
            var code = (int)System.Net.HttpStatusCode.InternalServerError;
            if(baseException is LoginException)
            {
                error = "auth";
                code = 401;
            } else if (baseException is AlertException)
            {
                error = baseException.Message;
                code = 501;
            } else
            {
                error = baseException.Message;
                //error = "امکان اجرای درخواست فراهم نشد لطفا با تیم توسعه تماس حاصل فرمایید";
            }

            filterContext.HttpContext.Response.StatusCode = code;
            filterContext.Result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new
                {
                    Error = error
                }
            };
        }
    }
}