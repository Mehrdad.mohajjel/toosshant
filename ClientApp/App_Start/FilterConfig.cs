using ClientApp.Filters;
using System.Web.Mvc;

namespace ClientApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ExceptionHandlerFilter());
        }
    }
}