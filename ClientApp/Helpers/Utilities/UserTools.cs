﻿using ClientApp.Models.ApiModels.Output;
using System;
using System.Linq;
using System.Web;

namespace ClientApp.Helpers.Utilities
{
    public static class UserTools
    {
        public static int? GetOnlineUser()
        {
            var sessionKey = "OnlineUserId";
            var currentSessionValue = HttpContext.Current.Session[sessionKey];
            return currentSessionValue != null ? Convert.ToInt32(currentSessionValue) : 0;
        }

        public static int GetUserIdByJsonWebToken(string JsonWebToken)
        {

            try
            {
                var input = new AuthService.WebServiceInputGetEndUserInfoByJsonWebToken
                {
                    Parameters = new AuthService.WebServiceInputGetEndUserInfoByJsonWebTokenParams
                    {
                        JsonWebToken = JsonWebToken
                    }
                };
                var client = new AuthService.AppClient();
                var output = client.GetUserInfoByJsonWebToken(input);
                client.Close();

                if(output.Status.Code != "G00000")
                {
                    return 0;
                }

                return output.Parameters != null ? output.Parameters.UserID : 0;
            }
            catch
            {
                return 0;
            }
        }

        public static User GetUserName(long userID)
        {
            try
            {
                var baseURL = "http://erp-server:2021/api/GetUserInfo";
                var server = new WebApiConsumer<Models.ApiModels.Output.UserInfo>(baseURL);
                var response = server.Get("UserId=" + userID);
                var successStatus = "1";

                var isResponceValid = !Equals(response, null) && response.Result.Status.Code == successStatus;

                if(!isResponceValid)
                {
                    return null;
                }

                var userInfo = response.Result.Params.List;
                var isUserInfoValid = userInfo != null && userInfo.Count() > 0;

                return isUserInfoValid ? userInfo[0] : null;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static bool HasAccessToEvent(int? userId, string formCode, string eventCode, string systemCode = "007")
        {
            try
            {
                var baseUrl = "http://erp-server/api/CheckEventAccess";
                var server = new WebApiConsumer<CheckEventAccess>(baseUrl);
                var url = string.Format("UserID={0}&FormCode={1}&SystemCode={2}&EventCode={3}", userId, formCode, systemCode, eventCode);
                var response = server.Get(url);
                var isResponseValid = !Equals(response, null) && response.Result.Status.Code == "G00000";
                if(!isResponseValid)
                {
                    return false;
                }

                var result = response.Result.Params.HasAccessToPage;
                return response != null ? result.HassAccess : false;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public static bool HasAccessToForm(int? userId, string formCode, string systemCode = "007")
        {
            try
            {
                var baseUrl = "http://erp-server/api/CheckFormAccess";
                var server = new WebApiConsumer<CheckFormAccess>(baseUrl);
                var url = string.Format("UserID={0}&FormCode={1}&SystemCode={2}", userId, formCode, systemCode);
                var response = server.Get(url);
                var isResponseValid = !Equals(response, null) && response.Result.Status.Code == "G00000";

                if(isResponseValid)
                {
                    return false;
                }

                var result = response.Result.Params.HasAccessToPage;
                return result != null ? result.HassAccess : false;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public static bool IsSessionValidByJsonWebToken(string jsonWebToken)
        {
            try
            {
                var input = new AuthService.WebServiceInputValidateSessionByJsonWebToken
                {
                    Parameters = new AuthService.WebServiceInputValidateSessionByJsonWebTokenParams
                    {
                        JsonWebToken = jsonWebToken
                    }
                };
                var client = new AuthService.AppClient();
                var output = client.ValidateSessionByJsonWebToken(input);
                client.Close();

                return output.Status.Code == "G00000" ? output.Parameters.IsValid : false;
            }
            catch
            {
                return false;
            }
        }

    }
}