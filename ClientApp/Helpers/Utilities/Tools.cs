﻿using DevExpress.Web.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Caching;
using System.Xml.Serialization;

namespace ClientApp.Helpers.Utilities
{
    public static class Tools
    {
        public static string ConvertArabicCharacterToPersian(string word)
        {
            return word.Replace("ی", "ي")
                       .Replace("ک", "ك");
        }

        public static string ConvertDataTableToJSON(DataTable table)
        {
            return JsonConvert.SerializeObject(table);
        }

        public static string ConvertToEnglishNumber(string number)
        {
            var arabic = new string[10] { "٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩" };
            var persian = new string[10] { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };

            var loopLength = 10;
            for(var i = 0; i < loopLength; i++)
            {
                var iAsNum = i.ToString();
                number = number.Replace(persian[i], iAsNum)
                               .Replace(arabic[i], iAsNum);
            }

            return number;
        }

        public static DateTime ConvertToLatinDate(string shamsiDate, string format = "yyyy-MM-dd", char spliter = '/')
        {
            if(format is null)
            {
                throw new ArgumentNullException(nameof(format));
            }

            var pc = new PersianCalendar();
            var shamsiList = shamsiDate.Split(spliter);
            var year = Convert.ToInt32(shamsiList[0]);
            var month = Convert.ToInt32(shamsiList[1]);
            var day = Convert.ToInt32(shamsiList[2]);

            return new DateTime(year, month, day, pc);
        }

        public static DateTime ConvertToLatinDateTime(string shamsiDate,string time)
        {
            var date = ConvertToLatinDate(shamsiDate);
            var splitedTime = time.Split(':');
            var hour = Convert.ToInt32(splitedTime[0]);
            var miniute = Convert.ToInt32(splitedTime[1]);
            date += new TimeSpan(hour, miniute, 0);
            return date;
        }

        public static List<dynamic> GetEnumList<T>()
        {
            var typeOfDescriptionAttribut = typeof(DescriptionAttribute);

            return Enum.GetValues(typeof(T))
                                .Cast<Enum>()
                                .Select(value => new
                                {
                                    (Attribute.GetCustomAttribute(value.GetType()
                                                                       .GetField(value.ToString()),
                                                                  typeOfDescriptionAttribut)
                                    as DescriptionAttribute).Description,
                                    value
                                })
                                .OrderBy(item => item.value)
                                .ToList<dynamic>();
        }

        public static string GetGenericName<T>()
        {
            var GenericType = typeof(T);
            string name;
            if(GenericType.IsGenericType)
            {
                var GenericName = GenericType.GetGenericTypeDefinition();
                name = GenericName.Name.Substring(0, GenericType.Name.IndexOf('`'));
            }
            else
            {
                name = GenericType.Name;
            }

            return name;
        }

        public static object GetGenericObjectSpecialFieldValue<T>(T GenericItem, string FieldName)
        {
            if(GenericItem == null)
            {
                return null;
            }

            var type = typeof(T);
            var properties = type.GetProperties();

            foreach(var propertyInfo in properties)
            {
                var namesEqual = propertyInfo.Name.ToLower().Trim() == FieldName.ToLower().Trim();
                if(!namesEqual)
                {
                    continue;
                }

                return propertyInfo.GetValue(GenericItem);
            }

            return null;
        }

        public static T GetJsonRequest<T>(string requestUrl)
        {
            try
            {
                var apiRequest = WebRequest.Create(requestUrl);
                var apiResponse = (HttpWebResponse)apiRequest.GetResponse();

                var isResponceValid = apiResponse.StatusCode == HttpStatusCode.OK;
                if(!isResponceValid)
                {
                    return default;
                }

                var jsonOutput = "";
                using(var streamReadeer = new StreamReader(apiResponse.GetResponseStream()))
                {
                    jsonOutput = streamReadeer.ReadToEnd();
                }

                var jsResult = JsonConvert.DeserializeObject<T>(jsonOutput);
                return jsResult != null ? jsResult : default;
            }
            catch(Exception)
            {
                return default;
            }
        }

        public static T GetXmlRequest<T>(string requestUrl)
        {
            try
            {
                var apiRequest = WebRequest.Create(requestUrl);
                var apiResponse = (HttpWebResponse)apiRequest.GetResponse();

                var isResponceValid = apiResponse.StatusCode == HttpStatusCode.OK;
                if(!isResponceValid)
                {
                    return default;
                }

                var xmlOutput = "";
                using(var streamReader = new StreamReader(apiResponse.GetResponseStream()))
                {
                    xmlOutput = streamReader.ReadToEnd();
                }

                var xmlSerialize = new XmlSerializer(typeof(T));
                var xmlResult = (T)xmlSerialize.Deserialize(new StringReader(xmlOutput));

                return xmlResult != null ? xmlResult : default;
            }
            catch(Exception)
            {
                return default;
            }
        }

        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            var ignoreCase = StringComparison.InvariantCultureIgnoreCase;
            for(var i = 0; i < dr.FieldCount; i++)
            {
                var isColumnNameEqual = dr.GetName(i).Equals(columnName, ignoreCase);
                if(isColumnNameEqual)
                {
                    return true;
                }
            }

            return false;
        }

        public static T Map<T>(IDataRecord record)
        {
            var DataTransferObject = Activator.CreateInstance<T>();
            foreach(var property in typeof(T).GetProperties())
            {
                var hasColumn = record.HasColumn(property.Name);
                var value = record.GetOrdinal(property.Name);
                var hasValue = !record.IsDBNull(value);

                if(hasColumn && hasValue)
                {
                    property.SetValue(DataTransferObject, record[property.Name]);
                }
            }

            return DataTransferObject;
        }

        public static void RemovedCallback(string key, object value, CacheItemRemovedReason reason)
        {
            var fileName = value.ToString();
            var fileExist = File.Exists(fileName);
            if(fileExist)
            {
                File.Delete(fileName);
            }
        }

        public static void RemoveFileWithDelay(string key, string fullPath, int delay)
        {
            if(HttpUtils.GetCache()[key] != null)
            {
                return;
            }

            var timeSpan = new TimeSpan(0, delay, 0);
            var absoluteExpiration = DateTime.Now.Add(timeSpan);
            var cacheItem = new CacheItemRemovedCallback(RemovedCallback);

            HttpUtils.GetCache().Insert(key, fullPath, null, absoluteExpiration,
                                            Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, cacheItem);
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            var properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach(PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                _ = table.Columns.Add(prop.Name, type);
            }
            foreach(var item in data)
            {
                var row = table.NewRow();
                foreach(PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }

            return table;
        }

        public static DataTable ToDataTable<T>(this T item)
        {
            var properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach(PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                _ = table.Columns.Add(prop.Name, type);
            }

            var row = table.NewRow();
            foreach(PropertyDescriptor prop in properties)
            {
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            }
            table.Rows.Add(row);

            return table;
        }

        public static string ToShamsiDateString(this DateTime value, string seperator = "/")
        {
            var pc = new PersianCalendar();
            var year = pc.GetYear(value);
            var month = pc.GetMonth(value).ToString("00");
            var day = pc.GetDayOfMonth(value).ToString("00");

            return year + seperator + month + seperator + day;
        }

        public static T[] PrependGetAllItemToArray<T>(T[] arrayToAppend, T getAllItem)
        {
            var newArray = new T[arrayToAppend.Length + 1];
            newArray[0] = getAllItem;                              
            Array.Copy(arrayToAppend, 0, newArray, 1, arrayToAppend.Length);
            return newArray;
        }

    }
}