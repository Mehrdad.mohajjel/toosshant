﻿using System;

namespace ClientApp.Models.DomainModels
{
    public class Tbl_User
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string NickName { get; set; }
        public Nullable<int> Status { get; set; }

    }
}