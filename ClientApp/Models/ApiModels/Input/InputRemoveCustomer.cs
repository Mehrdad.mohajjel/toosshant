﻿using System.ComponentModel.DataAnnotations;

namespace ClientApp.Models.ApiModels.Input
{
    public class InputRemoveCustomer
    {
        [Required]
        public long CustomerID { get; set; }


        [Required]
        public int UserID { get; set; }
    }
}