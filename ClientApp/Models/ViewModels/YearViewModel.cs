﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientApp.Models.ViewModels
{
    public class YearViewModel
    {
        public int Id { get; set; }
        public int PersianYear { get; set; }
    }
}