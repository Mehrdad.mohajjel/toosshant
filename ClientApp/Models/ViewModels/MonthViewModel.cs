﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientApp.Models.ViewModels
{
    public class MonthViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
    }
}