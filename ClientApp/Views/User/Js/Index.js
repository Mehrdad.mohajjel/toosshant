﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.user = (function () {

    var dom = {};
    var state = {
        filter: {
            username: "",
            email: "",
            nickname: "",
            isValid: false
        }
    };
    var tools = motorsazanClient.tools;
    function addUser() {
        var canAssign = isAddFormValid();
        if (!canAssign) return false;


        var apiParam = {
            UserName: dom.addUserName.GetText(),
            Password: dom.addPassword.GetText(),
            Mobile: dom.addUserMobile.GetText() ,
            EmailAddress: dom.addUserEmail.GetText() ,
            NickName: dom.addNickNameTXT.GetText() 
        };
        var url = "/User/AddUser";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                motorsazanClient.messageModal.success(apiResult);
                resetAddForm();
                refreshUserGrid();
            });
    }

    function getActiveRowKey() {
        var activeIndex = dom.peopleGrid.GetFocusedRowIndex();
        return dom.peopleGrid.GetRowKey(activeIndex);
    }


    function searchForUser() {

        state.filter.username = dom.filterUserName.GetText();
        state.filter.nickname = dom.filterNickName.GetText();
        var url = "/User/ReportGrid";

        var apiParam = {
            username: state.filter.username,
            nickname: state.filter.nickname
        };

        motorsazanClient.connector.post(url, apiParam)
            .then(function (reportContent) {
                dom.reportGridParent.html(reportContent);
            });
    }

    function handlUserGridCustomBtnClick(source, event) {
        var buttonId = event.buttonID;
        if (buttonId === "resetPasswordCutsomBtn") return showresetPassModal();
        if (buttonId === "changeStatusCutsomBtn") return showchangeStatusModal();
        if (buttonId === "removeCutsomBtn") return showRemoveModal();
        
    }
    function showresetPassModal() {
        var title = "ریست رمز";
        var content = "آیا از ریست کردن رمز عبور مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, resetPassword, title);
    }
    function showchangeStatusModal() {
        var title = "تغییر وضعیت";
        var content = "آیا از تغییر وضعیت کاربر مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, changeStatus, title);
    }
    function showRemoveModal() {
        var title = "حذف کاربر";
        var content = "آیا از حذف کاربر مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removeUser, title);
    }
    

    function isAddFormValid() {
        var isValid = true;

        tools.hideItem(dom.addUsernameError);
        var userName = dom.addUserName.GetText();
        var userNameHasValue = !tools.isNullOrEmpty(userName);
        if (!userNameHasValue) {
            isValid = false;
            tools.showItem(dom.addUsernameError);
        }

        tools.hideItem(dom.addPasswordError);
        var password = dom.addPassword.GetValue();
        var passwordHasValue = !tools.isNullOrEmpty(password);
        if (!passwordHasValue) {
            isValid = false;
            tools.showItem(dom.addPasswordError);
        }

        tools.hideItem(dom.addNickNameError);
        var addNickName = dom.addNickNameTXT.GetText();
        var addNickNameHasValue = !tools.isNullOrEmpty(addNickName);
        if (!addNickNameHasValue) {
            isValid = false;
            tools.showItem(dom.addNickNameError);
        }

        tools.hideItem(dom.addUsermobileError);
        var UserMobile = dom.addUserMobile.GetText();
        var UserMobileHasValue = !tools.isNullOrEmpty(UserMobile);
        if (!UserMobileHasValue) {
            isValid = false;
            tools.showItem(dom.addUsermobileError);
        }

        tools.hideItem(dom.addUserEmailError);
        var UserEmail = dom.addUserEmail.GetText();
        var UserEmailHasValue = !tools.isNullOrEmpty(UserEmail);
        var IsEmailValid = tools.isValidEmail(UserEmail);
        if (!UserEmailHasValue && !IsEmailValid) {
            isValid = false;
            tools.showItem(dom.addUserEmailError);
        }

        return isValid;
    }
    function resetAddForm() {
        dom.addUserName.SetValue('');
        dom.addPassword.SetValue('');
        dom.addNickNameTXT.SetValue('');
        dom.addUserMobile.SetValue('');
        dom.addUserEmail.SetValue('');

    }



    function refreshUserGrid() {
        var apiParam = {
        };
        var url = "/user/ReportGrid";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (gridHtml) {
                dom.reportGridParent.html(gridHtml);
                setDom();
            });
    }

    function resetPassword() {
        var apiParam = {
            userID: getActiveRowKey()
        };
        var url = "/user/resetPassword";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                refreshUserGrid();
                motorsazanClient.messageModal.hide();
                motorsazanClient.messageModal.success(apiResult);
            });
    }
    function changeStatus() {
        var apiParam = {
            userID: getActiveRowKey()
        };
        var url = "/user/changeStatus";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                refreshUserGrid();
                motorsazanClient.messageModal.hide();
                motorsazanClient.messageModal.success(apiResult);
            });
    }
    function removeUser() {
        var apiParam = {
            userID: getActiveRowKey()
        };
        var url = "/user/removeUser";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                refreshUserGrid();
                motorsazanClient.messageModal.hide();
                motorsazanClient.messageModal.success(apiResult);
            });
    }
    
    function getActiveRowKey() {
        var activeIndex = dom.reportGrid.GetFocusedRowIndex();
        return dom.reportGrid.GetRowKey(activeIndex);
    }


    function setDom() {
        // add form
        dom.addUsernameParent = $("#addUsernameParent");
        dom.addUserName = ASPxClientTextBox.Cast("UserNameTXT");
        dom.addUsernameError = $("#addUsernameError");


        dom.addPasswordParent = $("#addPasswordParent");
        dom.addPassword = ASPxClientTextBox.Cast("PasswordTXT");
        dom.addPasswordError = $("#addPasswordError");

        dom.addNickNameParent = $("#addNickNameParent");
        dom.addNickNameTXT = ASPxClientTextBox.Cast("NickNameTXT");
        dom.addNickNameError = $("#addNickNameError");

        dom.addUsermobileParent = $("#addUsermobileParent");
        dom.addUserMobile = ASPxClientTextBox.Cast("UserMobileTXT");
        dom.addUsermobileError = $("#addUsermobileError");

        dom.addUserEmailParent = $("#addUserEmailParent");
        dom.addUserEmail = ASPxClientTextBox.Cast("UserEmailTXT");
        dom.addUserEmailError = $("#addUserEmailError");

        // filter form
        dom.filterUsernameParent = $("#filterUsernameParent");
        dom.filterUserName = ASPxClientTextBox.Cast("filterUserNameTXT");
        dom.filterUsernameError = $("#filterUsernameError");

        dom.filterNickNameParent = $("#filterNickNameParent");
        dom.filterNickName = ASPxClientTextBox.Cast("filterNickNameTXT");
        dom.filterNickNameError = $("#filterNickNameError");




        // people grid
        dom.reportGridParent = $("#reportGridParent");
        dom.reportGrid = ASPxClientGridView.Cast("reportGrid");
        // transform modal
        dom.transformModalDepartmentCombo = ASPxClientComboBox.Cast("transformModalDepartmentCombo");
        dom.transferModalDepartmentComboParent = $("#transferModalDepartmentComboParent");
        dom.transferModalDepartmentComboError = $("#transferModalDepartmentComboError");
    }

    function showRemoveConfirmation() {
        var title = "تاییدیه حذف";
        var content = "آیا از حذف این ردیف مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removePeople, title);
    }

    function setReportGridCallbackUrl(source) {
        var url = "/User/ReportGrid?";
        url += "&username=" + state.filter.username;
        url += "&nickname=" + state.filter.nickname;

        source.callbackUrl = url;
    }

   
    $(document).ready(setDom);

    return {
        addUser: addUser,
        setReportGridCallbackUrl: setReportGridCallbackUrl,
        searchForUser: searchForUser,
        refreshUserGrid: refreshUserGrid,
        handlUserGridCustomBtnClick: handlUserGridCustomBtnClick,
        resetPassword: resetPassword,
    };

})();