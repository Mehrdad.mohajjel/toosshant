﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.customer = (function () {

    var dom = {};
    var state = {
        activeRowValues: null,
        filter: {
            startDate: "",
            endDate: "",
            isValid: false
        }
    };
    var tools = motorsazanClient.tools;
    function addCustomer() {
        var canAssign = isAddFormValid();
        if (!canAssign) return false;


        var apiParam = {
            FirstName: dom.addCustomerName.GetValue(),
            LastName: dom.addCustomerLastName.GetValue(),
            Code: dom.addCustomerCode.GetValue() ,
            Mobile: dom.addCustomerMobile.GetValue(),
            CustomerTypeId: dom.addCusotmerTypeCombo.GetValue(),
        };
        var url = "/Customer/addCustomer";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                motorsazanClient.messageModal.success(apiResult);
                resetAddForm();
                refreshCustomerGrid();
            });
    }
    function deleteCustomer() {

        var url = "/Customer/deleteCustomer";
        var params = {
            Id: state.activeRowValues.Id,
        };

        motorsazanClient.connector.post(url, params)
            .then(function (resultMessage) {
                refreshCustomerGrid();
                motorsazanClient.contentModal.hide();
                motorsazanClient.messageModal.success(resultMessage);
            });
    }
    function getActiveRowKey() {
        var activeIndex = dom.peopleGrid.GetFocusedRowIndex();
        return dom.peopleGrid.GetRowKey(activeIndex);
    }
    function getFocusedRowValues() {
        var index = dom.reportGrid.GetFocusedRowIndex();
        var fields = "Id;Code;" +
            "FirstName;lastName;" +
            "Mobile;CreationFaDate;IsActiveTitle" ;

        return new Promise((resolve, reject) => {
            dom.reportGrid.GetRowValues(index, fields, function (values) {
                var result = {
                    Id: values[0],
                    Code: values[1],
                    FirstName: values[2],
                    LastName: values[3],
                    Mobile: values[4],
                    CreationFaDate: values[5],
                    IsActiveTitle: values[6]
                };
                state.activeRowValues = result;
                resolve(result);
            });
        });
    }
    function getReport() {
        var canGetReport = isFilterFormValid();
        if (!canGetReport) return;

        state.filter.startDate = dom.startDatePicker.val();
        state.filter.endDate = dom.endDatePicker.val();
        var url = "/Customer/ReportGrid";

        var apiParam = {
            persinanStartDate: state.filter.startDate,
            persianEndDate: state.filter.endDate
        };

        motorsazanClient.connector.post(url, apiParam)
            .then(function (reportContent) {
                dom.reportGridParent.html(reportContent);
            });
    }


    function isAddFormValid() {
        var isValid = true;
        tools.hideItem(dom.addCustomerCodeError);
        var CustomerCode = dom.addCustomerCode.GetText();
        var customerCodeHasValue = !tools.isNullOrEmpty(CustomerCode);
        if (!customerCodeHasValue) {
            isValid = false;
            tools.showItem(dom.addCustomerCodeError);
        }

        tools.hideItem(dom.addCustomerNameError);
        var firstName = dom.addCustomerName.GetValue();
        var firstNameHasValue = !tools.isNullOrEmpty(firstName);
        if (!firstNameHasValue) {
            isValid = false;
            tools.showItem(dom.addCustomerNameError);
        }

        tools.hideItem(dom.addCustomerLastNameError);
        var lastName = dom.addCustomerLastName.GetValue();
        var lastNameHasValue = !tools.isNullOrEmpty(lastName);
        if (!lastNameHasValue) {
            isValid = false;
            tools.showItem(dom.addCustomerLastNameError);
        }

        tools.hideItem(dom.addCustomerMobileError);
        var mobile = dom.addCustomerMobile.GetValue();
        var mobileHasValue = !tools.isNullOrEmpty(mobile);
        if (!mobileHasValue) {
            isValid = false;
            tools.showItem(dom.addCustomerMobileError);
        }

        tools.hideItem(dom.addCustomerTypeError);
        var customer = dom.addCusotmerTypeCombo.GetValue();
        var cusotmerHasValue = !tools.isNullOrEmpty(customer);
        if (!cusotmerHasValue) {
            isValid = false;
            tools.showItem(dom.addCustomerTypeError);
        }


        return isValid;
    }
    function resetAddForm() {
        dom.addCustomerMobile.SetValue('');
        dom.addCustomerName.SetValue('');
        dom.addCustomerLastName.SetValue('');
        getLastCode();

    }
    function getLastCode() {
        var url = "/Customer/CalculateLastCode";
        motorsazanClient.connector.post(url)
            .then(function (gridHtml) {
                console.log(gridHtml);
                dom.addCustomerCodeParent.html(gridHtml);
                setDom();
            });
    }
    function isFilterFormValid() {
        var isValid = true;



        tools.hideItem(dom.startDatePickerError);
        var startDate = dom.startDatePicker.val();
        var isStartDateValid = tools.isValidPersianDate(startDate);
        if (!isStartDateValid) {
            isValid = false;
            tools.showItem(dom.startDatePickerError);
        }
        return isValid;

    }

    function refreshCustomerGrid() {
        var parameters = {
        };
        var url = "/Customer/ReportGrid";

        motorsazanClient.connector.post(url, parameters)
            .then(function (stopCodeGridHtmlContent) {
                dom.reportGridParent.html(stopCodeGridHtmlContent);
                setDom();
            });
    }

    function removePeople() {
        var apiParam = {
            productionLineEmployeeId: getActiveRowKey()
        };
        var url = "/assignPeople/removePeople";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                refreshCustomerGrid();
                motorsazanClient.messageModal.hide();
                motorsazanClient.messageModal.success(apiResult);
            });
    }


    function setCustomerGridFocusedRowOnExpanding(source, event) {
        source.SetFocusedRowIndex(event.visibleIndex);
    }

    function setKarkardModalContents() {
        setDom();
        getFocusedRowValues()
            .then(function (rowValues) {
                dom.editModalRequestDescriptionMemo.SetValue(rowValues.requestDescription);
                dom.editModalStopTitleInput.SetValue(rowValues.stopTitle);
                dom.editModalDepartmentCombo.SetValue(rowValues.departmentID);
            });
    }

    function setDom() {
        // add form
        dom.addCustomerCodeParent = $("#addCustomerCodeParent");
        dom.addCustomerCode = ASPxClientTextBox.Cast("CustomerCodeTXT");
        dom.addCustomerCodeError = $("#addCustomerCodeError");

        dom.addCustomerNameParent = $("#addCustomerNameParent");
        dom.addCustomerName = ASPxClientTextBox.Cast("CustomerFirstNameTXT");
        dom.addCustomerNameError = $("#addCustomerNameError");

        dom.addCustomerLastNameParent = $("#addCustomerLastNameParent");
        dom.addCustomerLastName = ASPxClientTextBox.Cast("CustomerLastNameTXT");
        dom.addCustomerLastNameError = $("#addCustomerLastNameError");

        dom.addCustomerMobileParent = $("#addCustomermobileParent");
        dom.addCustomerMobile = ASPxClientTextBox.Cast("CustomerMobileTXT");
        dom.addCustomerMobileError = $("#addCustomermobileError");

        dom.addCustomerTypeParent = $("#addCustomerTypeParent");
        dom.addCusotmerTypeCombo = ASPxClientComboBox.Cast("addCusotmerTypeCombo");
        dom.addCustomerTypeError = $("#addCustomerTypeError");


        // filter form
        dom.startDatePicker = $("#filterFormStartDatePicker");
        dom.startDatePickerError = $("#filterFormStartDatePickerError");
        dom.endDatePicker = $("#filterFormEndDatePicker");
        dom.endDatePickerError = $("#filterFormEndDatePickerError");
        // people grid
        dom.reportGridParent = $("#reportGridParent");
        dom.reportGrid = ASPxClientGridView.Cast("reportGrid");
        // transform modal
        dom.transformModalDepartmentCombo = ASPxClientComboBox.Cast("transformModalDepartmentCombo");
        dom.transferModalDepartmentComboParent = $("#transferModalDepartmentComboParent");
        dom.transferModalDepartmentComboError = $("#transferModalDepartmentComboError");
    }

    function showRemoveConfirmation() {
        var title = "تاییدیه حذف";
        var content = "آیا از حذف این ردیف مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removePeople, title);
    }
    function showConfirmModal() {
        getFocusedRowValues()
            .then(function (rowValues) {
                var title = "تاییدیه";
                var content = "آیا از تایید " + rowValues.stopTitle + " مطمئن هستید؟";
                motorsazanClient.messageModal.confirm(content, confirmStopCodeByDepartmentManager, title);
            });
    }
    function showDeleteConfirmationModal() {
        getFocusedRowValues()
            .then(function (rowItems) {
                var title = "تاییدیه حذف";
                var url = "/Customer/DeleteModal";
                var params = {
                    label: "آیا از حذف" + rowItems.FirstName + "  " + rowItems.LastName + " مطمئن هستید؟"
                };
                motorsazanClient.contentModal.ajaxShow(title, url, params, setDom);
            });
    }
    function showStatusHistoryModal() {
        getFocusedRowValues()
            .then(function (rowValues) {
                var url = "/Customer/HistoryModal";
                var params = {
                    CustomerID: rowValues.Id
                };
                var title = "نمایش لیست مغازه ها";

                motorsazanClient.contentModal.ajaxShow(title, url, params);
            });    }
    function showFinalVerifyConfirmationModal() {
        getFocusedRowValues()
            .then(function (rowValues) {
                var title = "تایید نهایی";
                var content = "آیا از تایید نهایی " + rowValues.stopTitle + " مطمئن هستید؟";
                motorsazanClient.messageModal.confirm(content, verifyByProductionHead, title);
            });
    }

    function setReportGridCallbackUrl(source) {
        var url = "/Customer/ReportGrid?";
        url += "&persinanStartDate=" + state.filter.startDate;
        url += "&persianEndDate=" + state.filter.endDate;

        source.callbackUrl = url;
    }

   
    $(document).ready(setDom);

    return {
        addCustomer: addCustomer,
        setReportGridCallbackUrl: setReportGridCallbackUrl,
        showConfirmModal: showConfirmModal,
        deleteCustomer: deleteCustomer,
        refreshCustomerGrid: refreshCustomerGrid,
        showDeleteConfirmationModal: showDeleteConfirmationModal,
        showFinalVerifyConfirmationModal: showFinalVerifyConfirmationModal,
        setCustomerGridFocusedRowOnExpanding: setCustomerGridFocusedRowOnExpanding,
        showStatusHistoryModal: showStatusHistoryModal,
    };

})();