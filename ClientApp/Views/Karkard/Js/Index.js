﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.karkard = (function () {

    var dom = {};
    var state = {
        filter: {
            startDate: "",
            endDate: "",
            isValid :true,
        },
        addform: {
            startDate: "",
            endDate: "",
            customerShopID: 0,
            startInput: "",
            endInput: "",
            PriceId: 0,
        }
    };
    var tools = motorsazanClient.tools;

    function addExpence() {
        var canaddExpence = isAddFormValid();
        if (!canaddExpence) return false;

        state.addform.customerShopID = dom.addCustomerShopCombo.GetValue() ;
        state.addform.startDate = dom.startDatePicker.val();
        state.addform.endDate = dom.endDatePicker.val();
        state.addform.startInput = dom.addKarkardStartInput.GetValue();
        state.addform.endInput = dom.addKarkardEndtInput.GetValue();
        state.addform.PriceId = dom.addPriceCombo.GetValue();
        var apiParam = {
            CustomerShopID: state.addform.customerShopID,
            StartInput: state.addform.startInput,
            EndInput: state.addform.endInput,
            StartDate: state.addform.startDate,
            EndDate: state.addform.endDate,
            PriceID: state.addform.PriceId,
        };
        var url = "/Karkard/addExpence";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                motorsazanClient.messageModal.success(apiResult);
                resetAddForm();
                refreshKarkardGrid();
            });
    }

    function getActiveRowKey() {
        var activeIndex = dom.peopleGrid.GetFocusedRowIndex();
        return dom.peopleGrid.GetRowKey(activeIndex);
    }

    function handlePeopleGridBeginCallback(source) {
        source.callbackUrl = "/assignPeople/peopleGrid?departmentId=" +
            state.filter.department; 
    }

    function handlReportGridCustomBtnClick(source, event) {
        var buttonId = event.buttonID;
        if (buttonId === "reportGridPrintCutsomBtn") return window.open("/print/printPerson?KarkardID=" + getActiveRowKey());
    }
    function print() {
        return window.open("/print/PrintAllByDAte?StartDate=" +state.filter.startDate + "&EndDate="+ state.filter.endDate);
    }
    function handleAddFormCustomerComboChange() {
        refreshAddFormCustomerShopCombo();
    }
    function handleAddFormCustomerShopBeginCallback(command) {
        var cusotmerID = dom.addFormCustomerCombo.GetValue() || 0;
        command.callbackUrl = "/karkard/AddFormCustomerShopCombo?cusotmerID=" + cusotmerID;
    }


    function isAddFormValid() {
        var isValid = true;

        tools.hideItem(dom.addPriceComboError);
        var price = dom.addPriceCombo.GetValue();
        var priceHasValue = !tools.isNullOrEmpty(price);
        if (!priceHasValue) {
            isValid = false;
            tools.showItem(dom.addPriceComboError);
        }

        tools.hideItem(dom.addFormCustomerComboError);
        var customer = dom.addFormCustomerCombo.GetValue();
        var customerHasValue = !tools.isNullOrEmpty(customer);
        if (!customerHasValue) {
            isValid = false;
            tools.showItem(dom.addFormCustomerComboError);
        }

        tools.hideItem(dom.addCustomerShopComboError);
        var customerShop = dom.addCustomerShopCombo.GetValue();
        var customerShopHasValue = !tools.isNullOrEmpty(customerShop);
        if (!customerShopHasValue) {
            isValid = false;
            tools.showItem(dom.addCustomerShopComboError);
        }

        tools.hideItem(dom.startDatePickerError);
        var startDate = dom.startDatePicker.val();
        var isStartDateValid = tools.isValidPersianDate(startDate);
        if (!isStartDateValid) {
            isValid = false;
            tools.showItem(dom.startDatePickerError);
        }

        tools.hideItem(dom.endDatePickerError);
        var endDate = dom.endDatePicker.val();
        var isEndDateValid = tools.isValidPersianDate(endDate);
        if (!isEndDateValid) {
            isValid = false;
            tools.showItem(dom.endDatePickerError);
        }

        tools.hideItem(dom.addKarkardStartInputError);
        var startInput = dom.addKarkardStartInput.GetValue();
        var startInputHasValue = !tools.isNullOrEmpty(startInput);
        if (!startInputHasValue) {
            isValid = false;
            tools.showItem(dom.addKarkardStartInputError);
        }

        tools.hideItem(dom.addKarkardEndtInputError);
        var EndInput = dom.addKarkardEndtInput.GetValue();
        var EndInputHasValue = !tools.isNullOrEmpty(EndInput);
        if (!EndInputHasValue) {
            isValid = false;
            tools.showItem(dom.addKarkardEndtInputError);
        }
        return isValid;
    }

    function isFilterFormValid() {
        state.filter.isValid = true;
        tools.hideItem(dom.filterFormStartDatePickerError);
        var startDate = dom.filterFormStartDatePicker.val();
        var isStartDateValid = tools.isValidPersianDate(startDate);
        if (!isStartDateValid) {
            state.filter.isValid = false;
            tools.showItem(dom.filterFormStartDatePickerError);
        }

        tools.hideItem(dom.filterFormEndDatePickerError);
        var startDate = dom.filterFormEndDatePicker.val();
        var isStartDateValid = tools.isValidPersianDate(startDate);
        if (!isStartDateValid) {
            state.filter.isValid = false;
            tools.showItem(dom.filterFormEndDatePickerError);
        }

        return state.filter.isValid;
    }


    function refreshKarkardGrid() {
        var apiParam = {
            StartDate: '',
            EndDate: ''
        };
        var url = "/Karkard/KarkardGrid";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (gridHtml) {
                dom.KarkardGridParent.html(gridHtml);
                setDom();
 });
    }

    function refreshAddFormCustomerShopCombo() {
        var url = "/Karkard/addFormCustomerShopCombo";
        var params = {
            CustomerId: dom.addFormCustomerCombo.GetValue()
        }

        motorsazanClient.connector.post(url, params)
            .then(function (customerShopCombo) {
                dom.addCustomerShopComboParent.html(customerShopCombo);
                setDom();
            });
    }



    function resetAddForm() {
        dom.addFormCustomerCombo.SetValue('');
        dom.addCustomerShopCombo.SetValue('');
        dom.addPriceCombo.SetValue('');
        dom.addKarkardStartInput.SetValue('');
        dom.addKarkardEndtInput.SetValue('');
    }

    function searchForPeople() {
        var isEditForm = isFilterFormValid();
        if (!isEditForm) return false;
        state.filter.startDate = dom.filterFormStartDatePicker.val()
        state.filter.endDate = dom.filterFormEndDatePicker.val()
        refreshFilterKarkardGrid();
        if (state.filter.isValid) {
            dom.filterPrintBTN.SetEnabled(true);
        }

    }
    function refreshFilterKarkardGrid() {
        var apiParam = {
            StartDate: state.filter.startDate,
            EndDate: state.filter.endDate
        };
        var url = "/Karkard/KarkardGrid";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (gridHtml) {
                dom.KarkardGridParent.html(gridHtml);
                setDom();
            });
    }

    function setDom() {
        // add form
        dom.addFormCustomerComboParent = $("#addFormCustomerComboParent");
        dom.addFormCustomerCombo = ASPxClientComboBox.Cast("addFormCustomerCombo");
        dom.addFormCustomerComboError = $("#addFormCustomerComboError");

        dom.addCustomerShopComboParent = $("#addCustomerShopComboParent");
        dom.addCustomerShopCombo = ASPxClientComboBox.Cast("addCustomerShopCombo");
        dom.addCustomerShopComboError = $("#addCustomerShopComboError");

        dom.startDatePicker = $("#addFormStartDatePicker");
        dom.startDatePickerError = $("#addFormStartDatePickerError");

        dom.endDatePicker = $("#AddFormEndDatePicker");
        dom.endDatePickerError = $("#AddFormEndDatePickerError");

        dom.addKarkardStartInput = ASPxClientTextBox.Cast("addKarkardStartInput");
        dom.addKarkardStartInputError = $("#addKarkardStartInputError");

        dom.addKarkardEndtInput = ASPxClientTextBox.Cast("addKarkardEndtInput");
        dom.addKarkardEndtInputError = $("#addKarkardEndInputError");

        dom.addPriceComboParent = $("#addPriceComboParent");
        dom.addPriceCombo = ASPxClientComboBox.Cast("addPriceCombo");
        dom.addPriceComboError = $("#addPriceComboError");

        
        // filter form
        dom.filterPrintBTN = ASPxClientButton.Cast("filterPrintBTN");
        dom.filterFormStartDatePicker = $("#FilterFormStartDatePicker");
        dom.filterFormStartDatePickerError = $("#FilterFormStartDatePickerError");
        dom.filterFormEndDatePicker = $("#FilterFormEndDatePicker");
        dom.filterFormEndDatePickerError = $("#FilterFormEndDatePickerError");


        // people grid
        dom.KarkardGridParent = $("#KarkardGridParent");
        dom.KarkardGrid = ASPxClientGridView.Cast("peopleGrid");
        // transform modal
        dom.transformModalDepartmentCombo = ASPxClientComboBox.Cast("transformModalDepartmentCombo");
        dom.transferModalDepartmentComboParent = $("#transferModalDepartmentComboParent");
        dom.transferModalDepartmentComboError = $("#transferModalDepartmentComboError");
    }
    function getActiveRowKey() {
        var activeIndex = dom.KarkardGrid.GetFocusedRowIndex();
        return dom.KarkardGrid.GetRowKey(activeIndex);
    }
   


   
    $(document).ready(setDom);

    return {
        addExpence: addExpence,
        handlePeopleGridBeginCallback: handlePeopleGridBeginCallback,
        searchForPeople: searchForPeople,
        handleAddFormCustomerComboChange: handleAddFormCustomerComboChange,
        handleAddFormCustomerShopBeginCallback: handleAddFormCustomerShopBeginCallback,
        handlReportGridCustomBtnClick: handlReportGridCustomBtnClick,
        print: print,
    };

})();