﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.assignShop = (function () {

    var tools = motorsazanClient.tools;
    var dom = {};
    var state = {
        filter: {
            complex: 0,
            isValid: false
        }
    };

    function assignNewShop() {
        var canAssign = isAddFormValid();
        if (!canAssign) return false;

        var customerID = dom.addFormCustomerCombo.GetValue();
        var shopID = dom.addFormShopCombo.GetValue();
        var apiParam = {
            CustomerID: customerID,
            ShopID: shopID,
            TabloName: dom.tabloName.GetValue() 
        };
        var url = "/assignShop/assignNewShop";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                motorsazanClient.messageModal.success(apiResult);
                resetAddForm();
                refreshPeopleGrid();
            });
    }

    function fillShopByBazar() {
        var url = "/AssignShop/AddFormShopCombo";
        var apiParams = {
            complexID: dom.addFormBazarCombo.GetValue() || 0
        };

        motorsazanClient.connector.post(url, apiParams)
            .then(function (shoplist) {
                dom.addFormShopComboParent.html(shoplist);
                setDom();
            });
    }

    function getActiveRowKey() {
        var activeIndex = dom.shopGrid.GetFocusedRowIndex();
        console.log(activeIndex);
        console.log(dom.shopGrid.GetRowKey(activeIndex));
        return dom.shopGrid.GetRowKey(activeIndex);
    }




    function isAddFormValid() {
        var isValid = true;
        tools.hideItem(dom.addFormBazarComboError);
        var bazar = dom.addFormBazarCombo.GetValue();
        var bazarrHasValue = !tools.isNullOrEmpty(bazar);
        if (!bazarrHasValue) {
            isValid = false;
            tools.showItem(dom.addFormBazarComboError);
        }

        tools.hideItem(dom.addFormCustomerComboError);
        var customer = dom.addFormCustomerCombo.GetValue();
        var cusotmerHasValue = !tools.isNullOrEmpty(customer);
        if (!cusotmerHasValue) {
            isValid = false;
            tools.showItem(dom.addFormCustomerComboError);
        }

        tools.hideItem(dom.addFormShopComboError);
        var shop = dom.addFormShopCombo.GetValue();
        var shopHasValue = !tools.isNullOrEmpty(shop);
        if (!shopHasValue) {
            isValid = false;
            tools.showItem(dom.addFormShopComboError);
        }

        tools.hideItem(dom.addFormTabloError);
        var tableName = dom.tabloName.GetValue();
        var tbleNameHasValue = !tools.isNullOrEmpty(tableName);
        if (!tbleNameHasValue) {
            isValid = false;
            tools.showItem(dom.addFormTabloError);
        }


        return isValid;
    }

    function isFilterFormValid() {
        tools.hideItem(dom.filterFormBazarComboError);
        var Complex = dom.filterFormBazarCombo.GetValue();
        var bazarHasValue = !tools.isNullOrEmpty(Complex);
        if (bazarHasValue) {
            state.filter.complex = Complex;
            return true;
        }

        tools.showItem(dom.filterFormBazarComboError);
        state.filter.department = 0;
        return false;
    }



    function refreshPeopleGrid() {
        var ComplexId = dom.filterFormBazarCombo.GetValue();
        var apiParam = {
            ComplexId: ComplexId
        };
        var url = "/assignShop/searchShop";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (gridHtml) {
                dom.shopGridParent.html(gridHtml);
                setDom();
            });
    }

    function resetAddForm() {
        dom.tabloName.SetValue('');
        dom.addFormCustomerCombo.SetSelectedIndex(-1);
        dom.addFormShopCombo.SetSelectedIndex(-1);
        dom.addFormBazarCombo.SetSelectedIndex(-1);
    }

    function searchshop() {
        var isEditForm = isFilterFormValid();
        if (!isEditForm) return false;

        refreshPeopleGrid();
    }

    function setCustomerShopComboCallbackUrl(command) {
        var ComplexId = dom.addFormBazarCombo.GetValue() || 0;
        command.callbackUrl = "/AssignShop/searchshop?ComplexId=" + ComplexId;
    }


    function setDom() {
        // add form
        dom.addFormCustomerComboParent = $("#addFormCustomerComboParent");
        dom.addFormCustomerCombo = ASPxClientComboBox.Cast("addFormCusotmerCombo");
        dom.addFormCustomerComboError = $("#addFormCustomerComboError");

        dom.addFormShopComboParent = $("#addFormShopComboParent");
        dom.addFormShopCombo = ASPxClientComboBox.Cast("addFormShopCombo");
        dom.addFormShopComboError = $("#addFormShopComboError");


        dom.addFormBazarComboParent = $("#addFormBazarComboParent");
        dom.addFormBazarCombo = ASPxClientComboBox.Cast("addFormBazarCombo");
        dom.addFormBazarComboError = $("#addFormBazarComboError");
        

        dom.addFormTabloParent = $("#addFormTabloParent");
        dom.tabloName = ASPxClientTextBox.Cast("TabloNameTXT");
        dom.addFormTabloError = $("#addFormTabloError");

        // filter form
        dom.filterFormBazarComboParent = $("#filterFormBazarComboParent");
        dom.filterFormBazarCombo = ASPxClientComboBox.Cast("filterFormBazarCombo");
        dom.filterFormBazarComboError = $("#filterFormBazarComboError");
        // people grid
        dom.shopGridParent = $("#shopGridParent");
        dom.shopGrid = ASPxClientGridView.Cast("shopGrid");
        // transform modal
        dom.transformModalDepartmentCombo = ASPxClientComboBox.Cast("transformModalDepartmentCombo");
        dom.transferModalDepartmentComboParent = $("#transferModalDepartmentComboParent");
        dom.transferModalDepartmentComboError = $("#transferModalDepartmentComboError");
    }
    function handlePeopleGridCustomBtnClick(source, event) {
        var buttonId = event.buttonID;
        if (buttonId === "peopleGridRemoveCutsomBtn") return showRemoveConfirmation();
    }
    function showRemoveConfirmation() {
        var title = "تاییدیه حذف";
        var content = "آیا از حذف این ردیف مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removePeople, title);
    }
    function removePeople() {
        var apiParam = {
            CustomerShopID: getActiveRowKey()
        };
        var url = "/assignshop/removePeople";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                refreshPeopleGrid();
                motorsazanClient.messageModal.hide();
                motorsazanClient.messageModal.success(apiResult);
            });
    }


   
    $(document).ready(setDom);

    return {
        assignNewShop: assignNewShop,
        fillShopByBazar: fillShopByBazar,
        searchshop: searchshop,
        setCustomerShopComboCallbackUrl: setCustomerShopComboCallbackUrl,
        handlePeopleGridCustomBtnClick: handlePeopleGridCustomBtnClick,
    };

})();