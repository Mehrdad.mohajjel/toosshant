﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.complex = (function () {

    var dom = {};
    var state = {
        filter: {
            ComplexTitle: "",
            isValid: false
        }
    };
    var tools = motorsazanClient.tools;
    function addComplex() {
        var canAssign = isAddFormValid();
        if (!canAssign) return false;

        var apiParam = {
            ComplexTitle: dom.addComplexName.GetText()
        };
        var url = "/Complex/AddComplex";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                motorsazanClient.messageModal.success(apiResult);
                resetAddForm();
                refreshUserGrid();
            });
    }

    function getActiveRowKey() {
        var activeIndex = dom.peopleGrid.GetFocusedRowIndex();
        return dom.peopleGrid.GetRowKey(activeIndex);
    }


    function searchForUser() {

        var url = "/Complex/ReportGrid";


        motorsazanClient.connector.post(url, apiParam)
            .then(function (reportContent) {
                dom.reportGridParent.html(reportContent);
            });
    }

    function handlUserGridCustomBtnClick(source, event) {
        var buttonId = event.buttonID;
        if (buttonId === "RemoveComplexBTN") return showRemoveModal();
        
    }

    function showRemoveModal() {
        var title = "حذف بازار";
        var content = "آیا از حذف بازار مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removeComplex, title);
    }
    

    function isAddFormValid() {
        var isValid = true;

        tools.hideItem(dom.addComplexNameError);
        var userName = dom.addComplexName.GetText();
        var userNameHasValue = !tools.isNullOrEmpty(userName);
        if (!userNameHasValue) {
            isValid = false;
            tools.showItem(dom.addComplexNameError);
        }


        return isValid;
    }
    function resetAddForm() {
        dom.addComplexName.SetValue('');

    }



    function refreshUserGrid() {
        var apiParam = {
        };
        var url = "/Complex/ReportGrid";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (gridHtml) {
                dom.reportGridParent.html(gridHtml);
                setDom();
            });
    }

    function removeComplex() {
        var apiParam = {
            ComplexID: getActiveRowKey()
        };
        var url = "/Complex/removeComplex";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                refreshUserGrid();
                motorsazanClient.messageModal.hide();
                motorsazanClient.messageModal.success(apiResult);
            });
    }
    
    function getActiveRowKey() {
        var activeIndex = dom.reportGrid.GetFocusedRowIndex();
        return dom.reportGrid.GetRowKey(activeIndex);
    }


    function setDom() {
        // add form
        dom.addComplexNameParent = $("#addComplexNameParent");
        dom.addComplexName = ASPxClientTextBox.Cast("ComplexNameTXT");
        dom.addComplexNameError = $("#addComplexNameError");

        // people grid
        dom.reportGridParent = $("#reportGridParent");
        dom.reportGrid = ASPxClientGridView.Cast("reportGrid");
    }

    function showRemoveConfirmation() {
        var title = "تاییدیه حذف";
        var content = "آیا از حذف این ردیف مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removePeople, title);
    }

    function setReportGridCallbackUrl(source) {
        var url = "/complex/ReportGrid?";

        source.callbackUrl = url;
    }

   
    $(document).ready(setDom);

    return {
        addComplex: addComplex,
        setReportGridCallbackUrl: setReportGridCallbackUrl,
        searchForUser: searchForUser,
        refreshUserGrid: refreshUserGrid,
        handlUserGridCustomBtnClick: handlUserGridCustomBtnClick,
    };

})();