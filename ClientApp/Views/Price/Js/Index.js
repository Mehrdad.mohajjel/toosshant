﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.price = (function () {

    var dom = {};
    var state = {
        filter: {
            Price: "",
            isValid: false
        }
    };
    var tools = motorsazanClient.tools;
    function addPrice() {
        var canAssign = isAddFormValid();
        if (!canAssign) return false;


        var apiParam = {
            Price: dom.addPrice.GetText()
        };
        var url = "/Price/AddPrice";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                motorsazanClient.messageModal.success(apiResult);
                resetAddForm();
                refreshUserGrid();
            });
    }

    function getActiveRowKey() {
        var activeIndex = dom.reportGrid.GetFocusedRowIndex();
        return dom.reportGrid.GetRowKey(activeIndex);
    }


    function searchForUser() {

        var url = "/Price/ReportGrid";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (reportContent) {
                dom.reportGridParent.html(reportContent);
            });
    }

    function handlUserGridCustomBtnClick(source, event) {
        var buttonId = event.buttonID;
        if (buttonId === "resetPasswordCutsomBtn") return showresetPassModal();
        if (buttonId === "changeStatusCutsomBtn") return showchangeStatusModal();
        if (buttonId === "removeCutsomBtn") return showRemoveModal();
        
    }
    function showchangeStatusModal() {
        var title = "تغییر وضعیت";
        var content = "آیا از تغییر وضعیت مبلغ مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, changeStatus, title);
    }
    function showRemoveModal() {
        var title = "حذف مبلغ";
        var content = "آیا از حذف مبلغ مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removeUser, title);
    }
    

    function isAddFormValid() {
        var isValid = true;

        tools.hideItem(dom.addPriceError);
        var userName = dom.addPrice.GetText();
        var userNameHasValue = !tools.isNullOrEmpty(userName);
        if (!userNameHasValue) {
            isValid = false;
            tools.showItem(dom.addUserError);
        }

       

        return isValid;
    }
    function resetAddForm() {
        dom.addPrice.SetValue('');
    }



    function refreshUserGrid() {
        var apiParam = {
        };
        var url = "/price/ReportGrid";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (gridHtml) {
                dom.reportGridParent.html(gridHtml);
                setDom();
            });
    }

    function changeStatus() {
        var apiParam = {
            PriceID: getActiveRowKey()
        };
        var url = "/Price/changeStatus";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                refreshUserGrid();
                motorsazanClient.messageModal.hide();
                motorsazanClient.messageModal.success(apiResult);
            });
    }
    function removeUser() {
        var apiParam = {
            userID: getActiveRowKey()
        };
        var url = "/Price/removeprice";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                refreshUserGrid();
                motorsazanClient.messageModal.hide();
                motorsazanClient.messageModal.success(apiResult);
            });
    }
    
    function getActiveRowKey() {
        var activeIndex = dom.reportGrid.GetFocusedRowIndex();
        return dom.reportGrid.GetRowKey(activeIndex);
    }


    function setDom() {
        // add form
        dom.addPriceParent = $("#addPriceParent");
        dom.addPrice = ASPxClientTextBox.Cast("PriceTXT");
        dom.addPriceError = $("#addPriceError");

        // people grid
        dom.reportGridParent = $("#reportGridParent");
        dom.reportGrid = ASPxClientGridView.Cast("reportGrid");
    }

    function showRemoveConfirmation() {
        var title = "تاییدیه حذف";
        var content = "آیا از حذف این ردیف مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removePeople, title);
    }

    function setReportGridCallbackUrl(source) {
        var url = "/Price/ReportGrid";

        source.callbackUrl = url;
    }

   
    $(document).ready(setDom);

    return {
        addPrice: addPrice,
        setReportGridCallbackUrl: setReportGridCallbackUrl,
        searchForUser: searchForUser,
        refreshUserGrid: refreshUserGrid,
        handlUserGridCustomBtnClick: handlUserGridCustomBtnClick,
    };

})();