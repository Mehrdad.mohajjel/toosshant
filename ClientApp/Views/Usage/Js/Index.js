﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.usage = (function () {

    var dom = {};
    var state = {
        filter: {
            startDate: "",
            endDate: "",
            isValid: false,
            customerId: 0,
            customerShopId: 0
        }
    };
    var tools = motorsazanClient.tools;
    function addUsage() {
        var canAssign = isAddFormValid();
        if (!canAssign) return false;
        state.filter.startDate = dom.startDatePicker.val();
        state.filter.endDate = dom.endDatePicker.val();
        state.filter.customerId = dom.addCustomerCombo.GetValue();
        var apiParam = {
            CustomerId: dom.addCustomerCombo.GetValue(),
            strtUsage: dom.addStartUsageTXT.GetValue(),
            persinanStartDate: state.filter.startDate,
            endUsage: dom.addEndUsageTXT.GetValue(),
            persianEndDate: state.filter.endDate,

        };
        var url = "/Usage/addUsage";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                motorsazanClient.messageModal.success(apiResult);
                resetAddForm();
                //refreshPeopleGrid();
            });
    }


    function handleUsageGridBeginCallback(source) {
        source.callbackUrl = "/Usage/ReportGrid?customerId=" +
            state.filter.customerId;
    }

    function handlePeopleGridCustomBtnClick(source, event) {
        var buttonId = event.buttonID;
        if (buttonId === "peopleGridTransferCutsomBtn") return showTrasnformModal();
        if (buttonId === "peopleGridRemoveCutsomBtn") return showRemoveConfirmation();
    }

    function isAddFormValid() {
        var isValid = true;

        tools.hideItem(dom.addCustomerError);
        var CustomerCode = dom.addCustomerCombo.GetValue();
        var customerCodeHasValue = !tools.isNullOrEmpty(CustomerCode);
        if (!customerCodeHasValue) {
            isValid = false;
            tools.showItem(dom.addCustomerError);
        }

        tools.hideItem(dom.addCustomerShopError);
        var CustomerShopCode = dom.addCustomerShopCombo.GetValue();
        var customerShopCodeHasValue = !tools.isNullOrEmpty(CustomerShopCode);
        if (!customerShopCodeHasValue) {
            isValid = false;
            tools.showItem(dom.addCustomerShopError);
        }

        tools.hideItem(dom.addStartUsageError);
        var startUsage = dom.addStartUsageTXT.GetValue();
        var startUsageHasValue = !tools.isNullOrEmpty(startUsage);
        if (!startUsageHasValue) {
            isValid = false;
            tools.showItem(dom.addStartUsageError);
        }

        tools.hideItem(dom.addEndUsagerror);
        var endUsage = dom.addEndUsageTXT.GetValue();
        var endUsageHasValue = !tools.isNullOrEmpty(endUsage);
        if (!endUsageHasValue) {
            isValid = false;
            tools.showItem(dom.addEndUsagerror);
        }


        tools.hideItem(dom.startDatePickerError);
        var startDate = dom.startDatePicker.val();
        var isStartDateValid = tools.isValidPersianDate(startDate);
        if (!isStartDateValid) {
            isValid = false;
            tools.showItem(dom.startDatePickerError);
        }

        tools.hideItem(dom.endDatePickerError);
        var endDate = dom.endDatePicker.val();
        var isEndDateValid = tools.isValidPersianDate(endDate);
        if (!isEndDateValid) {
            isValid = false;
            tools.showItem(dom.endDatePickerError);
        }

        return isValid;
    }
    function getReport() {
        var canGetReport = isFilterFormValid();
        if (!canGetReport) return;

        state.filter.startDate = dom.filterStartDatePicker.val();
        state.filter.endDate = dom.filterEndDatePicker.val();
        var url = "/Usage/ReportGrid";

        var apiParam = {
            persinanStartDate: state.filter.startDate,
            persianEndDate: state.filter.endDate,
        };

        motorsazanClient.connector.post(url, apiParam)
            .then(function (reportContent) {
                dom.reportGridParent.html(reportContent);
            });
    }
    function isFilterFormValid() {
        var isValid = true;

       

        tools.hideItem(dom.filterFormStartDatePickerError);
        var startDate = dom.filterFormStartDatePicker.val();
        var isStartDateValid = tools.isValidPersianDate(startDate);
        if (!isStartDateValid) {
            isValid = false;
            tools.showItem(dom.startDatePickerError);
        }

        tools.hideItem(dom.filterFormEndDatePickerError);
        var endDate = dom.filterFormEndDatePicker.val();
        var isendDateValid = tools.isValidPersianDate(endDate);
        if (!isendDateValid) {
            isValid = false;
            tools.showItem(dom.filterFormEndDatePickerError);
        }
        return isValid;

    }


    function handleAddCustomerComboChange() {
        refreshAddFormCustomerShopCombo();
    }


    function refreshAddFormCustomerShopCombo() {
        var url = "/usage/addFormCustomerShopCombo";
        var params = {
            customerId: dom.addCustomerCombo.GetValue()
        }

        motorsazanClient.connector.post(url, params)
            .then(function (CustomerCombo) {
                dom.addCustomerShopParent.html(CustomerCombo);
                setDom();
                dom.addCustomerShopCombo.SetEnabled(true);
            });
    }

    function handleAddCustomerShopComboBeginCallback() {
        var customerId = dom.addCustomerCombo.GetValue() || 0;
        command.callbackUrl = "/usage/AddCustomerShopCombo?customerId=" + customerId;

    }

    function resetAddForm() {
        dom.addCustomerCombo.SetValue("");
        dom.addCustomerShopCombo.SetValue("");

    }
    function setDom() {
        // add form

        dom.addStartUsageParent = $("#addStartUsageParent");
        dom.addStartUsageTXT = ASPxClientTextBox.Cast("StartUsageTXT");
        dom.addStartUsageError = $("#addStartUsageError");

        dom.addEndUsageParent = $("#addEndUsageParent");
        dom.addEndUsageTXT = ASPxClientTextBox.Cast("EndUsageTXT");
        dom.addEndUsagerror = $("#addEndUsagerror");


        dom.addCustomerParent = $("addCustomerParent");
        dom.addCustomerCombo = ASPxClientComboBox.Cast("addFormCustomerCombo");
        dom.addCustomerError = $("#addCustomerError");

        dom.addCustomerShopParent = $("addCustomerShopParent");
        dom.addCustomerShopCombo = ASPxClientComboBox.Cast("addFormCustomerShopCombo");
        dom.addCustomerShopError = $("#addCustomerShopError");

        dom.startDatePicker = $("#addFormStartDatePicker");
        dom.startDatePickerError = $("#addFormStartDatePickerError");
        dom.endDatePicker = $("#addFormEndDatePicker");
        dom.endDatePickerError = $("#addFormEndDatePickerError");

        // filter form


        dom.filterStartDatePicker = $("#filterFomStartDatePicker");
        dom.filterStartDatePickerError = $("#filterFomStartDatePickerError");
        dom.filterEndDatePicker = $("#filterFomEndDatePicker");
        dom.filterEndDatePickerError = $("#filterFomEndDatePickerError");
        // people grid
        dom.reportGridParent = $("#reportGridParent");
        dom.reportGrid = ASPxClientGridView.Cast("reportGrid");
        // transform modal
        dom.transformModalDepartmentCombo = ASPxClientComboBox.Cast("transformModalDepartmentCombo");
        dom.transferModalDepartmentComboParent = $("#transferModalDepartmentComboParent");
        dom.transferModalDepartmentComboError = $("#transferModalDepartmentComboError");
    }

    function showRemoveConfirmation() {
        var title = "تاییدیه حذف";
        var content = "آیا از حذف این ردیف مطمئن هستید؟";

        motorsazanClient.messageModal.confirm(content, removePeople, title);
    }

    function setReportGridCallbackUrl(source) {
        var url = "/Customer/ReportGrid?";
        url += "&persinanStartDate=" + state.filter.startDate;
        url += "&persianEndDate=" + state.filter.endDate;

        source.callbackUrl = url;
    }


    $(document).ready(setDom);

    return {
        addUsage: addUsage,
        getReport: getReport,
        handleUsageGridBeginCallback: handleUsageGridBeginCallback,
        handleAddCustomerShopComboBeginCallback: handleAddCustomerShopComboBeginCallback,
        handleAddCustomerComboChange: handleAddCustomerComboChange,
        handlePeopleGridCustomBtnClick: handlePeopleGridCustomBtnClick,
    };

})();