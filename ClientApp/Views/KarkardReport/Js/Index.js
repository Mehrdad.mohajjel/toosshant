﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.karkardreport = (function () {

    var dom = {};
    var state = {
        filter: {
            Year: "",
            Month: "",
            ComplexID:0,
            isValid :true,
        }
    };
    var tools = motorsazanClient.tools;


    function getActiveRowKey() {
        var activeIndex = dom.peopleGrid.GetFocusedRowIndex();
        return dom.peopleGrid.GetRowKey(activeIndex);
    }


    function handlReportGridCustomBtnClick(source, event) {
        var buttonId = event.buttonID;
        if (buttonId === "reportGridPrintCutsomBtn") return window.open("/print/printPerson?KarkardID=" + getActiveRowKey());
    }
    function print() {
        searchForPeople();
        return window.open("/print/PrintAllByYear?Year=" + state.filter.Year + "&Month=" + state.filter.Month + "&ComplexID= " + state.filter.ComplexID);
    }




    function isFilterFormValid() {
        state.filter.isValid = true;

        tools.hideItem(dom.filterYearComboError);
        var year = dom.filterYearCombo.GetValue();
        var yearHasValue = !tools.isNullOrEmpty(year);
        if (!yearHasValue) {
            state.filter.isValid = false;
            tools.showItem(dom.filterYearComboError);
        }

        tools.hideItem(dom.filterMonthComboError);
        var month = dom.filterMonthCombo.GetValue();
        var monthHasValue = !tools.isNullOrEmpty(month);
        if (!monthHasValue) {
            state.filter.isValid = false;
            tools.showItem(dom.filterMonthComboError);
        }

        tools.hideItem(dom.filterFormBazarComboError);
        var bazar = dom.filterFormBazarCombo.GetValue();
        var bazarHasValue = !tools.isNullOrEmpty(bazar);
        if (!bazarHasValue) {
            state.filter.isValid = false;
            tools.showItem(dom.filterFormBazarComboError);
        }

        return state.filter.isValid;
    }

    function searchForPeople() {
        var isEditForm = isFilterFormValid();
        if (!isEditForm) return false;
        state.filter.Year = dom.filterYearCombo.GetValue();
        state.filter.Month = dom.filterMonthCombo.GetValue();
        state.filter.ComplexID = dom.filterFormBazarCombo.GetValue();
        refreshFilterKarkardGrid();

    }
    function refreshFilterKarkardGrid() {
        var apiParam = {
            Year: dom.filterYearCombo.GetValue(),
            Month: dom.filterMonthCombo.GetValue(),
            ComplexID: dom.filterFormBazarCombo.GetValue()
        };
        var url = "/KarkardReport/KarkardGrid";
        motorsazanClient.connector.post(url, apiParam)
            .then(function (gridHtml) {
                dom.KarkardGridParent.html(gridHtml);
                setDom();
            });
    }

    function setDom() {
      
        
        // filter form
        dom.filterPrintBTN = ASPxClientButton.Cast("filterPrintBTN");
        dom.filterFormStartDatePicker = $("#FilterFormStartDatePicker");

        dom.filterYearComboParent = $("#FilterYearComboParent");
        dom.filterYearCombo = ASPxClientComboBox.Cast("filterYearCombo");
        dom.filterYearComboError = $("#FilterYearComboError");


        dom.filterMonthComboParent = $("#FilterMonthComboParent");
        dom.filterMonthCombo = ASPxClientComboBox.Cast("filterMonthCombo");
        dom.filterMonthComboError = $("#FilterMonthComboError");

        dom.filterFormBazarComboParent = $("#filterFormBazarComboParent");
        dom.filterFormBazarCombo = ASPxClientComboBox.Cast("filterFormBazarCombo");
        dom.filterFormBazarComboError = $("#filterFormBazarComboError");



        // people grid
        dom.KarkardGridParent = $("#KarkardGridParent");
        dom.KarkardGrid = ASPxClientGridView.Cast("peopleGrid");
    }
    function handlReportGridCustomBtnClick(source, event) {
        var buttonId = event.buttonID;
        if (buttonId === "reportGridPrintCutsomBtn") return window.open("/print/printPerson?KarkardID=" + getActiveRowKey());
    }

    function getActiveRowKey() {
        var activeIndex = dom.KarkardGrid.GetFocusedRowIndex();
        return dom.KarkardGrid.GetRowKey(activeIndex);
    }
   


   
    $(document).ready(setDom);

    return {
        searchForPeople: searchForPeople,
        handlReportGridCustomBtnClick: handlReportGridCustomBtnClick,
        print: print,
    };

})();