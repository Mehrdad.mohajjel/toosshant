﻿///<reference path="../../../Client/DevExpressIntellisense/devexpress-web.d.ts" />

window.motorsazanClient = window.motorsazanClient || {};
window.motorsazanClient.login = (function () {

    var dom = {};
    var state = {
        filter: {
            department: 0,
            startDate: "",
            endDate: ""
        }
    };

    var tools = motorsazanClient.tools;

    function login() {
        var canGetReport = isFilterFormValid();
        if (!canGetReport) return;

        var url = "/Login/Login";
        var apiParam = {
            username: dom.UsernameTXT.val(),
            password: dom.PasswordTXT.val(),
        };

        motorsazanClient.connector.post(url, apiParam)
            .then(function (apiResult) {
                document.location.href = "/Home/Index";
            });
            
    }

    function isFilterFormValid() {
        var isValid = true;

        hideItem(dom.UsernameTXTError);
        var Username = dom.UsernameTXT.val();
        var UsernameHasValue = !isNullOrEmpty(Username);
        if (!UsernameHasValue) {
            isValid = false;
            showItem(dom.UsernameTXTError);
        }

        hideItem(dom.PasswordTXTError);
        var password = dom.PasswordTXT.val();
        var passwordHasValue = !isNullOrEmpty(password);
        if (!passwordHasValue) {
            isValid = false;
            showItem(dom.PasswordTXTError);
        }

        return isValid;
    }
    function hideItem(item) {
        item.addClass("app__hide");
    }
    function showItem(item) {
        item.removeClass("app__hide");
        item.removeClass("hide");
    }
    function isNullOrEmpty(value) {
        if (value === null || value === undefined) return true;
        return value.toString().trim() === "";
    }

    function isNumberOnly(value) {
        return (/^\d+$/.test(value));
    }
    function setDom() {
        dom.UsernameTXTError = $("#UsernameTXTError");
        dom.UsernameTXT = $("#UsernameTXT");
        dom.PasswordTXTError = $("#PasswordTXTError");
        dom.PasswordTXT = $("#PasswordTXT");
    }



    $(document).ready(setDom);

    return {
        login: login,
        hideItem: hideItem,
        showItem: showItem,
    };

})();