﻿using ClientApp.Api;
using COMMON;
using System;
using System.Web;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index( )
        {
            return View();
        }
        public void Login(User values)
        {
            var JWT = ApiList.Login(values);
            Session["MohajjelJsonWebToken"] = (COMMON.User) JWT;
        }
    }
}