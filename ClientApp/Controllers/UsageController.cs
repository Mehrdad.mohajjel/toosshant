﻿using ClientApp.Api;
using GabAPI.Models.Inputs.Customer;
using GabAPI.Models.Inputs.CustomerShop;
using GabAPI.Models.Inputs.KarKard;
using GabAPI.Models.Outputs.Customer;
using GabAPI.Models.Outputs.CustomerShop;
using System;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class UsageController : BaseController
    {
        // GET: Usage
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddFormCustomerCombo()
        {
            var partialViewUrl = "~/Views/Usage/AddForm/AddFormCustomerCombo.cshtml";
            var dataSource = GetAllCustomerList();
            return PartialView(partialViewUrl, dataSource);
        }
        public ActionResult AddFormCustomerShopCombo(long CustomerId = 0)
        {
            var partialViewUrl = "~/Views/Usage/AddForm/AddCustomerShopCombo.cshtml";
            var dataSource = GetCustomerShopList(CustomerId);
            return PartialView(partialViewUrl, dataSource);

        }
        public ActionResult AddNewExpence(InputaddExpence values)
        {
            var token = GetUserToken();
            values.UserID = GetCurrentUser().Id ;
            var apiResult = ApiList.AddNewExpence(values, token);
            return Content(apiResult);
        }

        private OutputGetCustomerList[] GetAllCustomerList()
        {

            var apiParam = new InputGetCustomerList();
           
            var token = GetUserToken();
            return ApiList.GetAllCustomerList(apiParam, token);
        }

        public OutputGetCustomerShopListByCustomertID[] GetCustomerShopList(long CustomerID)
        {
            if(CustomerID == 0)
            {
                return new OutputGetCustomerShopListByCustomertID[0];
            }

            var apiParam = new InputGetCustomerShopListByCustomertID
            {
                CustomerID = CustomerID
            };
            var token = GetUserToken();
            return ApiList.GetCustomerShopByCustomerID(apiParam, token);
        }
        public ActionResult ReportGrid(string persinanStartDate = "", string persianEndDate = "")
        {
            var partialViewUrl = "~/Views/Customer/ReportGrid/ReportGrid.cshtml";
            var dataSource = GetReport(persinanStartDate, persianEndDate);
            return PartialView(partialViewUrl, dataSource);

        }
        private OutputGetCustomerList[] GetReport(string persinanStartDate = "", string persianEndDate = "")
        {
            var now = DateTime.Now;
            var isSatrtDateSetted = !string.IsNullOrEmpty(persinanStartDate);
            var startDate = isSatrtDateSetted ? Tools.Tools.shamsi2miladi(persinanStartDate) : now.AddYears(-1);
            var isEndDateSetted = !string.IsNullOrEmpty(persianEndDate);
            var endDate = isEndDateSetted ? Tools.Tools.shamsi2miladi(persianEndDate) : now;

            var apiParam = new InputGetCustomerList
            {
                EndDate = endDate,
                StartDate = startDate
            };
            var token = GetUserToken();
            return ApiList.GetCustomerList(apiParam, token);
        }

    }
}