﻿using ClientApp.Report;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class PrintController : Controller
    {
        // GET: Print
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Print(long CustomerShopId)
        {

            XtraReport report = new A5SingleReport();
            report.Parameters["CustomerShopId"].Value = CustomerShopId;


            using(var ms = new MemoryStream())
            {
                report.ExportToPdf(ms, new PdfExportOptions() { ShowPrintDialogOnOpen = false });
                return ExportDocument(ms.ToArray(), "pdf", "Report.pdf", true);
            }
        }


        [HttpGet]
        public ActionResult PrintPerson(long KarkardID)
        {

            XtraReport report = new A5SingleReport();
            report.Parameters["KarkardID"].Value = KarkardID;


            using(var ms = new MemoryStream())
            {
                report.ExportToPdf(ms, new PdfExportOptions() { ShowPrintDialogOnOpen = false });
                return ExportDocument(ms.ToArray(), "pdf", "Report.pdf", true);
            }
        }
        [HttpGet]
        public ActionResult PrintAllByDAte(string startDate,string endDate, long ComplexID = 0)
        {

            XtraReport report = new A4MonthlyReport();
            report.Parameters["StartDate"].Value = Tools.Tools.ConvertToLatinDate(startDate) ;
            report.Parameters["EndDate"].Value = Tools.Tools.ConvertToLatinDate(endDate);
            report.Parameters["ComplexId"].Value = ComplexID;


            using(var ms = new MemoryStream())
            {
                report.ExportToPdf(ms, new PdfExportOptions() { ShowPrintDialogOnOpen = false });
                return ExportDocument(ms.ToArray(), "pdf", "Report.pdf", true);
            }
        }
        [HttpGet]
        public ActionResult PrintAllByYear(int year ,int month,long ComplexID =0)
        {
            var startDate = year.ToString() + "/" + month.ToString() + "/01";
            var EnSartDate = Tools.Tools.ConvertToLatinDate(startDate);
            var EnEndDate = EnSartDate.AddMonths(1);

            XtraReport report = new A4MonthlyReport();
            report.Parameters["StartDate"].Value = EnSartDate;
            report.Parameters["EndDate"].Value = EnEndDate;
            report.Parameters["ComplexId"].Value = ComplexID;


            using(var ms = new MemoryStream())
            {
                report.ExportToPdf(ms, new PdfExportOptions() { ShowPrintDialogOnOpen = false });
                return ExportDocument(ms.ToArray(), "pdf", "Report.pdf", true);
            }
        }
        
        private FileResult ExportDocument(byte[] document, string format, string fileName, bool isInline)
        {
            string contentType;
            var disposition = isInline ? "inline" : "attachment";

            switch(format.ToLower())
            {
                case "docx":
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
                case "xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case "xlsx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case "mht":
                    contentType = "message/rfc822";
                    break;
                case "html":
                    contentType = "text/html";
                    break;
                case "txt":
                case "csv":
                    contentType = "text/plain";
                    break;
                case "png":
                    contentType = "image/png";
                    break;
                default:
                    contentType = string.Format("application/{0}", format);
                    break;
            }

            Response.AddHeader("Content-Disposition", string.Format("{0}; filename={1}", disposition, fileName));
            return File(document, contentType);
        }

    }
}