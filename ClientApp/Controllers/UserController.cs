﻿using ClientApp.Api;
using COMMON;
using GabAPI.Models.Inputs.User;
using GabAPI.Models.Outputs.User;
using System;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult AddUser(InputAddUser values)
        {
            var ActiveStatus = Convert.ToInt32(SystemEnumsList.UserStatus.Active);
            var purePassword = values.Password;
            var hashedPassword = Tools.Tools.HashString(purePassword);
            values.Password = hashedPassword;

            var item = new User
            {
                Status = ActiveStatus,
                EmailAddress = values.EmailAddress,
                Mobile = values.Mobile,
                UserName = values.UserName,
                Password = values.Password,
                NickName = values.NickName
               
            };

            var token = GetUserToken();
            var apiResult = ApiList.AddUser(item, token);
            return Content(apiResult);
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ReportGrid(string Username = "", string Nickname = "")
        {
            var partialViewUrl = "~/Views/User/ReportGrid/ReportGrid.cshtml";
            var dataSource = GetReport(Username,Nickname);
            return PartialView(partialViewUrl, dataSource);

        }

        private OutputGetAllUserList[] GetReport(string Username = "", string Nickname = "")
        {

            var apiParam = new InputGetAllUserList
            {
                Username = Username,
                NickName = Nickname
            };
            var token = GetUserToken();
            return ApiList.GetAllUserList(apiParam, token);
        }
        public ActionResult ResetPassword(long userID)
        {
            var apiParam = new InputResetPassword
            {
                UserID = userID,
                OperationUserId = GetCurrentUser().Id
            };
            var token = GetUserToken();
            var apiResult = ApiList.ResetPassword(apiParam, token);
            return Content(apiResult);
        }
        public ActionResult RemoveUser(long userID)
        {
            var apiParam = new InputRemoveUser
            {
                UserID = userID,
            };
            var token = GetUserToken();
            var apiResult = ApiList.RemoveUser(apiParam, token);
            return Content(apiResult);
        }
        public ActionResult ChangeStatus(long userID)
        {
            var apiParam = new InputChangeStatus
            {
                UserID = userID
            };
            var token = GetUserToken();
            var apiResult = ApiList.ChangeStatus(apiParam, token);
            return Content(apiResult);
        }

    }
}