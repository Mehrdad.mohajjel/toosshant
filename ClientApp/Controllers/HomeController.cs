﻿using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Session = Session["MohajjelJsonWebToken"];
            return View();
        }
    }
}