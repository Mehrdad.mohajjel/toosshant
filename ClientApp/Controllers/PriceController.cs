﻿using ClientApp.Api;
using COMMON;
using GabAPI.Models.Inputs.Price;
using GabAPI.Models.Outputs.Price;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class PriceController : BaseController
    {
        // GET: Price
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddPrice(InputAddPrice values)
        {

            var item = new Price
            {
                BasePrice = values.Price,
                IsActive = true,
                CreationDate = DateTime.Now
            };

            var token = GetUserToken();
            var apiResult = ApiList.AddPrice(item, token);
            return Content(apiResult);
        }
        public ActionResult ReportGrid()
        {
            var partialViewUrl = "~/Views/Price/ReportGrid/ReportGrid.cshtml";
            var dataSource = GetReport();
            return PartialView(partialViewUrl, dataSource);

        }

        private OutputGetAllPriceList[] GetReport()
        {

            var token = GetUserToken();
            return ApiList.GetAllPrice(token);
        }
        public ActionResult ChangeStatus(long PriceID)
        {
            var apiParam = new InputEditPrice
            {
                PriceID = PriceID
            };
            var token = GetUserToken();
            var apiResult = ApiList.ChangePriceStatus(apiParam, token);
            return Content(apiResult);
        }

    }
}