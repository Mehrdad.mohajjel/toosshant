﻿using ClientApp.Api;
using COMMON;
using GabAPI.Models.Inputs.Customer;
using GabAPI.Models.Inputs.CustomerShop;
using GabAPI.Models.Outputs.Customer;
using GabzApi.Utilities;
using System;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class CustomerController : BaseController
    {
        public ActionResult AddCustomer(Customer values)
        {
            var token = GetUserToken();
            var apiResult = ApiList.AddCustomer(values, token);
            return Content(apiResult);
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddForm()
        {
            var partialViewUrl = "~/Views/Customer/AddForm/AddForm.cshtml";
            return PartialView(partialViewUrl);
        }

        public ActionResult AddFormDepartmentCombo()
        {
            var partialViewUrl = "~/Views/Customer/AddForm/AddFormDepartmentCombo.cshtml";
            return PartialView(partialViewUrl);
        }
        public ActionResult CalculateLastCode()
        {
            var partialViewUrl = "~/Views/Customer/AddForm/CalculateLastCode.cshtml";
            var apiResult = ApiList.GetLastCode();
             ViewBag.LastCode = apiResult.ToString();
            return PartialView(partialViewUrl);

        }
        public ActionResult AddFormCustomerType()
        {
            var partialViewUrl = "~/Views/Customer/AddForm/AddFormCustomerTypeCombo.cshtml";
            var dataSource = GetCustomerTypeList();
            return PartialView(partialViewUrl, dataSource);

        }
        public ActionResult GridDetailRow(long CustomerID,bool IsActive)
        {
            ViewBag.CustomerID = CustomerID;
            ViewBag.IsActive = IsActive;
            ViewBag.UserId = UserTools.GetOnlineUser();
            
            return PartialView("~/Views/Customer/ReportGrid/GridDetailRow.cshtml");
        }
        public ActionResult DeleteModal(string label)
        {
            return PartialView("~/Views/Customer/DeleteModal/DeleteModal.cshtml", label);
        }
        public ActionResult DeleteCustomer(Customer values)
        {
            var token = GetUserToken();
            return Content(ApiList.RemoveCustomer(values, token));
        }

        public ActionResult HistoryModal(InputGetCustomerStatusHistory values)
        {
            var token = GetUserToken();
            var model = ApiList.GetCustomerShopHistory(values, token);
            return PartialView("~/Views/Customer/HistoryModal/HistoryModal.cshtml", model);
        }
        public ActionResult AddKarkardModal()
        {
            return PartialView("~/Views/Customer/Karkard/AddKarkardModal.cshtml");
        }
        public ActionResult ReportGrid(string persinanStartDate = "", string persianEndDate = "")
        {
            var partialViewUrl = "~/Views/Customer/ReportGrid/ReportGrid.cshtml";
            var dataSource = GetReport(persinanStartDate, persianEndDate);
            return PartialView(partialViewUrl, dataSource);

        }
        private COMMON.CustomerType[] GetCustomerTypeList()
        {
            var dataSource = ApiList.GetCustomerTypeList();
            return dataSource;

        }
        private OutputGetCustomerList[] GetReport( string persinanStartDate = "", string persianEndDate = "")
        {
            var now = DateTime.Now;
            var isSatrtDateSetted = !string.IsNullOrEmpty(persinanStartDate);
            var startDate = isSatrtDateSetted ? Tools.Tools.ConvertToLatinDate(persinanStartDate) : now.AddYears(-1);
            var isEndDateSetted = !string.IsNullOrEmpty(persianEndDate);
            var endDate = isEndDateSetted ? Tools.Tools.ConvertToLatinDate(persianEndDate) : now;

            var apiParam = new InputGetCustomerList
            {
                EndDate = endDate,
                StartDate = startDate
            };
            var token = GetUserToken();
            return ApiList.GetCustomerList(apiParam, token);
        }
        private OutputGetCustomerByID GetCustomersDetailByID(InputGetCustomerByID values)
        {
            var token = GetUserToken();
            return ApiList.GetCustomerDetailByID(values, token);
        }

    }
}