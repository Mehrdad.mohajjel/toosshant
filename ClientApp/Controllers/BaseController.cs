﻿using ClientApp.Models.Base;
using System.Web.Mvc;
using System;
using COMMON;
using System.Web;

namespace ClientApp.Controllers
{
    public class BaseController: Controller
    {
        private const string JwtKeyName = "MohajjelJsonWebToken";

        private const string baseLoginDomain = @"/Login";

        private void ClearSessionInfo()
        {
            Session.Clear();
            if(Request.Cookies["userInfo"] != null)
            {
                Response.Cookies["userInfo"].Expires = DateTime.Now.AddDays(-1);
            }
        }

        [HttpPost]
        public bool DoLogin(string userName, string password)
        {
            if(!ModelState.IsValid)
            {
                throw new AlertException("اطلاعات هویتی به درستی وارد نشده است");
            }
            var item = new COMMON.User
            {
                UserName = userName,
                Password = password
            };
            var userJsonWebToken = ClientApp.Api.ApiList.Login(item);

            if(userJsonWebToken == null)
            {
                throw new AlertException("امکان اجرای درخواست فراهم نشد، لطفا با تیم پشتیبانی تماس حاضل فرمایید");
            }

            //  var userInfo = GetUerInfoByJwt(userJsonWebToken);
            if(userJsonWebToken != null)
            {
                SetUserInfoInSessionAndViewBag(userJsonWebToken);
            }

            return true;
        }

        private void ForceUserToLogin()
        {
            var url = "/Login/index";

            var isPostBack = !Request.IsAjaxRequest();
            if(isPostBack)
            {
                Response.Redirect(url);
            }

            throw new LoginException();
        }

        protected COMMON.User GetCurrentUser()
        {
            var token = GetUserToken();
            return token;
        }

        protected object GetDataFromCache(string cacheKey)
        {
            return HttpContext.Cache.Get(cacheKey);
        }

        public COMMON.User GetUserToken()
        {
            var userToken = (COMMON.User)Session[JwtKeyName];
            return userToken;
        }

        private UserJsonClass GetUerInfoByJwt(string userToken)
        {

            var jsonsession = Session["MohajjelJsonWebToken"].ToString();
            var reqCookies = Request.Cookies["userInfo"];

            var stirngarray = userToken.Split('.');
            if(stirngarray.Length != 3)
            {
                userToken = jsonsession;
            }
            else if(reqCookies != null)
            {
                userToken = reqCookies["MohajjelJsonWebToken"].ToString();
            }
            var userJsonWebToken = Api.ApiList.GetUerInfoByJwt(userToken);

            Session["MohajjelJsonWebToken"] = userToken;
            if(userJsonWebToken == null)
            {
                ForceUserToLogin();
                return null;
            }
            var item = new UserJsonClass
            {
                Id = userJsonWebToken.Id,
                EmailAddress = userJsonWebToken.EmailAddress,
                Exp = userJsonWebToken.Exp,
                NickName = userJsonWebToken.NickName,
                UserName = userJsonWebToken.UserName,
            };
            Session.Add(JwtKeyName, item);
            return item;
        }

        private bool IsSessionContainsJwt()
        {
            return Session[JwtKeyName] != null;
        }

        private bool IsUrlContainsJwt()
        {
            return Request.QueryString[JwtKeyName] == null ? false : !string.IsNullOrEmpty(Request.QueryString[JwtKeyName]);
        }

        protected override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            var actionName = context.RouteData.Values["action"].ToString().ToLower();
            if(actionName == "dologin")
            {
                return;
            }

            //var isJwtAvailableInUrl = IsUrlContainsJwt();
            var isJwtIAvailableInSession = IsSessionContainsJwt();

            var isForcedToLogin = !isJwtIAvailableInSession;
            if(isForcedToLogin)
            {
                ForceUserToLogin();
                return;
            }

            var userJsonWebToken = GetUserToken();
           // var userInfo = GetUerInfoByJwt(userJsonWebToken);
            if(userJsonWebToken != null)
            {
                SetUserInfoInSessionAndViewBag(userJsonWebToken );
            }
            else
            {
                ForceUserToLogin();
            }
        }

        protected void SetDataInCache(string cacheKey, dynamic cacheData, int cacheSeconds = 30)
        {
            var expriteTime = DateTime.UtcNow.AddSeconds(cacheSeconds);
            var slideingExpire = System.Web.Caching.Cache.NoSlidingExpiration;
            HttpContext.Cache.Insert(cacheKey, cacheData, null, expriteTime, slideingExpire);
        }

        private void SetUserInfoInSessionAndViewBag(UserJsonClass userInfo, string userJsonWebToken)
        {
            var fullName = string.Format("{0}", userInfo.NickName);

            Session["Id"] = userInfo.Id;
            Session["fullName"] = fullName;
            Session[JwtKeyName] = userJsonWebToken;

            ViewBag.UserFullName = fullName;
            ViewBag.Id = userInfo.Id;
            ViewBag.JWT = userJsonWebToken;

            var avatarBaseUrl = "/Images/PersonalPic/";
            ViewBag.UserAvatarUrl = avatarBaseUrl + "profile.png";
        }
        private void SetUserInfoInSessionAndViewBag(COMMON.User userInfo)
        {
            var fullName = string.Format("{0}", userInfo.NickName);

            Session["Id"] = userInfo.Id;
            Session["fullName"] = fullName;
            Session[JwtKeyName] = userInfo;
            
            ViewBag.UserFullName = fullName;
            ViewBag.Id = userInfo.Id;
            ViewBag.JWT = userInfo;
            var userInfoCookies = new HttpCookie("userInfo");
            userInfoCookies["UserId"] = userInfo.Id.ToString();
            userInfoCookies.Expires.Add(new TimeSpan(100, 1, 0));
            Response.Cookies.Add(userInfoCookies);
        }

        [HttpPost]
        public string SignOut()
        {
            ClearSessionInfo();
            return baseLoginDomain;
        }
    }
}