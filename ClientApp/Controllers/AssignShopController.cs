﻿using ClientApp.Api;
using COMMON;
using GabAPI.Models.Inputs.AssignShop;
using GabAPI.Models.Inputs.CustomerShop;
using GabAPI.Models.Inputs.CustomerShop.AssignPeopleToLine;
using GabAPI.Models.Inputs.CustomerShop.AssignShop;
using GabAPI.Models.Outputs.AssignShop;
using GabAPI.Models.Outputs.Customer;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class AssignShopController: BaseController
    {
        public ActionResult AddForm()
        {
            var partialViewUrl = "~/Views/AssignShop/AddForm/AddForm.cshtml";
            return PartialView(partialViewUrl);
        }
        public ActionResult AddFormShopCombo(long ComplexId = 0)
        {
            var partialViewUrl = "~/Views/AssignShop/AddForm/AddFormShopCombo.cshtml";
            var dataSource = GetShopListByComplexID(ComplexId);
            return PartialView(partialViewUrl, dataSource);
        }
        public ActionResult AddFormCustomerCombo()
        {
            var partialViewUrl = "~/Views/AssignShop/AddForm/AddFormCustomerCombo.cshtml";
            var dataSource = GetCustomerList();
            return PartialView(partialViewUrl, dataSource);

        }
        public ActionResult AddFormBazarCombo()
        {
            var partialViewUrl = "~/Views/AssignShop/AddForm/AddFormBazarCombo.cshtml";
            var dataSource = GetComplexList();
            return PartialView(partialViewUrl, dataSource);
        }

        public ActionResult AssignNewShop(InputAddCustomerToShop values)
        {
            var token = GetUserToken();
            values.UserID = GetCurrentUser().Id;
            var apiResult = ApiList.AddCustomerToShop(values, token);
            return Content(apiResult);
        }
        public ActionResult FilterForm()
        {
            var partialViewUrl = "~/Views/AssignShop/FilterForm/FilterForm.cshtml";
            return PartialView(partialViewUrl);
        }
        public ActionResult FilterFormBazarCombo()
        {
            var partialViewUrl = "~/Views/AssignShop/FilterForm/filterFormBazarCombo.cshtml";
            var dataSource = GetComplexList();
            return PartialView(partialViewUrl, dataSource);

        }
        private Complex[] GetComplexList()
        {
            return ApiList.GetComplexList();
        }
        private Shop[] GetShopListByComplexID(long ComplexID = 0)
        {
            var apiParam = new InputGetShopListByComplexID
            {
                ComplexID = ComplexID
            };
            var token = GetUserToken();
            return ApiList.GetShopListByComplexID(apiParam, token);
        }
        private OutputCustomerShopByComplexID[] GetCustomerShopByComplexID(InputCustomerShopByComplexID values )
        {
            var dataSource = ApiList.GetCustomerShopByComplexID(values);
            return dataSource;
        }
        private OutputGetCustomerList[] GetCustomerList()
        {
            var dataSource = ApiList.GetAllCustomersList();
            return dataSource;
        }
        private object[] GetProductionLineList()
        {
            return null;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult RemovePeople(long? CustomerShopID)
        {
            var apiParam = new InputRemovePeople
            {
                CustomerShopID = CustomerShopID,
                UserID = GetCurrentUser().Id
            };
            var token = GetUserToken();
            var apiResult = ApiList.RemoveShopCustomer(apiParam, token);
            return Content(apiResult);
        }
        public ActionResult SearchShop(int ComplexId = 0)
        {
            var item = new InputCustomerShopByComplexID
            {
                ComplexID = ComplexId
            };
            var partialViewUrl = "~/Views/AssignShop/shopGrid/shopGrid.cshtml";
            var dataSource = GetCustomerShopByComplexID(item);
            return PartialView(partialViewUrl, dataSource);
        }
        public ActionResult TransferModal(long productionLineEmployeeId)
        {
            var dataSource = productionLineEmployeeId;// GetEmployeeCurrentLine(productionLineEmployeeId);
            var partialViewUrl = "~/Views/AssignShop/TransferModal/TransferModal.cshtml";
            return PartialView(partialViewUrl, dataSource);
        }
        public ActionResult TransformModalDepartmentCombo()
        {
            var partialViewUrl = "~/Views/AssignShop/TransferModal/TransformModalDepartmentCombo.cshtml";
            var dataSource = GetProductionLineList();
            return PartialView(partialViewUrl, dataSource);
        }

    }
}