﻿using ClientApp.Api;
using GabAPI.Models.Inputs.CustomerShop;
using GabAPI.Models.Inputs.KarKard;
using GabAPI.Models.Outputs.Customer;
using GabAPI.Models.Outputs.CustomerShop;
using GabAPI.Models.Outputs.KarKard;
using System;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class KarkardController: BaseController
    {
        public ActionResult AddExpence(InputaddExpence values)
        {
            var token = GetUserToken();
            values.UserID = GetCurrentUser().Id;
            var apiResult = ApiList.AddNewExpence(values, token);
            return Content(apiResult);
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddCustomerCombo()
        {
            var partialViewUrl = "~/Views/Karkard/AddForm/AddFormCustomerCombo.cshtml";
            var dataSource = GetCustomerList();
            return PartialView(partialViewUrl, dataSource);
        }
        public ActionResult AddFormCustomerShopCombo(long CustomerId = 0)
        {
            var partialViewUrl = "~/Views/Karkard/AddForm/AddFormCustomerShopCombo.cshtml";
            var dataSource = GetCustomerShopList(CustomerId);
            return PartialView(partialViewUrl, dataSource);

        }

        public ActionResult AddPriceCombo()
        {
            var partialViewUrl = "~/Views/Karkard/AddForm/AddFormPriceCombo.cshtml";
            var dataSource = GetActivePriceList();
            return PartialView(partialViewUrl, dataSource);

        }

        public OutputGetCustomerShopListByCustomertID[] GetCustomerShopList(long CustomerID)
        {
            if(CustomerID == 0)
            {
                return new OutputGetCustomerShopListByCustomertID[0];
            }

            var apiParam = new InputGetCustomerShopListByCustomertID
            {
                CustomerID = CustomerID
            };
            var token = GetUserToken();
            return ApiList.GetCustomerShopByCustomerID(apiParam, token);
        }

        public OutputPrice[] GetActivePriceList()
        {
           
            var token = GetUserToken();
            return ApiList.GetActivePriceList(token);
        }
        public OutputGetCustomerList[] GetCustomerList()
        {
            var cacheKey = "CustomerList";
            var cachedDataSource = GetDataFromCache(cacheKey) as OutputGetCustomerList[];
            var hasCache = cachedDataSource != null && cachedDataSource.Length > 0;
            if(hasCache)
            {
                return cachedDataSource;
            }

            var dataSource = ApiList.GetAllCustomersList();
            SetDataInCache(cacheKey, dataSource, 60);
            return dataSource;
        }
        public OutputGetKarkardDetail[] GetKarkardDetail(InputGetKarkardDetail values)
        {
            var dataSource = ApiList.GetKarkardDetail(values);
            return dataSource;
        }
        public ActionResult KarkardGrid(string StartDate,string EndDate)
        {
            var partialViewUrl = "~/Views/Karkard/KarkardGrid/KarkardGrid.cshtml";
            if(string.IsNullOrEmpty(StartDate))
            {
                StartDate = Tools.Tools.GetFarsiShortDate(DateTime.Now.AddMonths(-3));
            }
            if(string.IsNullOrEmpty(EndDate))
            {
                EndDate = Tools.Tools.GetFarsiShortDate(DateTime.Now);
            }
            var item = new InputGetKarkardDetail()
            {
                StartDate = Tools.Tools.ConvertToLatinDate(StartDate),
                EndDate = Tools.Tools.ConvertToLatinDate(EndDate),
            };
            var dataSource = GetKarkardDetail(item);
            return PartialView(partialViewUrl, dataSource);
        }
    }
}