﻿using ClientApp.Api;
using ClientApp.Models.ViewModels;
using COMMON;
using GabAPI.Models.Inputs.KarKard;
using GabAPI.Models.Outputs.KarKard;
using System;
using System.Globalization;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class KarkardReportController: BaseController
    {
        // GET: KarkardReport
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult FilterMonthCombo()
        {
            var partialViewUrl = "~/Views/KarkardReport/FilterForm/FilterMonthCombo.cshtml";
            var monthlist = new MonthViewModel[12];
            var j = 0;
            for(var i = 0; i < 12; i++)
            {
                j++;
                monthlist[i] = new MonthViewModel
                {
                    Id = j
                };
                switch(j)
                {
                    case 1:
                        monthlist[i].Title = "فروردین";
                        break;
                    case 2:
                        monthlist[i].Title = "اردیبهشت";
                        break;
                    case 3:
                        monthlist[i].Title = "خرداد";
                        break;
                    case 4:
                        monthlist[i].Title = "تیر";
                        break;
                    case 5:
                        monthlist[i].Title = "مرداد";
                        break;
                    case 6:
                        monthlist[i].Title = "شهریور";
                        break;
                    case 7:
                        monthlist[i].Title = "مهر";
                        break;
                    case 8:
                        monthlist[i].Title = "آبان";
                        break;
                    case 9:
                        monthlist[i].Title = "آذر";
                        break;
                    case 10:
                        monthlist[i].Title = "دی";
                        break;
                    case 11:
                        monthlist[i].Title = "بهمن";
                        break;
                    case 12:
                        monthlist[i].Title = "اسفند";
                        break;
                    default:
                        monthlist[i].Title = "";
                        break;
                }
            }
            return PartialView(partialViewUrl, monthlist);
        }
        public ActionResult FilterYearCombo()
        {

            var pc = new PersianCalendar();
            var PDate = Convert.ToInt32(pc.GetYear(DateTime.Now));
            var YearList = new YearViewModel[5];
            for(var i = 0; i <5; i++)
            {
                YearList[i] = new YearViewModel
                {
                    Id = i,
                    PersianYear = PDate - i,
                };
            }
            var partialViewUrl = "~/Views/KarkardReport/FilterForm/FilterYearCombo.cshtml";
            return PartialView(partialViewUrl, YearList);

        }
        public ActionResult KarkardGrid(int? Year, int? Month , long? ComplexID =0)
        {
            const string  partialViewUrl = "~/Views/KarkardReport/KarkardGrid/KarkardGrid.cshtml";
            if((Year == null && Month == null)||(Year == 0 && Month == 0))
            {
                Year = 1395;
                Month = 1;
            }
            var startDate = Year.ToString() + "/" + Month.ToString() + "/01";
            var EnSartDate = Tools.Tools.ConvertToLatinDate(startDate);
            var EnEndDate = EnSartDate.AddMonths(1);
            var item = new InputGetKarkardDetail()
            {
                StartDate = EnSartDate,
                EndDate = EnEndDate,
                ComplexID = Convert.ToInt64(ComplexID)
            };
            var dataSource = GetKarkardDetail(item);
            return PartialView(partialViewUrl, dataSource);
        }
        public OutputGetKarkardDetail[] GetKarkardDetail(InputGetKarkardDetail values)
        {
            
            var dataSource = ApiList.GetKarkardDetail(values);
            return dataSource;
        }
        public ActionResult FilterFormBazarCombo()
        {
            var partialViewUrl = "~/Views/KarkardReport/FilterForm/filterFormBazarCombo.cshtml";
            var dataSource = GetComplexList();
            return PartialView(partialViewUrl, dataSource);
        }
        private Complex[] GetComplexList()
        {
            return ApiList.GetComplexList();
        }
    }
}