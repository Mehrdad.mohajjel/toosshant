﻿using ClientApp.Api;
using COMMON;
using GabAPI.Models.Inputs.Complex;
using GabAPI.Models.Outputs.Complex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClientApp.Controllers
{
    public class ComplexController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddComplex(InputAddComplex values)
        {

            var item = new Complex
            {
                Title = values.ComplexTitle,
            };

            var token = GetUserToken();
            var apiResult = ApiList.AddComplex(item, token);
            return Content(apiResult);
        }
        public ActionResult ReportGrid()
        {
            var partialViewUrl = "~/Views/Complex/ReportGrid/ReportGrid.cshtml";
            var dataSource = GetReport();
            return PartialView(partialViewUrl, dataSource);

        }

        private OutputGetAllComplexList[] GetReport()
        {

            var token = GetUserToken();
            return ApiList.GetAllComplex(token);
        }
        public ActionResult RemoveComplex(long ComplexID)
        {
            var apiParam = new InputEditComplex
            {
                ComplexID = ComplexID
            };
            var token = GetUserToken();
            var apiResult = ApiList.RemoveComplex(apiParam, token);
            return Content(apiResult);
        }
    }
}