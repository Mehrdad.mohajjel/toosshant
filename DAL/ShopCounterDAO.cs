﻿using COMMON;

namespace DAL
{
    public class ShopCounterDAO :ModuleGenericDAO<ShopCounter>
    {
        public ShopCounterDAO(bool saveAllChangesAtEndOfScope = false)
          : base(saveAllChangesAtEndOfScope)
        {

        }
    }
}
