﻿using COMMON;
using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public class UserDAO : ModuleGenericDAO<User>
    {
        public UserDAO(bool saveAllChangesAtEndOfScope = false)
           : base(saveAllChangesAtEndOfScope)
        {

        }
        public List<GetAllUserList> GetAllUserList()
        {
            List<GetAllUserList> ResultQuery = (from a1 in ObjectContext.Set<User>()
                                      join a2 in ObjectContext.Set<Status>() on new { Id = (int)a1.Status } equals new { Id = a2.Id }
                                      select a1).Distinct().Select(x => new GetAllUserList
                                      {
                                          EmailAddress = x.EmailAddress,
                                          Id = x.Id,
                                          Mobile = x.Mobile,
                                          NickName = x.NickName,
                                          StatusTitle = x.Status1.ShowName,
                                          UserName = x.UserName
                                      }).ToList()
                                      ;


            return ResultQuery;
        }
    }

    public class GetAllUserList
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string NickName { get; set; }
        public string StatusTitle { get; set; }
        public int Status { get; set; }
    }
}
