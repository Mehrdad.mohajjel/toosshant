﻿using COMMON;

namespace DAL
{
    public class PriceDAO :ModuleGenericDAO<Price>
    {
        public PriceDAO(bool saveAllChangesAtEndOfScope = false)
           : base(saveAllChangesAtEndOfScope)
        {

        }
    }
}
