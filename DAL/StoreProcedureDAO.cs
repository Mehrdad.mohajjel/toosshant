﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class StoreProcedureDAO:ModuleGenericDAO<object>
    {
        public StoreProcedureDAO(bool saveAllChangesAtEndOfScope = false)
    : base(saveAllChangesAtEndOfScope)
        {

        }

        public DataTable GetCustomerShopByCustomerID(long CustomerID)
        {
            DataTable ResultDT = new DataTable();
            SqlParameter[] sqlParameters = new SqlParameter[1];

            sqlParameters[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            sqlParameters[0].Value = CustomerID;


            ResultDT = SelectSqlQuery("prc_GetCustomerShopByCustomerID", CommandType.StoredProcedure, sqlParameters);
            return ResultDT;
        }
        public DataTable GetExpenceDetail(DateTime StartDate, DateTime EndDate ,long ComplexId =0)
        {
            DataTable ResultDT = new DataTable();
            SqlParameter[] sqlParameters = new SqlParameter[3];

            sqlParameters[0] = new SqlParameter("@StartDate", SqlDbType.Date);
            sqlParameters[0].Value = StartDate;
            sqlParameters[1] = new SqlParameter("@EndDate", SqlDbType.Date);
            sqlParameters[1].Value = EndDate;
            sqlParameters[2] = new SqlParameter("@ComplexId", SqlDbType.BigInt);
            sqlParameters[2].Value = ComplexId;


            ResultDT = SelectSqlQuery("prc_GetExpenceDetail", CommandType.StoredProcedure, sqlParameters);
            return ResultDT;
        }
        public void AssingShop(long ShopID, long CustomerID, string TabloName,long UserId)
        {
            SqlParameter[] sqlParameters = new SqlParameter[4];
            sqlParameters[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            sqlParameters[0].Value = CustomerID;

            sqlParameters[1] = new SqlParameter("@ShopID", SqlDbType.BigInt);
            sqlParameters[1].Value = ShopID;

            sqlParameters[2] = new SqlParameter("@Tablo", SqlDbType.NVarChar);
            sqlParameters[2].Value = TabloName;

            sqlParameters[3] = new SqlParameter("@UserID", SqlDbType.BigInt);
            sqlParameters[3].Value = UserId;


            ExecuteSqlQuery("prc_AddCustomerToShop", CommandType.StoredProcedure, sqlParameters);
        }
        public DataTable GetCustomerShopByComplexID(long ComplexID)
        {
            DataTable ResultDT = new DataTable();
            SqlParameter[] sqlParameters = new SqlParameter[1];

            sqlParameters[0] = new SqlParameter("@ComplexID", SqlDbType.BigInt);
            sqlParameters[0].Value = ComplexID;


            ResultDT = SelectSqlQuery("prc_GetCustomerShopByComplexID", CommandType.StoredProcedure, sqlParameters);
            return ResultDT;
        }
        public DataTable GetExpenceDetailByKarkardID(long KarkardID)
        {
            DataTable ResultDT = new DataTable();
            SqlParameter[] sqlParameters = new SqlParameter[1];

            sqlParameters[0] = new SqlParameter("@KarkardID", SqlDbType.BigInt);
            sqlParameters[0].Value = KarkardID;


            ResultDT = SelectSqlQuery("prc_GetExpenceDetailByKarkardID", CommandType.StoredProcedure, sqlParameters);
            return ResultDT;
        }
        
        public DataTable RemoveCustomerShop(long? CustomerShopID, long UserID)
        {
            DataTable ResultDT = new DataTable();
            SqlParameter[] sqlParameters = new SqlParameter[2];

            sqlParameters[0] = new SqlParameter("@CustomerShopID", SqlDbType.BigInt);
            sqlParameters[0].Value = CustomerShopID;

            sqlParameters[1] = new SqlParameter("@UserID", SqlDbType.BigInt);
            sqlParameters[1].Value = UserID;

            ResultDT = SelectSqlQuery("prc_RemoveCustomerShop", CommandType.StoredProcedure, sqlParameters);
            return ResultDT;
        }
        public DataTable GetShopListByComplexID(long ComplexID)
        {
            DataTable ResultDT = new DataTable();
            SqlParameter[] sqlParameters = new SqlParameter[1];

            sqlParameters[0] = new SqlParameter("@ComplexID", SqlDbType.BigInt);
            sqlParameters[0].Value = ComplexID;


            ResultDT = SelectSqlQuery("prc_GetShopListByComplexID", CommandType.StoredProcedure, sqlParameters);
            return ResultDT;
        }
        
    }
}
