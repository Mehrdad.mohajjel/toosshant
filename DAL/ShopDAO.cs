﻿using COMMON;

namespace DAL
{
    public class ShopDAO :ModuleGenericDAO<Shop>
    {
        public ShopDAO(bool saveAllChangesAtEndOfScope = false)
           : base(saveAllChangesAtEndOfScope)
        {

        }
    }
}
