﻿using COMMON;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace DAL
{
    public class ModuleGenericDAO<T> : IDisposable where T : class
    {
        //----------------------------------------*
        private bool SaveContext = false;
        private bool HasLocalTransaction = false;
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// Returns a ModuleDbContext object. 
        /// </summary>
        public ModuleDbContext ObjectContext
        {
            get
            {
                return DataContextFactory.GetScopedDataContext();
            }
        }
        //----------------------------------------*

        //----------------------------------------*
        public ModuleGenericDAO(bool saveAllChangesAtEndOfScope = false)
        {
            SaveContext = saveAllChangesAtEndOfScope;
            HasLocalTransaction = DataContextFactory.BeginCurrentTransaction(SaveContext); // شروع تراکنش در صورت نیاز
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// بازیابی تمامی آبجکت های یک کلاس مشخص
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IQueryable<T> SelectAll()
        {
            return ObjectContext.Set<T>().AsNoTracking<T>();
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// بازیابی یک آبجکت از دیتابیس
        /// </summary>
        /// <typeparam name="T">
        /// هر نوع کلاس اِنتیتی
        /// </typeparam>
        /// <param name="t">
        /// آبجکتی که حاوی مقدار کلید برای بازیابی می باشد
        /// </param>
        /// <returns>
        /// آبجکت بازیابی شده
        /// </returns>
        public T SelectById(T t)
        {
            if (t != null)
            {
                var type = typeof(T);
                var properties = type.GetProperties();

                foreach (var propertyInfo in properties)
                {
                    if (propertyInfo.Name.ToLower().Trim() == "id") // By Default id is primary key in all Entities.
                        return ObjectContext.Set<T>().Find(propertyInfo.GetValue(t));

                }
            }
            return null;
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// افزودن لیستی از آبجکت ها به صورت یکجا
        /// </summary>
        /// <param name="Tlist"></param>
        public void Insert(List<T> Tlist)
        {
            try
            {
                foreach (T item in Tlist)
                    ObjectContext.Set<T>().Add(item);

                ObjectContext.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// افزودن یک آبجکت به دیتابیس
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public T Insert(T t)
        {
            try
            {
                ObjectContext.Set<T>().Add(t);
                ObjectContext.SaveChanges();
                return t;
            }
            catch
            {
                throw;
            }
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// ویرایش لیستی از آبجکت ها به صورت یکجا
        /// </summary>
        /// <param name="Tlist"></param>
        public void Update(List<T> Tlist)
        {
            try
            {
                foreach (T item in Tlist)
                    ObjectContext.Entry<T>(item).State = EntityState.Modified;
            }
            catch
            {
                throw;
            }
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// ویرایش یک آبجکت در روی دیتابیس
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public T Update(T t)
        {
            try
            {
                ObjectContext.Entry<T>(t).State = EntityState.Modified;
                return t;
            }
            catch
            {
                throw;
            }
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// حذف لیستی از آبجکت ها به صورت یکجا
        /// </summary>
        /// <param name="Tlist"></param>
        public void Delete(List<T> Tlist)
        {
            foreach (T item in Tlist)
                this.Delete(item);
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// حذف یک آبجکت از روی دیتابیس
        /// </summary>
        /// <param name="t"></param>
        public void Delete(T t)
        {
            try
            {
                ObjectContext.Entry<T>(t).State = EntityState.Deleted;
            }
            catch
            {
                throw;
            }
        }
        //----------------------------------------*

        //----------------------------------------*
        public DataTable SelectSqlQuery(string sqlSelect, CommandType CommandType = CommandType.Text , params object[] sqlParameters)
        {
            try
            {
                DataTable Result = new DataTable();
                if (ObjectContext.Database.Connection.State != ConnectionState.Open)
                    ObjectContext.Database.Connection.Open();
                using (var cmd = ObjectContext.Database.Connection.CreateCommand())
                {
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = sqlSelect;
                    cmd.CommandType = CommandType;
                    foreach (var param in sqlParameters)
                        cmd.Parameters.Add(param);
                    using (var reader = cmd.ExecuteReader())
                    {
                        Result.Load(reader);
                    }
                    
                }
                
                return Result;
            }
            catch
            {
                throw;
            }
        }
        //----------------------------------------*

        //----------------------------------------*
        public void ExecuteSqlQuery(string query, CommandType CommandType = CommandType.Text ,params object[] sqlParameters)
        {
            try
            {
                if (ObjectContext.Database.Connection.State != ConnectionState.Open)
                    ObjectContext.Database.Connection.Open();

                using (var cmd = ObjectContext.Database.Connection.CreateCommand())
                {
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType;
                    foreach (var param in sqlParameters)
                        cmd.Parameters.Add(param);
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
                throw;
            }
        }
        //----------------------------------------*

        //----------------------------------------*
        public void RollbackTransaction()
        {
            DataContextFactory.RollbackCurrentTransaction();
        }
        //----------------------------------------*

        //----------------------------------------*
        /// <summary>
        /// Disposes the ModuleGenericDAO.
        /// </summary>
        public void Dispose()
        {
            if (SaveContext && HasLocalTransaction)
            {
                if (ObjectContext != null && ObjectContext.Database.CurrentTransaction != null)
                {
                    ObjectContext.SaveChanges();
                    ObjectContext.Database.CurrentTransaction.Commit();
                }
                DataContextFactory.Dispose();
            }
        }
        //----------------------------------------*
    }
}
