﻿using COMMON;

namespace DAL
{
    public class CustomerShopDAO : ModuleGenericDAO<CustomerShop>
    {
        //----------------------------------------------------------------------//
        public CustomerShopDAO(bool saveAllChangesAtEndOfScope = false)
            : base(saveAllChangesAtEndOfScope)
        {

        }
    }
}
