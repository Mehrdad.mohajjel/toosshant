﻿
using COMMON;

namespace DAL
{
    public  class CustomerDAO : ModuleGenericDAO<Customer>
    {
        //----------------------------------------------------------------------//
        public CustomerDAO(bool saveAllChangesAtEndOfScope = false)
            : base(saveAllChangesAtEndOfScope)
        {

        }
    }
}
