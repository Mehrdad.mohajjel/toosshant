﻿using COMMON;

namespace DAL
{
    public class ComplexDAO : ModuleGenericDAO<Complex>
    {
        public ComplexDAO(bool saveAllChangesAtEndOfScope = false): base(saveAllChangesAtEndOfScope)

        {

        }
    }
}
