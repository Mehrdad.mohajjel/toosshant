﻿using COMMON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class CustomerTypeDAP : ModuleGenericDAO<CustomerType>
    {
        public CustomerTypeDAP(bool saveAllChangesAtEndOfScope = false)
    : base(saveAllChangesAtEndOfScope)
        {

        }

    }
}

