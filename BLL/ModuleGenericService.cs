﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BLL
{
    public class ModuleGenericService<T> where T : class
    {
        //-------------------------------------------*
        public static IQueryable<T> GetIQueryableAllData()
        {
            try
            {
                //---------- Start Business Logic --------*
                using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(false))
                {
                    return Manager.SelectAll();
                }
                //----------- End Business Logic ---------*
            }
            catch
            {
                throw;
            }
        }
        //-------------------------------------------*

        //-------------------------------------------*
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static List<T> GetALL(bool UseLocalTransaction = false)
        {
            try
            {
                //---------- Start Business Logic --------*
                using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(UseLocalTransaction))
                {
                    List<T> Result = Manager.SelectAll().ToList();

                    return Result;
                }
                //----------- End Business Logic ---------*
            }
            catch
            {
                throw;
            }
        }
        //-------------------------------------------*

        //-------------------------------------------*
        [DataObjectMethod(DataObjectMethodType.Select)]
        public static T GetSpecial(T item, bool UseLocalTransaction = false)
        {
            try
            {
                //---------- Start Business Logic --------*
                using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(UseLocalTransaction))
                {
                    T Result = Manager.SelectById(item);

                    return Result;
                }
                //----------- End Business Logic ---------*
            }
            catch
            {
                throw;
            }
        }
        //-------------------------------------------*

        //-------------------------------------------*
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public static T Add(T item, bool UseLocalTransaction = true)
        {
            using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(UseLocalTransaction))
            {
                try
                {
                    T result = Manager.Insert(item);
                    return result;
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        //-------------------------------------------*

        //-------------------------------------------*
        public static void Add(List<T> itemList, bool UseLocalTransaction = true)
        {
            using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(UseLocalTransaction))
            {
                try
                {
                    Manager.Insert(itemList);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        //-------------------------------------------*

        //-------------------------------------------*
        [DataObjectMethod(DataObjectMethodType.Update)]
        public static T Edit(T item, bool UseLocalTransaction = true)
        {
            using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(UseLocalTransaction))
            {
                try
                {
                    return Manager.Update(item);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        //-------------------------------------------*

        //-------------------------------------------*
        public static void Edit(List<T> itemList, bool UseLocalTransaction = true)
        {
            using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(UseLocalTransaction))
            {
                try
                {
                    Manager.Update(itemList);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        //-------------------------------------------*

        //-------------------------------------------*
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public static void Remove(T item, bool UseLocalTransaction = true)
        {
            using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(UseLocalTransaction))
            {
                try
                {
                    Manager.Delete(item);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        //-------------------------------------------*

        //-------------------------------------------*
        public static void Remove(List<T> itemList, bool UseLocalTransaction = true)
        {
            using (ModuleGenericDAO<T> Manager = new ModuleGenericDAO<T>(UseLocalTransaction))
            {
                try
                {
                    Manager.Delete(itemList);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        //-------------------------------------------*
    }
}
