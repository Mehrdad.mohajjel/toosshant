﻿using DAL;
using System.Collections.Generic;
using System.Linq;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using COMMON;
using System.Web;

namespace BLL
{
    public class UserService : ModuleGenericService<User>
    {
        private const string JWTSecretKey = "pY?QPPGkWrHWkX4hKX2QT&Z%Z@hRpx";
        public static User GetUserByUserName(string userName, bool UseLocalTransaction = false)
        {
            using (UserDAO Manager = new UserDAO(UseLocalTransaction))
            {
                try
                {
                    var pureName = userName.ToLower().Replace(".", string.Empty);
                    var UserItem = Manager.SelectAll().Where(a => a.UserName.Replace(".", string.Empty) == pureName).FirstOrDefault();
                    return UserItem;
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        public static bool IsUserPasswordMachByDatabase(string password, string dbHashedPassword)
        {
            bool result = false;
            if (Tools.Tools.HashString(password) == dbHashedPassword)
            {
                result = true;
            }
            return result;
        }
        public static string GenerateUserToken(User userInfo)
        {
            IDateTimeProvider provider = new UtcDateTimeProvider();
            var oneDay = provider.GetNow().AddDays(1);
            var secondsSinceEpoch = UnixEpoch.GetSecondsSince(oneDay);

            var payload = new Dictionary<string, object>
            {
                { "Id", userInfo.Id },
                { "UserName", userInfo.UserName },
                { "EmailAddress", userInfo.EmailAddress },
                { "NickName", userInfo.NickName },
                { "exp", secondsSinceEpoch }
            };

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var token = encoder.Encode(payload, JWTSecretKey);
            return token;
        }

        public static string ParseToken(string token)
        {
            const string secret = JWTSecretKey;
            var reqCookies = HttpContext.Current.Request.Cookies["userInfo"];
            if (reqCookies != null)
            {
                token = reqCookies["MohajjelJsonWebToken"].ToString();
            }


            IJsonSerializer serializer = new JsonNetSerializer();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

            var json = decoder.Decode(token, secret, verify: true);
            return json;
        }
    }
}
