﻿using DAL;
using System;
using System.ComponentModel;
using System.Data;

namespace BLL
{
    public class StoreProcedureService : ModuleGenericService<object>
    {
        //----------------------------------------------------------------------//
        public static DataTable GetExpenceDetail(DateTime StartDate, DateTime EndDate,long ComplexID =0, bool UseLocalTransaction = false)
        {
            using (StoreProcedureDAO Manager = new StoreProcedureDAO(UseLocalTransaction))
            {
                try
                {
                    return Manager.GetExpenceDetail(StartDate, EndDate, ComplexID);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        public static DataTable GetCustomerShopByCustomerID(long CustomerID, bool UseLocalTransaction = false)
        {
            using (StoreProcedureDAO Manager = new StoreProcedureDAO(UseLocalTransaction))
            {
                try
                {
                    return Manager.GetCustomerShopByCustomerID(CustomerID);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }
        
        public static void AssingShop(long CustomerID, long ShopID, string TabloName, long UserID, bool UseLocalTransaction = false)
        {
            using (StoreProcedureDAO Manager = new StoreProcedureDAO(UseLocalTransaction))
            {
                try
                {
                    Manager.AssingShop(ShopID, CustomerID, TabloName, UserID);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }

        public static DataTable GetCustomerShopByComplexID(long ComplexID, bool UseLocalTransaction = false)
        {
            using (StoreProcedureDAO Manager = new StoreProcedureDAO(UseLocalTransaction))
            {
                try
                {
                    return Manager.GetCustomerShopByComplexID(ComplexID);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }

        public static DataTable GetExpenceDetailByKarkardID(long KarkardID, bool UseLocalTransaction = false)
        {
            using (StoreProcedureDAO Manager = new StoreProcedureDAO(UseLocalTransaction))
            {
                try
                {
                    return Manager.GetExpenceDetailByKarkardID(KarkardID);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }


        public static void RemoveCustomerShop(long? CustomerShopID, long UserID, bool UseLocalTransaction = false)
        {
            using (StoreProcedureDAO Manager = new StoreProcedureDAO(UseLocalTransaction))
            {
                try
                {
                    Manager.RemoveCustomerShop(CustomerShopID, UserID);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }

            }

        }
        public static DataTable GetShopListByComplexID(long ComplexID, bool UseLocalTransaction = false)
        {
            using (StoreProcedureDAO Manager = new StoreProcedureDAO(UseLocalTransaction))
            {
                try
                {
                    return Manager.GetShopListByComplexID(ComplexID);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
        }

    }
}
